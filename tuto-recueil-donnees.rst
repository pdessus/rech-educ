.. _tuto_recueil-donnees:

***********************************************
Tutoriel -- Recueillir des données de recherche
***********************************************

.. index::
	single: auteurs; Dessus, Philippe

.. admonition:: Informations

	* **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, Espé & LaRAC, Univ. Grenoble Alpes.

	* **Date de création** : Janvier 2017.

	* **Date de modification** : |today|.

	* **Statut du document** : En travaux. 

	* **Résumé** : Ce tutoriel donne des pistes de travail pour élaborer un protocole de recueil de données dans le cadre d'une recherche en éducation. **Attention : ce tutoriel ne peut se substituer aux conseils d'un-e directeur-e de recherche**.

	* **Voir aussi** : Le :ref:`tuto_problematique`, le :ref:`tuto_organiser_memoire`, et le :ref:`tuto_redaction_memoire`.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Une fois que le sujet et la problématique ont été formulés, il faut réfléchir à la stratégie qui va permettre de récupérer des informations du contexte (*i.e.*, classes, écoles, établissements) afin d'apporter des solutions valides et fiables à la problématique.

Ce document recense quelques moyens de recueil de données éducatives, en référant principalement aux autres documents de cette base. Leur classement est relié, d'une part, aux acteurs (enseignants, élèves), d'autre part, à leurs activités (enseigner, apprendre). Pour chaque item, un article de recherche en lien avec le sujet est conseillé. 

Notons enfin que les documents ne donnent pas toujours des méthodes de recueil prêtes à l'emploi, mais parfois seulement les dimensions de l'activité concernée, qu'il faut donc traduire en unités observables.

Enseignants
===========

Représentations des enseignants
-------------------------------

#. Quelle est la représentation des enseignants à propos des stéréotypes, notamment de genre ? (voir :ref:`mixite_ecole`).
#. `Quelle est l'opinion des enseignants sur la manière dont ils favorisent l'apprentissage des élèves ? Le climat de leur classe ? <relationseleves.html>`_
#. `Quel est l'opinion des enseignants sur leur manière de gérer la discipline ? <testdiscipline.html>`_
#. Quels sont les différents moyens dont on dispose pour que les enseignants réfléchissent à leur métier (voir :ref:`enseignant_reflexif`) ?
#. Quelles sont les connaissances que l'enseignant a à propos du numérique ? (voir :ref:`tpack`).

Travail des enseignants
-----------------------

#. Comment évaluer la manière dont un enseignant est distant ou présent ? (voir :ref:`dist_enseignement`).
#. Quelles sont les routines, les épisodes, que les enseignants suivent quand ils enseignent ? (voir :ref:`concevoir_enseignement`).
#. Par quelles phases les enseignants passent quand ils préparent leurs cours ? (voir :ref:`concevoir_enseignement`).
#. Comment les enseignants peuvent-ils scénariser leurs cours ? (voir :ref:`sbd`).
#. `Comment les enseignants travaillent-ils en équipe ? <equipe.html>`_. Voir les dimensions du travail en équipe des enseignants dans Grangeat *et al*. (2009). `Analyser le travail collectif des enseignants : effets du contexte de l’activité sur les conceptualisations des acteurs <http://rsse.elearninglab.org/wp-content/uploads/2012/12/SZBW_9.1_Varia_Grangeat.pdf>`_. *Revue Suisse des Sciences de l’Éducation*, *31*\(1), 151-168 (notamment les dimensions p. 159).
#. `À quels types d'imprévus, d'incidents, les enseignants sont-ils confrontés ? <imprevu.html>`_
#. `Quels sont les problèmes de comportement d'élèves auxquels un enseignant fait face ? <discipline.htm>`_
#. Quel est le rôle des postures de l'enseignant en classe, en lien avec ses interactions avec les élèves ? (voir :ref:`corps_ecole`).
#. Comment peut-on analyser les démarches d'innovation des enseignants ? (voir :ref:`innovation`).
#. Quels types d'usage du numérique sont réalisés en classe ? (voir :ref:`usage_TIC`).


Elèves
======

L'activité des élèves
---------------------

#. Comment analyser les tâches et activités des élèves ? (voir :ref:`taches_ens_el`).
#. Comment analyser l'activité des élèves réalisant des exercices, sur papier ou avec un logiciel ? (voir :ref:`exerciseurs`), ou un tableau blanc interactif ? (voir :ref:`tbi`).
#. Comment analyser l'usage de logiciels par les élèves ? (voir :ref:`usage_TIC` ).


Interagir en classe
-------------------

#. `Comment observer et caractériser le climat émotionnel, organisationnel, et d'apprentissage d'une classe ? <../cours/cours-CLASS/>`_.
#. Comment observer le comportement des élèves en classe ? (voir :ref:`peda_differenciee`).
#. Comment observer les interactions enseignant-élèves en classe ? (voir :ref:`activite`).
#. Comment élaborer un sociogramme des relations interélèves (voir :ref:`sociometrie`).
#. `Comment observer les séances de travail en groupes ? <techgroupe.html>`_ (voir aussi :ref:`activite`).
#. Comment analyser une classe en tant que communauté de pratique ? (voir :ref:`constr_conn`).

Comprendre
----------

#. Comment analyser le processus de construction de connaissances (individuel, collectif) (voir :ref:`constr_conn`).
#. Quelles sont les différentes sortes de stratégies de compréhension que les élèves peuvent mettre en œuvre ? (voir :ref:`litt_form_hum`).
#. Comment faire verbaliser les élèves sur ce qu'ils ont compris ? (voir :ref:`appr_apprendre`).
#. Quelles sont les démarches que les élèves suivent dans le cadre d'expérimentations ? (voir :ref:`demarche_sci`).
#. Quelles sont les techniques que les élèves peuvent mettre en œuvre pour apprendre un contenu ? (voir :ref:`appr_visible`).
#. Comment analyser les documents (textuels, vidéo) du point de vue de la charge cognitive qu'ils peuvent engendrer ? (voir :ref:`charge_cognitive`).

Evaluer
-------

#. Comment questionner un élève sur son rapport au savoir ? (voir :ref:`culture_familiale`).
#. `Avec quels outils les élèves peuvent-ils s'auto-évaluer ? <outilseval.hmtl>`_
#. Comment analyser et évaluer les erreurs que les élèves font ? (voir :ref:`erreur`).
#. Quelles questions métacognitives peuvent guider les élèves dans leur apprentissage ? (voir :ref:`appr_apprendre`).
#. Comment évaluer la motivation des élèves ? (voir :ref:`motivation`).
#. Comment utiliser les cartes de concepts pour évaluer la compréhension des élèves ? (voir :ref:`cartes_concepts`).

Contexte
========

#. Comment analyser les fonctionnalités de matériel numérique pour l'apprentissage ? (voir :ref:`usage_TIC`).