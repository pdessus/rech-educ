.. _index:

======================================
Initiation à la recherche en éducation
======================================

.. http://www.cnesco.fr/fr/12-evaluations-du-cnesco-en-resume/

.. admonition:: Information

	* **Responsable éditorial** : `Philippe Dessus <http://pdessus.fr>`_, Inspé & LaRAC, Univ. Grenoble Alpes.

	* **Résumé** : Cette page indexe différents documents tutoriels d'initiation à la recherche en éducation.
	
	* **Dernière mise à jour** : |today|.

	* **Note** : La réalisation de ce site a été en partie financée par le projet `ANR-IDEFI numérique ReflexPro <http://www.idefi-reflexpro.fr>`_. 

	* **Téléchargements** : Liens vers les compilations de tous les documents ci-dessous en `PDF <http://espe-rtd-reflexpro.u-ga.fr/media/pdf/sciedu-cours-rech-educ/latest/sciedu-cours-rech-educ.pdf>`_ et `epub <http://espe-rtd-reflexpro.u-ga.fr/projects/sciedu-cours-rech-educ/downloads/epub/latest/>`_.

	* **Licence** : Sauf mention contraire spécifiée sur le document, tous les documents de ce site sont placés sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Tutoriels sur la recherche en éducation
=======================================

.. considérer mettre ici certains docs Recherche et veille documentaire avec le numérique du général.

Outils de veille
----------------

.. toctree::
	:maxdepth: 1

	Veille pédagogique et académique : outils et stratégies (janv. 2012) <veillepeda>	
	Veille académique, une sélection de ressources (janv. 2016) <veille-sci-ressources>
	Comprendre la littérature scientifique (mars 2017) <litt_sci>	
	

Problématique et méthodes
-------------------------

.. toctree::
	:maxdepth: 1

	Tutoriel -- Lire un article de recherche (Avril 2017) <tuto-lire-article>
	Tutoriel -- Problématique et hypothèses de recherche (Janv. 2015) <tuto-problematique>
	Tutoriel -- Définir une méthode de recherche (Avril 2017) <tuto-methode-rech>
	Tutoriel -- Concevoir un questionnaire : des dimensions aux questions (Fév. 2019) <tuto_concevoir_quest>
	Faire des recherches sur l’usage du numérique* (Sept. 2017) <rech_info_TIC>
	CONPA, un jeu de création et réflexion sur l'usage du numérique (Nov. 2016) <conpa-jeu>

Recueil de données
------------------

.. toctree::
	:maxdepth: 1

	Tutoriel -- Recueillir des données de recherche (Janv. 2017) <tuto-recueil-donnees>
	Tutoriel -- Mener un *focus group* (Déc. 2018) <tuto-focus-group>
	Tutoriel -- La captation de vidéos de classes (Déc. 2018) <tuto-captation-video-classe>	

Rédaction d'un mémoire ou d'un poster
-------------------------------------

.. toctree::
	:maxdepth: 1

	Tutoriel -- Organiser la rédaction d'un mémoire (Janv. 2015) <tuto-organiser-memoire>
	Tutoriel -- La rédaction d'un mémoire (Janv. 2015) <tuto-redaction-memoire>
	Tutoriel -- Tableaux et figures APA (*American Psychological Association*) (Juin 2018) <tuto-tab-fig-apa>
	Tutoriel -- Les références APA (*American Psychological Association*) (Juin 2018) <tuto-references-apa>
	Atelier -- Réaliser un poster scientifique (Avril 2017) <atelier_poster_sci>


Syllabi
-------

.. toctree::
	:maxdepth: 1

	Syllabus -- UE 205 “Recherche”, Espé, UGA, 2018-20 <syl-ue-rech-prob-2>
	Syllabus -- UE 206 “Recherche”, Espé, UGA, 2017-18 <syl-ue-rech-prob>
	Syllabus -- UE 800 Recherche SD, Espé, UGA, 2016-17 <syllabus-ue-recherche>


Index et tables
===============
 
* :ref:`genindex`
* :ref:`search`