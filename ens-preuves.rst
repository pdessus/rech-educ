.. _ens_preuves:

************************************
Enseignement informé par les preuves
************************************

.. Lire
.. https://www.edge.org/response-detail/25433
.. {Rømer, 2018 #21902}
.. https://olc-wordpress-assets.s3.amazonaws.com/uploads/2019/10/Neuromyths-Betts-et-al.-September-2019.pdf
.. https://circlcenter.org/evidence-centered-design/
.. https://docs.google.com/spreadsheets/d/1qEqE45gyxaPt1mqTE1V3JLkLq290iCvFVJdMWEgHR9E/edit?usp=sharing


.. index::
	single: auteurs; Pobel-Burtin, Céline
	single: auteurs; Dessus, Philippe


.. admonition:: Informations

	* **Auteurs** : Céline Pobel-Burtin, LaRAC & Inspé-UGA, & `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé-UGA.

	* **Date de création** : Novembre 2019.

	* **Date de modification** : |today|.

	* **Statut du document** : En cours.

	* **Résumé** :  

	* **Voir aussi** : Le Document ens. explicite XX.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

Introduction
============

Le terme "éducation fondée sur les preuves" (EFP, ou *evidence-based education*), amène de nombreuses questions. La première est "sur quoi donc d'autre voudriez-vous les fonder ? des croyances ? vous n'y pensez pas ?". La seconde est "bon, d'accord, sur la science, mais comment distinguer une bonne preuve d'une mauvaise ?". La troisième est "efficace, mais pour quoi faire ? une pratique donnée peut-elle être bonne *en soi* ?" La quatrième est "bon, d'accord, celle-ci est bonne, et je sais pour quoi je vais l'utiliser, mais comment vais-je faire dans ma classe ?". La cinquième et dernière est : "Au fait, quelles preuves avons-nous que l'éducation fondée sur des preuves marche ?"

Ce document donne quelques pistes à propos de ces différentes questions.

Tout d'abord, quelques précisions question par question :

1. Effectivement, l'EFP peut paraître en opposition avec les croyances, les perceptions ou les intuitions des enseignants eux-mêmes. Toutefois, est-il raisonnable de penser que la seule source de preuves utilisable pour fonder la pratique des enseignants est la littérature de recherche ? Ne doit-on pas aussi compter sur sa propre expérience, sa réflexivité, les réactions des élèves ? (Mockler & Stacey, 2019)

2. Les preuves, ici, sont des preuves scientifiques, issues de la littérature de recherche. Quand on parcourt des articles de recherche en éducation, on peut avoir d'assez bonnes idées des pratiques pouvant être efficaces, ou non, dans les contextes dans lesquels ces recherches ont été faites, bien sûr. Toutefois, il faut être prudent car certaines preuves peuvent avoir une meilleure validité externe que d'autres (*i.e.*, être plus généralisables). Nous verrons plus loin cette hiérarchie des preuves.

3. Effectivement, il est sans doute nécessaire de se fier à des données probantes plutôt que de simples intuitions, mais pour faire quoi ? (Biesta 2010)

5. Il semble y avoir très peu de preuves que l'EFP se soit appliqué à soi-même ce qu'elle réclame de faire. [Est-ce que, automatiquement, choisir une pratique ayant montré ses preuves fait mieux réussir qu'une pratique de contrôle ?]

Le problème se pose lorsqu'on veut les répliquer dans notre propre contexte, qui  diffère très souvent du contexte initial, selon des variables pas toujours évidentes à caractériser.

Alors, les pratiques qu'on utilise pour enseigner ou éduquer sont-elles si éloignées des preuves scientifiques pour qu'on ait besoin d'insister sur les liens entre science et pratique ? D'autres parlent plutôt de "pratique informée par les preuves" (*evidence-informed practice*) :cite:`nelson17`

Où trouver des travaux pouvant inspirer le design de l'enseignement ?


Ce que l'on sait
================

Qu'est-que l'EFP ?
------------------

Comme les recherches fondées sur les preuves des autres domaines, il est difficile d'avoir une définition précise de ce que serait l'EFP. Bouffard et Reid :cite:`bouffard12` signalent que sa préoccupation principale est de déterminer "ce qui marche", plutôt que d'établir la vérité : réaliser des interventions pédagogiques pour lesquelles il y a suffisamment de preuves établies qu'elles "marchent", autrement dit, si on les compare à d'autres interventions non fondées sur les preuves, les premières permettent des résultats significativement meilleurs et durables

La hiérarchie des preuves
-------------------------

Toutes les preuves se valent donc ? Peut-on établir une hiérarchie ? Il existe de nombreuses manières de représenter une hiérarchie de preuves, et peu s'accordent :cite:`bouffard12` ; de plus, les travaux faisant état de telles hiérarchies n'expliquent en général pas comment elles ont été construites. Enfin, toujours en reprenant Bouffard et Reid, ces hiérarchies amènent à penser que certains types de recherches sont scientifiquement supérieurs à d'autres, et donc le rejet de certaines pratiques qui ont tout de même pu amener des connaissances intéressantes (il est connu que Jean Piaget menait souvent des études avec un nombre réduit de participants). 

Présentons tout de même une pyramide des preuves :

* méta-analyse
* cas (cf. A.L. Davidson) https://www.linkedin.com/pulse/letting-cat-out-bag-small-sample-sizes-ann-louise-davidson


Les pratiques, pour quoi faire ?
--------------------------------

Comme le signale justement Biesta :cite:`biesta19`, dire qu'une pratique est efficace ne donne aucune information sur d'éventuelles valeurs morales, ou éthiques, liées à cette pratique. Comme il le signale directement (p. 3) "Il existe des tortures efficaces et des tortures non efficaces […], mais ça ne veut pas dire que les tortures efficaces […] sont bonnes".


Et si on demandait aux enseignants ?
------------------------------------

Une autre possibilité pour cerner la validité des preuves est tout simplement de demander aux enseignants ce qu'ils considèrent comme une preuve valide. Mockler et Stacey (2019) ont réalisé un questionnaire en ce sens, auprès de 500 enseignants australiens des premier et second degrés. Les preuves les plus fiables, selon eux, sont les suivantes, par ordre décroissant de fiabilité (seuil supérieur à 50 % de réponses "très fiable" ou "extrêmement fiable") :

* leur propre observation de classe systématique et réflexion ;
* les entretiens avec les étudiants (individuels ou collectifs) ;
* les activités d'évaluation ;
* l'observation de la classe par un collègue ou un pair ;
* l'observation de la classe par un collègue expérimenté.

Méta-méta-méta-analyses
-----------------------

Le modèle en 6 étapes : https://ebtn.org.uk/six-steps/

Ce que l'on peut faire
======================

L'EFP fonctionne un peu comme un test de Rorschach : l'avis qu'ont les chercheurs et enseignants sur ce champ peut révéler leur propre opinion sur l'éducation, en plus de leur opinion sur l'EFP.

Par exemple, on peut penser :cite:`alvarez13,bryk`, que l'EFP permet de rapprocher les enseignants de la recherche.

Tout d'abord, on peut réfléchir à la manière d'implémenter une intervention inspirée par les preuves ? [Céline, à toi de jouer]

Ensuite, on peut explorer les nombreuses ressources, souvent gratuites, proposant des rapports très variés sur l'EFP.

Ressources
==========

Ci-dessous une liste non exhaustive de ressources gratuites pouvant inspirer le design de l'enseignement [Les classer mieux].

Classement par taille d'effet
-----------------------------

* `Evidence for ESSA (J. Hopkins Univ., États-Unis) <https://www.evidenceforessa.org>`_ : Description d'interventions en mathématiques et lecture (école-collège-lycée).
* `Teaching and learning toolkit (EEF) <https://educationendowmentfoundation.org.uk/evidence-summaries/teaching-learning-toolkit/>`_ : Pour visualiser les effets des composants.
* `Visible learning (John Hattie) <https://visible-learning.org>`_ : Le site d'un des plus célèbres outils d'estimation d'effets [liste des effets <https://visible-learning.org/hattie-ranking-influences-effect-sizes-learning-achievement/>`_].
* `What works Clearinghouse (USA) <https://ies.ed.gov/ncee/wwc/>`_ : Important site de synthèses d'EFP.

Rapports
--------

* `Best evidence encyclopedia (GB) <http://www.bestevidence.org.uk>`_ : Des rapports sur l'enseignement des maths et de la lecture (premier et second degré). 
* `Campbell Collaboration  <https://campbellcollaboration.org>`_ : Réalise des synthèses de recherches dans de nombreux domaines, dont l'éducation (env. 40 rapports).
* `Coallition for evidence (US) <http://coalition4evidence.org>`_ : Site qui n'est plus mis à jour.
* `Centre for Evaluation and Monitoring (GB) <http://www.cem.org/blog/>`_ : Des billets de blog, des vidéos sur l'EFP.
* `Deans for impact (USA) <https://deansforimpact.org>`_ : Des fiches documentaires EFP.
* `Education Endowment Foundation (UK) <https://educationendowmentfoundation.org.uk>`_ : L'un des meilleurs sites de diffusion de l'EFP.
* `Entrance to future education (projet européen) <http://efe-project.eu>`_ : Répertoire de pratiques pédagogiques.
* `EPPI Centre (GB) <https://eppi.ioe.ac.uk/cms/Default.aspx?tabid=53>`_ : Portail de ressources sur l'EFP, parfois payantes.
* `Evidence-based teaching (S. Kilian, Australie) <https://www.evidencebasedteaching.org.au>`_ : Un site répertoriant des pratiques d'enseignement efficaces.
* `Evidence-based living (Cornell Univ., USA) <http://evidencebasedliving.human.cornell.edu/category/youth-development/>`_ : des billets de blog de thèmes divers pointant sur des rapports complets.
* `Evidence for Learning (Australie) <https://www.evidenceforlearning.org.au>`_ : Site contenant de nombreux rapports sur l'EFP.
* `Education Development Trust (EDT) <https://www.educationdevelopmenttrust.com/>`_ : Nombreux rapports de recherche, dimension internationale.
* `Institute for effective education (UK) <https://the-iee.org.uk>`_ : 
* `The Learning Scientists (USA) <https://www.learningscientists.org>`_ : Des posters sur des stratégies d'apprentissage efficaces, et de nombreux billets de blog.
* `The learning agency lab (USA) <https://www.the-learning-agency-lab.com/science-of-learning-research-meets-practice.html>` : Des vidéos intéressantes sur des stratégies d'apprentissage efficaces. 
* `National Foundation for Educational Research (GB) <https://www.nfer.ac.uk/>`_
* `Evidence based education (EBE, GB) <https://evidencebased.education/>`_ : Quelques manuels sur l'évaluation (Resources>E-Library).
* `REF Impact studies <https://impact.ref.ac.uk/casestudies/Search1.aspx>`_ : Portail référençant de très nombreuses 'études d'impact'.
* `What works database (GB) <https://www.thecommunicationtrust.org.uk>`_ : (sur inscription gratuite) Grande base de données d'interventions (pas toujours très bien documentées).


Références
==========

Webographie
-----------

* Mockler, N., & Stacey, M. (2019, 18 mars). `What’s good ‘evidence-based’ practice for classrooms? We asked the teachers, here’s what they said <https://www.aare.edu.au/blog/?p=3844>`_. Blog EduResearch Matters.

Travaux
-------

.. bibliography:: ens-preuves.bib
	:style: apa
