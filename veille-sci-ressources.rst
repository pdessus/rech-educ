.. _ressources_veille:

***********************************************
Veille académique : une sélection de ressources
***********************************************

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Le Hénaff, Benjamin
	single: auteurs; Mermet, Jean-Michel

.. admonition:: Informations

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, Univ. Grenoble Alpes, Benjamin Le Hénaff, LaRAC, et Jean-Michel Mermet, SIMSU, Univ. Grenoble Alpes.

	* **Date de création** : Janvier 2016 (entièrement refondu d'une version antérieure débutée en 2010).

	* **Date de modification** : |today|.

	* **Nombre de références** : **195**.

	* **Statut du document** : En travaux.

	* **Résumé** : Ce document a pour but de répertorier et partager des ressources sur la productivité académique, ou d'autres activités propres aux chercheurs ou enseignants : veiller, collecter, traiter des informations pour la recherche ou l'enseignement.

	* **Voir aussi** : Doc. SAPP :ref:`veille_peda`.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

.. contents:: Table des matières du document
	:local:
	:depth: 3

------------
Introduction
------------

Ce document a pour but de répertorier et partager des ressources sur la productivité académique, ou d'autres activités propres aux chercheurs ou enseignants : veiller, collecter, traiter des informations pour la recherche ou l'enseignement. Il complète le doc. SAPP :ref:`veille_peda`. Il est construit ainsi : par thème, un outil fondamental est décrit tout d'abord, précédé d'un "1.", suit une liste d'autres concurrents possibles.

Le cycle décrit ici est librement adapté de Hull, D., Pettifer, S. R., & Kell, D. B. (2008). Defrosting the Digital Library: Bibliographic tools for the next generation web. *PLoS Comput Biol, 4* (10) ; avec des ajouts provenant de Blair, A. M. (2010). *Too much to know: Managing scholarly information before the modern age*. New Haven: Yale UP.

Le lecteur intéressé à ces questions pourra consulter le blog des auteurs : `Productivité académique <https://productiviteacademique.wordpress.com>`_ et le blog de Jean-Michel Mermet : `De la page au clic <http://delapageauclic.tumblr.com>`_. Nous avons également organisé une demi-journée d'études le 19 mai 2010  à l'IUFM de Grenoble, dont l'ensemble des podcasts est à http://podcast.grenet.fr/structures/ujf/chercheur-2-0/.

.. _veille_select:

-----------------------------
Sélectionner des informations
-----------------------------

Rechercher des informations
===========================

La première phase de la recherche est celle de la sélection d'informations pertinentes pour son travail d'enseignement ou de recherche. En effet, tout chercheur ou enseignant a besoin de connaître précisément les travaux de ses collègues, à la fois pour s'y appuyer, mais aussi montrer que son travail est original. Il est aussi nécessaire qu'il ait une politique de sauvegarde de ses travaux.

Les moteurs de recherche généralistes
*************************************

#. `Google <https://www.google.fr/>`_ est effectivement incontournable. 

Citons sa `page de recherche avancée <https://www.google.fr/advanced_search>`_, qui donne plus de latitude de mettre en valeur un terme (on le cherchera alors dans le titre de la page), de chercher des documents formalisés (on cherche les .doc ou les .pdf) et de déterminer la fraicheur du document par date (ou du moins sa fraicheur d'analyse ce qui n'est pas toujours la même chose). Le problème de Google est que son entreprise incite ses utilisateurs à l'utiliser en étant connecté à leur compte, ce qui permet de les tracer.

* `DuckDuckGo <https://duckduckgo.com>`_, qui n'est pas aussi performant que Google, ne trace pas ses utilisateurs.


Les moteurs de recherche du web invisible
*****************************************

Ils sont légion et recherchent des informations dans la partie du web, dite invisible, qui n'est justement pas indexée par les moteurs de recherche généralistes. C'est notamment le contenu des bases de données bibliographiques, factuelles et en texte intégral accessibles via internet, de façon libre ou non. On les repère *via* des annuaires spécialisés ou en effectuant une recherche sur internet avec le thème suivi du terme "database" (ex. "*toxic chemicals database*"). Voir aussi l'excellent `dossier <http://c.asselin.free.fr/french/invisible_web.htm>`_ du site Intelligence Center. 

* `ipl2 <http://www.ipl.org>`_
* `Infomine <http://library.ucr.edu/view/infomine>`_
* `CompletePlanet <http://aip.completeplanet.com>`_  


Les moteurs de recherche spécialisés
************************************

Ils proposent des fonctionnalités particulières (analyse automatique du contenu de la requête). 

#. `WolframAlpha <http://www.wolframalpha.com>`_ est le moteur le plus connu.

* `kngine <http://www.kngine.com>`_

.. _moteurs_acad:

Les moteurs de recherche académiques
************************************

Ces moteurs de recherche sont centrés sur le monde académique. Voire faire de la veille pour tomber sur des documents par hasard. Voir une `liste <http://iplus.ukoln.ac.uk/component-function/search.html>`_ de tels moteurs.

#. `Scholar Google <https://scholar.google.fr>`_ permet de rechercher des références (articles, ouvrages) puisées dans les sites de revues ou sites d'universités. Donne également une indication du nombre de citations de chaque référence et permet la création de sa page personnelle. 

* `adsabs <http://adsabs.harvard.edu>`_ [NASA, physique et astrophysique]
* `AuthorMapper <http://authormapper.com>`_ de Springer, permet de représenter les productions contenant des mots-clés donnés sur une carte du monde  [Site, gratuit]
* `BASE <https://www.base-search.net>`_ (Bielefield Academic Search Engine)
* `Digital promise <http://digitalpromise.org>`_ [Présente des vues en réseau]
* `Dimensions <https://app.dimensions.ai/>`_
* `Microsoft Academic Research <http://academic.research.microsoft.com/>`_
* `Odysci <http://academic.odysci.com/>`_ [Site, gratuit sur inscription ; contient aussi une base de conférences]
* `Paperity <http://paperity.org>`_ [Agrégateur de revues en libre accès]
* `RefSeek <http://www.refseek.com>`_
* `Scinapse <https://scinapse.io/>`_
* `Scopus <https://www.elsevier.com/solutions/scopus>`_ [site payant]
* `Sweet Search <http://www.sweetsearch.com>`_
  
Butiner
=======

Lors d'une session de recherche sur le net, on est confronté (souvent de manière inattendue, voir sérendipité) à de l'information qu'on veut mémoriser temporairement pour la lire plus tard. Ensuite, on peut toujours la référencer, la stocker et l'annoter.  La plupart de ces outils permet non seulement une mémorisation/annotation de ressources pour soi, mais aussi pour les autres (collaborative), ce qui permet également de butiner de l'information. Voici une liste complète de ces outils (`Bookmarking social websites <https://en.wikipedia.org/wiki/List_of_social_bookmarking_websites>`_).

Outils d'aide au butinage
*************************

#. `Instapaper <https://www.instapaper.com>`_ (cf. `tutoriel <http://tournerlapage.net/que-faire-dâ%C2%80%C2%99une-information-interessante-trouvee-sur-internet-3>`_) permet de mémoriser d'un clic une page intéressante et de retrouver, au moment opportun, toutes les pages marquées. Les pages peuvent alors être lues soit telles que vues initialement, ou bien en mode "lecture facilitée". Il est possible de classer les pages lues dans des dossiers ou de les partager.

* `Pocket <http://getpocket.com>`_

Mémoriser et annoter des sites
******************************

Les outils suivants permettent de mémoriser les url des ressources dont on veut garder la trace. Ils permettent d'affecter à chaque URL un titre, une description, des mots clés. 

#. `Diigo <https://www.diigo.com>`_ permet en outre de surligner des passages des pages, de placer des commentaires contextuels, d'exporter les infos ajoutées vers des outils de partage, etc.
 
* `Delicious <https://delicious.com/>`_
* `Historio.us <http://historio.us>`_
* `Hypothes.is <https://hypothes.is>`_
* `Licorize <https://licorize.com>`_ [site, payant, mélange de delicious et de liste de choses à faire]
* `Scoopit <http://www.scoop.it>`_ 
* `Sqworl <http://sqworl.com>`_
* `Stample <https://stample.co/>`_

Mémoriser et annoter des références bibliographiques
****************************************************

À l'image de ce qui est réalisable avec la mémorisation d'URL, ces outils permettent de mémoriser les références bibliographiques, de les annoter et de les gérer.


#. `Zotero <https://www.zotero.org>`_  est l'outil gratuit le plus puissant.
 
* `F1000 <http://f1000.com>`_ [Payant]
* `Mendeley <https://www.mendeley.com>`_

Recevoir
========

Avec la technologie RSS (`Really Simple Syndication <https://fr.wikipedia.org/wiki/RSS>`_) accessible sur de très nombreux sites, on peut faire de la veille de façon très performante grâce aux agrégateurs de contenus. Cela permet de collecter et "moissonner" chaque jour les nouvelles publications identifiées par les fils RSS auxquels on s'est abonné. 

#. `Feedly <http://feedly.com/i/welcome>`_

* `Sciencescape <https://sciencescape.org>`_ [Orienté sciences de la nature]
* `Sparrho <http://www.sparrho.com/>`_ [Orienté articles de recherche]
* `Tadaweb <http://www.tadaweb.com>`_

Outils de mashup
****************

Le mashup est l'intégration de flux RSS dans des tableaux de bord, aisément consultables.

* `Netvibes <http://www.netvibes.com/fr>`_
* `Protopage <http://www.protopage.com>`_

Outils d'automatisation de recherches
*************************************

* `Copernic <http://www.copernic.com/fr/>`_
* `DevonAgent <http://www.devontechnologies.com/products/devonagent/overview.html>`_ [OS X uniquement]

Outils d'alertes
****************

La quasi-totalité des éditeurs de journaux scientifiques proposent des alertes par courriel et par journaux ou thèmes.

#. `Google Scholar <https://scholar.google.fr>`_

Sauvegarder des travaux
=======================

À force de tomber sur des articles intéressants, il peut nous venir l'idée de les sauvegarder pour pouvoir les lire hors ligne (ou pour être sûr de pouvoir le retrouver). Il s'agit ici de sauvegarder sur son ordinateur des documents, souvent en PDF, en les annotant, en les classant, en en ayant une vue globale et en espérant pouvoir les retrouver. Certains de ces logiciels intègrent des mécanismes de recherche automatique de métadonnées d'articles (comme Papers, Mendeley ou ReadCube). voir aussi section :ref:`gerer_references`.

Lire un avis intéressant contre ce type de logiciels, et les commentaires qu'il a suscités : http://al3x.net/2009/01/31/against-everything-buckets.html.  Voir aussi un billet de notre blog sur les logiciels "fourre-tout" http://productiviteacademique.wordpress.com/2010/05/30/les-logiciels-fourre-tout/

#. `DevonThink <http://www.devontechnologies.com/products/devonthink/overview.html>`_ [OS X]

* `Benubird <http://www.debenu.com/products/desktop/debenu-pdf-benubird/>`_ [Windows, gratuit]
* `Colwiz <https://www.colwiz.com>`_ [gratuit, multiplateforme, gère les réfs biblio]
* `Dekstrus <http://www.dekstrus.com/main.asp>`_ [Windows]
* `Fluxiom <https://www.fluxiom.com>`_ [toutes plateformes, payant, sauvegarde dans les nuages]
* `Mendeley <mendeley.com>`_ [stand alone, multiplateformes, gère les réfs biblio]
* `PaperPile <https://paperpile.com>`_ [OS X & Linux]
* `Papers <http://papersapp.com>`_ [OS X]
* `ReadCube <https://paperpile.com>`_ [Windows & Mac OS X]
* `Together <http://reinventedsoftware.com/together/>`_ [OS X]
* `TogoDoc <http://tdc.cb.k.u-tokyo.ac.jp>`_ [Gratuit, OS X & Windows]
* `Utopiadocs <http://utopiadocs.com>`_
* `Zotero <https://www.zotero.org>`_ [multiplateforme]

.. _recommander:

Recommander et se faire recommander des travaux
===============================================

Il est important, dès l'étape de sélection des informations, de recommander des URL à sa communauté. Deux principes-clés : celle de veille et celle de curation. La veille impose de récupérer le plus régulièrement possible des informations possiblement intéressantes pour son propre réseau. La "curation" est un angliscisme signalant qu'une personne anime, sélectionne, nettoie un ensemble de ressources de la même manière qu'un jardinier s'occupe de son potager. Elle remplace petit à petit l'ancienne activité de "surveillance" (voir le billet d'O. Le Deuff `Le réveil de la veille : prendre soin plutôt que surveiller <http://www.guidedesegares.info/2011/11/27/le-reveil-de-la-veille-prendre-soin-plutot-que-de-surveiller/>`_). 
Voir aussi la Section :ref:`diffuser-travaux`.

Beaucoup d'outils de blog "légers", comme `Tumblr <https://www.tumblr.com>`_, peuvent également servir à cette fin. L'article de Kieslinger et Ebner `A Qualitative Approach towards Discovering Microblogging Practices of Scientists <http://www.icl-conference.org/dl/proceedings/ICL2011/program/contribution112_a.pdf>`_ (*Proc. ICL 2011*) approfondit cette question Il contient de nombreuses références aux outils de ce type.

#. `Twitter <http://twitter.com>`_ [microblogging].

* `Buzzfeed <http://www.buzzfeed.com>`_
* `Digg <http://digg.com>`_ [site de recommandation plus grand public]
* `Plurk <http://www.plurk.com/top/>`_
* `Reddit <https://www.reddit.com>`_
 
.. _veille_lire:

----  
Lire
----

Cette section est dédiée à de nombreux outils facilitant la lecture des ressources préalablement sélectionnées, mais aussi analysant leur importance d'un point de vue scientométrique (niveau de citation, facteur d'impact). 

Lire des travaux
================

Cette section contient une série de sites généralistes proposant des articles en ligne, en accès gratuit ou payant. Au vu du nombre gigantesque de tels sites, il est impossible ici de faire autre chose que de donner une liste très sommaire. Les bibliothèques universitaires sont également munies de très bons répertoires de tels sites. Bien évidemment, l'accès qu'on peut avoir des revues payantes dépend de l'abonnement de l'université et, selon ses finances, on peut vite se retrouver derrière un "mur payant".

Outils de recherche
*******************

* `Springer Exemplar <http://www.springerexemplar.com>`_ [Concordancier: Recherche d'expressions dans leur contexte, au sein des productions Springer]


Généralistes : Livres
*********************

Accès intégral ou partiel à des ouvrages pas nécessairement libres de droits

* `DOAB <http://doabooks.org>`_ (*Directory of Open Access Books*)
* `Europeana <http://www.europeana.eu/portal/>`_ (Bibliothèque numérique européenne, 48 M d'entrées)
* `Gallica <http://gallica.bnf.fr>`_ [Bibliothèque nationale de France]
* `Google Livres <https://books.google.fr>`_ [Accès partiel à desouvrages pas nécessairement libres de droits]
* `OAPEN <http://www.oapen.org/home>`_ (Bibliothèque open)

Généralistes : Revues (accès libre) 
***********************************

La plupart des éditeurs de journaux académiques publient maintenant une partie de leur fonds en accès libre. Nous ne les référençons pas ici, ni les fournisseurs d'articles payants.

Répertoires internationaux
--------------------------

* `DOAJ <https://doaj.org>`_ (*Directory of Open Access Journals*)
* `InTech <http://www.intechopen.com>`_ [Portail de livres et revues de sciences en open access]
* `JURN <http://www.jurn.org/#gsc.tab=0>`_ [Portail d'articles et thèses en accès gratuit]

Répertoires francophones
------------------------

* `Erudit.org <http://www.erudit.org>`_ (150 revues, livres, thèses et documents)
* `Index Savant <http://indexsavant.com>`_ 
* `Mir@bel <http://www.reseau-mirabel.info>`_ [plus de 3000 revues]
* `Persee <http://www.persee.fr>`_ (revues francophones en ligne, 577k documents)
* `Revues.org <http://www.revues.org>`_ (420 revues francophones)

Résumés d'articles
------------------

* `PubMed <http://www.ncbi.nlm.nih.gov/pubmed>`_ [recherche médicale et biologique]
* `Eric <http://eric.ed.gov>`_ [Résumés d'articles en éducation, contient aussi de plus en plus de littérature grise intégrale]

Editeurs
********

* `Athabasca UP <http://www.aupress.ca>`_
* `National Academies Press <http://www.nap.edu>`_

Répertoires et bouquets d'éditeurs à accès payant
-------------------------------------------------

* `Cairn <http://www.cairn.info>`_ (Français)
* `OpenLibrary <https://openlibrary.org>`_
* `Project MUSE <http://muse.jhu.edu>`_ (Anglais)

Littérature grise 
*****************

Rapports, documents techniques, conférences
-------------------------------------------

* `OpenGrey <http://www.opengrey.eu>`_ [Ressource européenne]
* `Scribd <https://fr.scribd.com>`_ [Qualité de contenu très variable, accès payant]
* `TEL <https://tel.archives-ouvertes.fr>`_ [Thèses en ligne]

Synthèses de sujets scientifiques
---------------------------------

* `Acawiki <http://acawiki.org/Home>`_ [Site de 1400 synthèses sous licence CC]
* `Citizendium <http://en.citizendium.org/wiki/Welcome_to_Citizendium>`_ [Site de synthèses et blogs scientifiques, 17 000 articles]


Scientométrie
=============

Ce mot  signale tout procédé pour tenter d'évaluer la qualité des travaux (et également des chercheurs qui les ont réalisés). Il ne suffit bien sûr pas de lire des articles et ouvrages. Il faut avoir une idée précise de leur qualité. Nous avons inséré dans cette rubrique une catégorie d'outils de nombre croissant, ceux de détection de plagiat.

#. `Google Scholar <https://scholar.google.fr>`_ Google Scholar permet de se créer une page personnelle et d'inviter des co-auteurs.

Outils centrés sur les supports
*******************************

* `Journalysis <http://journalysis.org>`_
* `Linkage <https://linkage.fr>`_ (analyse en réseaux des publications à partir de HAL)
* `Quality Open Access Market <https://www.qoam.eu>`_
* `SCImago <http://www.scimagojr.com>`_ [Classement de revues]
* `SCEAS <http://sceas.csd.auth.gr/php/index.php4>`_
* `SemanticScholar <https://www.semanticscholar.org/>`_
* `Web of Science <http://login.webofknowledge.com>`_ [Site incontournable, mais en accès payant].
 

Réseaux sociaux académiques
***************************

Le calcul du facteur d'impact personnel, ainsi que le nombre de réseaux sociaux académiques a explosé ces dernières années, et amènent un nouveau calcul du facteur d'impact, plus social, nommé *altmetrics*. Une fois de plus, `Google Scholar <https://scholar.google.fr>`_ est l'outil central.

* `Academia.edu <https://www.academia.edu>`_
* `EduObs <https://ife.ens-lyon.fr/eduObs>`_ [Centré sur les chercheurs en sciences de l'éducation français]
* `ImpactStory <https://impactstory.org>`_ [Site, Gratuit]
* `Incend <http://www.incend.net/>`_
* `Loop <http://loop.frontiersin.org>`_ [Site, relié à l'éditeur Frontiers]
* `ResearcherID <http://www.researcherid.com/>`_ [Géré par Thomson ISI, a pour but de lever les homonymies entre chercheurs]
* `ResearchGate <https://www.researchgate.net/>`_
* `SHSDocNet <http://docnet.ish-lyon.cnrs.fr/>`_ [Réseau social académique des SHS en France]

.. _outils-IF:

Outils centrés sur le calcul du facteur d'impact
************************************************

Ces outils ou sites ne mettent pas en avant de dimension sociale ("suivi" de chercheurs), mais permettent l'accès aux productions académiques d'une personne donnée et à certaines mesures scientométriques (voir aussi Section :ref:`comm-labos`)

* `Altmetric <https://www.altmetric.com/audience/researchers/>`_ [Payant]
* `CIDS <http://cids.fc.ul.pt/cids_3_0/>`_ [Corrige le score de Google Scholar des auto-citations]
* `CiteSeer <http://citeseerx.ist.psu.edu>`_
* `iAmResearcher <http://iamresearcher.com/>`_ [réseau social de chercheurs, partage de publications]
* `Microsoft Academic Research <http://academic.research.microsoft.com/>`_
* `MyScienceWork <https://www.mysciencework.com/>`_
* `ORCID <http://orcid.org/>`_
* `Publish or Perish <http://www.harzing.com/resources/publish-or-perish>`_ [Windows/Linux]
* `QuadSearch <http://quadsearch.csd.auth.gr/index.php>`_ [Site, permet la comparaison d'auteurs]
* `Scholarometer <http://scholarometer.indiana.edu>`_ [Extension FireFox fondée sur Google Scholar]
* `Scholar H-index calculator <https://addons.mozilla.org/en-US/firefox/addon/scholar-h-index-calculator/>`_ [Extension FireFox, fondée sur Google Scholar]

Détecteur de plagiat
********************

* `Duplichecker <http://www.duplichecker.com>`_
* `Grammarly <https://www.grammarly.com/>_
* `Unplag <https://fr.unplag.com/free-plagiarism-checker/>`_
  
Analyser des contenus
*********************

* `Anatext (O. Kraif, Univ. Grenoble Alpes) <http://olivier.kraif.u-grenoble3.fr/index.php?option=com_content&task=view&id=48&Itemid=65>`_
* `ReaderBench <http://readerbench.com>`_
* `Voyant <https://voyant-tools.org/>`_

Analyser qualitativement des contenus
*************************************

* `QCAmap <https://www.qcamap.org>`_

.. _gerer_references:

Gérer ses références
====================

Cette section contient des outils de gestion de références bibliographiques qui comportent des fonctionnalités spécifiques à ce domaine : reconnaissance automatique des métadonnées, interface avec un logiciel de traitement de textes pour l'ajout automatique de références, interrogation de bases bibliographiques en ligne, etc. Nous distinguons ici deux grands types d'outils, selon qu'ils sont collaboratifs (souvent en ligne) ou individuels. Voir aussi cette `liste complète de logiciels de gestion de bibliographie <https://wiki.openoffice.org/wiki/Bibliographic/Software_and_Standards_Information>`_.

Bases bibliographiques locales 
******************************

Liste de quelques outils de gestion de références installables en local. `CitationStyles <http://citationstyles.org>` répertorie plus de 8 000 styles et de nombreux gestionnaires de références.

#. `Zotero <https://www.zotero.org>`_ [gratuit, extension FireFox et logiciel]
	
* `Bibdesk <http://bibdesk.sourceforge.net>`_ [gratuit, OS X]
* `Biblioscape <http://www.biblioscape.com>`_ [payant, Windows]
* `B3 <http://b3bib.sourceforge.net>`_ [gratuit, multiplateformes]
* `Citavi <http://www.citavi.com/en/>`_ [payant, Windows]
* `Docear <http://www.docear.org>`_ [open source, multiplateforme, cartes de concepts]
* `Mendeley <https://www.mendeley.com>`_ [gratuit, multiplateforme]
* `Colwiz <https://www.colwiz.com>`_ [gratuit, multiplateforme]
* `Papers <http://papersapp.com>`_ [payant, OS X]
* `PaperPile <https://paperpile.com>`_ [on line et stand alone]
* `Qiqqa <http://www.qiqqa.com/>`_ [gratuit, Windows, web, & Android] 
* `ReadCube <https://www.readcube.com>`_ [stand alone, gratuit, avec version pro payante]

Bases bibliographiques collaboratives
*************************************

#. `CiteUlike <http://www.citeulike.org/>`_

* `Bibliograph <http://cboulanger.github.io/bibliograph/>`_
* `Bibsonomy <http://www.bibsonomy.org>`_  [en ligne, gratuit]
* `Citelighter <https://www.citelighter.com>`_ [en ligne, gratuit/payant, avec des fonctionnalités de sélection/organisation des documents]
* `Citethisforme <https://www.citethisforme.com/fr>`_ [en ligne, gestionnaire de bibliographies et vérification du plagiat, freemium]
* `Heurist <http://heuristnetwork.org>`- [en ligne ou à installer sur serveur, spécialisé sciences humaines]
* `Mendeley <https://www.mendeley.com>`_   [gratuit, Existe également en version sur poste. Voir aussi ReaderMeter, calculant des indices bibliométriques à partir de ses données]
* `PaperPile <https://paperpile.com>`_ [on line et stand alone, biblio et gestion de PDF]
* `Researchr <http://researchr.org>`_ 
* `Wizfolio <http://wizfolio.com/>`_  [en ligne, freemium]

Outils de formatage de citations
********************************

* `EasyBib <http://www.easybib.com>`_
* `Text2bib <http://text2bib.economics.utoronto.ca>`_

.. _veille_ecrire:

--------------------------------------------------
Faire passer une expérimentation, un questionnaire
--------------------------------------------------

Si les outils de passation de questionnaire sont assez connus, ceux pouvant permettre de faire passer des expérimentations plus sophistiquées le sont moins. 

* `FramaForm <https://framaforms.org>`_

Equivalent
* `Expyriment <http://docs.expyriment.org/index.html>`_
* `OpenSesame <http://osdoc.cogsci.nl>`_
* `oTree <http://www.otree.org>`_
* `PsychoPy <http://www.psychopy.org>`_
* `SoSci Survey <https://www.soscisurvey.de/en/index>`_
* `Testable < https://www.testable.org>`_


------
Ecrire
------

Après avoir sélectionné et lu du matériel, il est souvent nécessaire de passer par une phase d'écriture (travail de synthèse, articles, livre, notes de cours). Cette phase est vraisemblablement la plus difficile des quatre, et il est important d'avoir à sa disposition des outils qui la facilitent. Nous en listons trois types : le "cahier de laboratoire", pour consigner des résultats de recherche, l'aide à l'organisation des idées (résumer), afin d'avoir une vue globale du document, puis l'écriture du document proprement dite.

Pré-enregistrer des études
==========================

Il est de plus en plus courant, et recommandé, de pré-enregistrer l'étude que l'on veut mener, afin d'éviter de possibles biais de recherche (biais de publication). Des revues encouragent cette procédure (liste à https://cos.io/rr/).

* `Open science framework (OSF) <https://osf.io>`_
* `AsPredicted <https://aspredicted.org>`_

Déterminer l'autorat
====================

Déterminer qui peut (ou ne peut) co-signer une recherche n'est souvent pas si simple. Des outils aident à le déterminer.

* `CRediT <https://casrai.org/credit/>`_

Consigner des résultats de recherche
====================================

N'importe quel outil de gestion de journal peut faire l'affaire. Il existe aussi quelques outils dédiés à la recherche.

* `Findings <http://findingsapp.com>`_ [OS X, payant]
* `LabNoteBook <https://www.labfolder.com>`_ [multiplateformes, payant]

Aide à l'organisation des idées
===============================

Logiciels de cartes de concepts
*******************************

#. `Freemind <http://freemind.sourceforge.net/wiki/index.php/Main_Page>`_ ou `FreePlane <http://freemind.sourceforge.net/wiki/index.php/Main_Page>`_ [multiplateformes, open source]

* `Page wikipedia logiciels mindmapping <https://en.wikipedia.org/wiki/List_of_concept-_and_mind-mapping_software>`_
* `Curio <http://www.zengobi.com>`_ [OS X, logiciel de carte de concepts et gestion de documents]
* `CmapTools <http://cmap.ihmc.us>`_ [éditeur de cartes conceptuelles multiplateforme et open source]
* `Docear <http://www.docear.org>`_ [open source, multiplateforme, gestion biblio]
* `Mindomo <https://www.mindomo.com/fr/>`_ [En ligne, freemium]
* `Mindmeister <https://www.mindmeister.com/fr>`_ [En ligne, freemium]
* `MindManager <https://www.mindjet.com>`_ [mindmapping, commercial, OS X et Windows],
* `Xmind <http://www.xmind.net>`_ [mindmapping multiplateforme et open source), 

Logiciels de gestion de plan
****************************

* `Page Wikipédia outliners <http://en.wikipedia.org/wiki/Outliner>`_
* `Fargo.io <http://fargo.io>`_ [site, gratuit, connecté à Dropbox]
* `Omni Outliner <http://fargo.io>`_ [OS X]
* `Writing Outliner <http://writingoutliner.com>`_ [Add-on Microsoft Word]


Analyser des données
====================

Outils de transcription
***********************

* `Sonal [Windows] <http://www.sonal-info.com/en/page/welcome>`_

Outils d'analyse statistique
****************************

* `JAMOVI <https://www.jamovi.org>`_
* `JASP <https://jasp-stats.org>`_
* `RStudio <https://www.rstudio.com>`_

Ecrire un document
==================

Cette section s'intéresse à des logiciels un peu plus spécifiques que `LibreOffice <http://fr.libreoffice.org>`_ ou MS Word à la rédaction de documents académiques, bien que ces derniers rendent déjà de très grands services. Elle distingue les logiciels de type "cartes conceptuelles" (mind ou concept mapping), organisant graphiquement des idées ou concepts sur une page, des logiciels de type plan (organisant des sections de documents de manière hiérarchique), et enfin des logiciels généralistes, ajoutant souvent des fonctionnalités spécialisées à une base de type traitement de textes.

Outils Généralistes
******************* 

Souvent orientés écriture de fictions ou scénarios)

#. `Scrivener <http://www.literatureandlatte.com/scrivener.php>`_ [OS X & Windows, payant]

* `FoldingText <http://www.foldingtext.com>`_ [OS X, payant]
* `Gingko <https://gingkoapp.com>`_ [Web, gratuit pour 3 fichiers]
* `Growly Notes <http://growlybird.com/notes/index.html>`_ [OS X & iOS, Gratuit]
* `Ulysses <http://www.ulyssesapp.com>`_ [OS X & iOS, payant]
* `Liquid Story <http://www.blackobelisksoftware.com>`_ [Windows, payant]
* `Manuscript <http://www.manuscriptsapp.com>`_ [OS X, payant]
* `MS OneNote <https://www.onenote.com>`_ [OS X, iOS & Windows, gratuit] 
* `Notabene <https://www.notabene.com>`_ [Windows, payant]
* `PageFour <http://www.softwareforwriting.com/pagefour.html>`_ [Windows, payant]
* `Quiver <http://happenapps.com/#quiver>`_ [OS X, payant, gère des portions de textes]
* `TreeSheets <http://strlen.com/treesheets/>`_ [Multiplateformes, libre et gratuit, combine outliner et mindmap]
* `Shelfster <http://shelfster.com>`_ [Multiplateformes et web, gratuit. Collecter des bribes de documents pour écrire un article]
  
Outils orientés documents structurés
************************************

Logiciels permettant l'édition de documents structurés, parfois plus difficiles à utiliser que les suivants, plus grand public. L'initiative `ScholarlyMarkdown <http://scholarlymarkdown.com>`_, prometteuse mais encore en version alpha, va peut-être permettre l'édition aisée de documents académiques.

* `Dexy <http://www.dexy.it>`_
* `DocBook <http://www.docbook.org>`_
* `Makodo <http://madoko.codeplex.com>`_
* `Markdeep <http://casual-effects.com/markdeep/>`_
* `Sphinx <http://www.sphinx-doc.org/>`_ [Le logiciel utilisé pour produire cette page]
* `LaTeX <http://www.ams.org/publications/authors/tex/amslatex>`_
* `LeanPub <https://leanpub.com/blog/2014/04/github-integration.html>`_
* `LyX <https://www.lyx.org>`_ [Surcouche de LaTeX]
* `Rinohtype <https://github.com/brechtm/rinohtype>`_
* `ScholarlyMarkdown <http://scholarlymarkdown.com>`_


Outils orientés écriture de romans
**********************************

* `Celtx <https://www.celtx.com/index.html>`_ [Multiplateformes, orienté écriture de scripts de cinéma]
* `LiquidStory Binder XE <http://www.blackobelisksoftware.com>`_
* `Novlr <http://novlr.org>`_ [web, payant]
* `StoryMill <http://marinersoftware.com/products/storymill/>`_ [OS X, payant]
* `Writer's Cafe <http://www.writerscafe.co.uk>`_ [Multiplateformes, payant]
* `WriteWay Pro <http://www.writewaypro.com>`_ [Windows]
* `WriteItNow <http://www.ravensheadservices.com>`_ [Windows]
* `yWriter5 <http://www.spacejock.com/yWriter5.html>`_ [Windows]

Outils d'écriture collaborative
*******************************

* `EtherPad <http://etherpad.org>`_
* `Google Docs <https://docs.google.com>`_
* `Lens writer <http://cdn.substance.io/lens-writer/>`_
* `Overleaf <https://www.overleaf.com>`_
* `SciFlow <https://www.sciflow.net/>`_
* `Stenci.la <https://stenci.la>`_
* `Substance <http://substance.io>`_
  
Aide à la traduction
********************

* `Deepl <https://www.deepl.com/translator>`_

.. _veille_diffuser:

--------
Diffuser
--------

Une fois qu'un article ou un ouvrage est écrit, il s'agit de faire en sorte que le public le plus large puisse en prendre connaissance.

Outils de choix de supports
===========================

Ces outils permettent de choisir la revue la plus appropriée, en fonction de son processus de relecture.

* `Jane Journal-Author Name Estimator <http://jane.biosemantics.org/index.php>`_
* `SciRev <https://scirev.org>`_


Communiquer avec son laboratoire
================================

Ces différents outils permettent le partage de documents, de tâches, de calendriers entre membres d'un laboratoire. Ils peuvent aussi être utilisés pour organiser un projet de recherche. Les logiciels de travail collaboratif classiques (comme Google Docs) conviennent aussi.

* `Flowdock <https://www.flowdock.com>`_ [site, payant, centré sur chat]
* `Licorize <https://licorize.com>`_ [Site, payant, partage de signets et de tâches au sein d'une équipe]
* `Open Science Framework <https://osf.io/#!>`_ [Gratuit, site de collaboration entre chercheurs d'un même projet, permet de stocker des archives]
* `Producteev <https://www.producteev.com>`_ [site et applications, payant, centré sur courriels]
* `Slack <https://slack.com>`_ [site, payant]
* `Trello <https://trello.com>`_
* `Wiggio <https://wiggio.com/?>`_ [site, gratuit, gestion de courriels, tâches, calendriers, sondages, etc.]

.. _comm-labos:

Communiquer avec les autres laboratoires
========================================

Même type d'outils que précédemment, mais ciblés sur le monde académique. Contient parfois des outils de partage d'article et/ou de gestion bibliographique (Voir aussi  la Section :ref:`outils-IF`), mais plus orienté "événements" (cours, colloques, etc.) que les précédents.

* `Guaana <https://www.guaana.com/>`_ [Connecte des chercheurs à partir de projets]
* `Labroots <http://www.labroots.com>`_
* `OpenWetWare <http://openwetware.org/wiki/Main_Page>`_
* `Peerevaluation <http://www.peerevaluation.org>`_ [site, gratuit, partage d'article, annotation par les pairs]
* `Sciweavers <http://www.sciweavers.org>`_ [Outil de partage de signets et d'articles]

Promouvoir travaux et événements
================================

Cette section s'intéresse aux moyens de disséminer ses travaux de recherche, par le biais de conférences, de postage dans des archives ouvertes, etc. Il s'agit, d'abord, d'être informé des multiples appels à propositions (CfP ou *call for papers*), de pouvoir organiser un colloque, et enfin de gérer ses propres articles soumis.


Outils de communication avec les participants
*********************************************

* `Ethicadata <https://www.ethicadata.com>` [Diffusion de questionnaires et suivi de la recherche]

Outils de gestion d'appels à propositions et de colloques
*********************************************************

* `Academic Odysci <http://academic.odysci.com>`_ [Egalement base d'articles et d'auteurs]
* `ConferenceAlerts <http://conferencealerts.com>`_
* `EventBrite <https://www.eventbrite.com>`_ [Non focalisé sur l'académique]
* `EventSeer <http://eventseer.net>`_
* `Lanyrd <http://lanyrd.com>`_ [A été racheté par EvenBrite]
* `OurgLocal <http://www.ourglocal.com>`_
* `Papers Invited <http://www.papersinvited.com>`_
* `WikiCFP <http://wikicfp.com/cfp/>`_

Outils de gestion de colloques
******************************

#. `Easychair <http://easychair.org>`_ : [Site, gratuit] outil très complet de gestion de toutes les étapes d'organisation d'un colloque, de la soumission à la collation des actes 

* `SciencesConf <https://www.sciencesconf.org>`_ : Idem, promu par le CNRS [Site, Gratuit]

Outils de gestion des articles soumis
*************************************

* `Published! <https://www.published.com>`_ [OS X, payant] Base de données pour gérer ses articles. Plutôt pour écrivain professionnel que pour chercheur.

Outils de visualisation scientométrique
***************************************

* Sci2Tool <https://sci2.cns.iu.edu/user/index.php>`_ [Permet d'analyser les collaborations à l'intérieur d'un laboratoire]

.. _diffuser-travaux:

Diffuser ses travaux
====================

Cette section décrit quelques outils permettant de diffuser ses travaux au plus grand nombre, que ce soient des articles ou bien des présentations. Enfin, les blogs sont également un bon moyen de diffuser ses travaux (voir Section :ref:`recommander`). Nous avons écarté de la liste les nombreux sites de publication grand public à la demande.

Archives et dépôts d'articles
*****************************

#. `HAL <https://hal.archives-ouvertes.fr>`_ : Publication en ligne de productions scientifiques (articles, cours) [Site, gratuit]

* `arXiv <http://arxiv.org>`_ : Prépublication en sciences de la nature et informatique.
* `CEUR <http://ceur-ws.org>`_ : Publication en ligne d'actes de conférences [Site, gratuit]
* `CoRR <http://arxiv.org/corr/home>`_ : Computing Research Repository, spécialisé dans la recherche en informatique, intégré à Arxiv [Site, gratuit]
* `PoS <http://pos.sissa.it>`_ : Actes de conférences [Site]
* `PubZone <http://pubzone.org>`_ [Site, gratuit, permet de déposer et de faire commenter ses publications par des pairs]
* `Zenodo <http://zenodo.org>`_

Partage de données expérimentales
*********************************

* `Dataverse <http://dataverse.org>`_
* `Dryad <http://www.datadryad.org>`_ [payant]
* `Myexperiment <http://www.myexperiment.org/home>`_
* `Visa <http://visa.espe-bretagne.fr>`_ [base de données de films de situations scolaires]

Dépôts de présentations ou de figures
*************************************

* `AuthorSTREAM <http://www.authorstream.com>`_ [Site, gratuit]
* `Slideshare <http://www.authorstream.com>`_ [Site, gratuit]

Commentaires par les pairs
**************************

Il existe de plus en plus de sites qui proposent une relecture d'articles par les pairs. `Ce document <https://docs.google.com/document/d/1HD-BEaVeDdFjjCNFkb0j3pvwe7MrP3PtE-bWHkkdq7Q/edit>`_  en fait une revue exhaustive.

Blogs
*****

Les critères de choix du site de blog peuvent être les suivants : présence ou non de publicité, licence sur laquelle sont publiés les billets. Il existe quantité de moteurs de blog statiques permettant de créer un blog sans être tributaire de sites commerciaux.

#. Hypotheses.org <http://hypotheses.org>`_ [Site, gratuit. Plateforme de carnets de recherche en sciences humaines]

* `Science Blogs <http://scienceblogs.com>`_ [Site, gratuit, avec publicité]
* `WordPress <https://fr.wordpress.com>`_ [Site, gratuit]

Références
==========

Voici quelques sites référençant des ressources sur la veille scientifique.

* `Research Tools (N. Ale-Ebrahim) <https://www.mindmeister.com/fr/39583892/research-tools-by-nader-ale-ebrahim>`_
