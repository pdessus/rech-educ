.. https://koreatesol.org/sites/default/files/pdf/QuickGuide-to-Journal-Article-Writing.pdf

.. Faire un tuto sur les présentations en utilisant notamment : http://homosociabilis.blogspot.com/2017/10/recette-miracle-pour-une-bonne.html

.. _tuto_organiser_memoire:

***********************************************
Tutoriel -- Organiser la rédaction d'un mémoire
***********************************************

.. index::
	single: auteurs; Dessus, Philippe

.. admonition:: Information

 * **Auteur** : `Philippe Dessus <http://pdessus.fr>`_, Espé & LaRAC, Univ. Grenoble Alpes).

 * **Date de création** : Janvier 2015, modifié en janvier 2017.

 * **Date de modification** : |today|.

 * **Statut du document** : Terminé. 

 * **Résumé** : Ce tutoriel donne des conseils sur la manière d'organiser son travail de rédaction de mémoire pour qu'il soit optimal, notamment avec l'aide de moyens informatisés. **Attention : ce tutoriel ne peut se substituer aux conseils d'un-e directeur-e de recherche**.

 * **Voir aussi** : Le doc. :ref:`veille_peda`, le :ref:`tuto_problematique`, le :ref:`tuto_recueil-donnees`.

 * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

Introduction
============

L'organisation du travail comprend nécessairement une phase de travail avec ordinateur. Toutefois, selon les cas, il est possible de garder une part non négligeable du travail *via* papier (*e.g.*, cahiers). Recueillir des données est un long aller-retour entre lecture, prise de notes et commentaires de ces notes. Aller-retour qu’il est impossible de décrire en détail, tant il dépend des habitudes de travail de chacun. Certains vont saisir directement leurs notes sur ordinateur, d’autres passeront par les fiches ou le cahier.

Un premier conseil est de recueillir le matériau sans trop se demander s’il aura ou non une place définitive dans le mémoire, sans trop filtrer ni se censurer : il sera toujours possible d'« écrémer » par la suite.

Faire une rapide revue de la question
=====================================

Avant d’aborder la méthode de recueil du matériau préconisée, attardons-nous un instant sur la source de ce matériau. Il sera préférable de commencer par un « état des lieux », en lisant des ouvrages généraux : encyclopédies, dictionnaires spécialisés, ouvrages des collections *Que sais-je ?* (P.U.F.), ou *Repères* (La Découverte), et en discutant avec des experts du domaine (notamment, bien sûr, le/la directeur/trice de mémoire). 

Cette première approche doit permettre d’établir une bibliographie des revues et ouvrages majeurs du domaine traité, ainsi qu’une vue d’ensemble des problématiques du domaine d’étude. S’ensuit une lecture rapide de ces ouvrages, en vue de repérer les concepts, théories, expérimentations, méthodes, citations, etc., en vigueur dans le domaine. Ces différentes données seront engrangées dans les documents listés ci-dessous et donneront lieu à un plan, soumis au/à la directeur-trice de mémoire. Les logiciels mis en œuvre dans le recueil des données jouent un rôle essentiel. Voici comment ils peuvent s’intégrer à votre recherche. 

Les informations complémentaires données dans le document :ref:`XXXX` sur la manière de lire et prendre des informations sur les recherches pourront être utiles.


S'organiser pour travailler
===========================

Flux de travail avec ordinateur
-------------------------------

Rappelons le premier précepte de l’introduction :  *Chaque donnée à sa place et une place pour chaque donnée*. Il est en effet inutile de dupliquer les données recueillies. La saisie se réalisera, autant que faire se peut, directement sur clavier. Il convient de concevoir des outils adéquats pour pouvoir retrouver aisément les données saisies, comme son logiciel de traitement de textes préféré. 

Il sera utile de constituer au moins quatre fichiers, qu’ils soient électroniques ou sur papier (voir plus bas) :

* un fichier « références bibliographiques », comprenant les références de tous les ouvrages consultés, même brièvement, de façon à constituer ultérieurement l’annexe du même nom. Un fichier sur logiciel de traitement de texte convient tout à fait et les possibilités de « tri » permettront aisément de réaliser cette partie. Attention à bien noter les références de tous les ouvrages ou articles lus : un document sans référence est un document inutilisable.

* un fichier « citations et figures », comportant les extraits d’ouvrages, reproduits texto. Il est primordial de n’en changer aucun mot. Si le logiciel utilisé le permet, on insérera dans ce fichier les figures (graphiques, organigrammes, etc.) glanées çà et là. Sinon, on créera un fichier de type logiciel de traitement de texte séparé, « figures ». Attention, ici aussi, à ne pas oublier de noter le ou les auteur(s) des citations.

* un fichier « résumés », comprenant des fiches résumant les principaux ouvrages et articles lus. On veillera à décrire le plus fidèlement possible les détails des recherches ou des raisonnements. On pourra introduire sur ces fiches des éléments d’appréciation (à utiliser, inintéressant, à voir plus tard, etc.).

* un fichier « données de recherche », où l’on recueillera, là aussi très méticuleusement, les données, brutes, puis traitées, de ses propres recherches.


Flux de travail avec ordinateur en fin
--------------------------------------

La méthode nommée `bullet journal <http://bulletjournal.com>`_ (BJ) est simple et efficace, et donc tout à fait appropriée à la collation de données de recherche (voir plus d'informations sur le site en lien, et une `description française du BJ <http://www.ellybeth.fr/2014/03/sorganiser-avec-le-bullet-journal.html>`_).

Organiser le plan
=================

Au fur et à mesure de la saisie de ces données, il faudra sélectionner et classer de manière à alimenter le plan du mémoire. Deux stratégies sont possibles : 

* commencer par le plan et raffiner progressivement le contenu (stratégie *top-down* ou dirigée par le but) ou bien 
* commencer par collecter les données et en venir au plan (stratégie *bottom-up* ou dirigée par les données). En règle générale, on alternera entre l’une et l’autre stratégie au cours de cette phase.

Ici, nous ne pouvons donner que des indications générales, tant la forme du plan peut varier en fonction du sujet, du directeur, du jury, de la méthode, etc. Il conviendra donc de créer deux autres fichiers, de type « logiciel de traitement de textes » :

* un fichier « Plan », comprenant les différentes étapes du mémoire. Il faut bien noter que ce fichier sera amené à être segmenté, lorsque le mémoire aura pris plus d’ampleur. Il pourra donc être saisi sur un logiciel de gestion d’idées (ou gestionnaires de plans).

* un fichier « Revue de question » pourra être utile dans le cas d’un mémoire comprenant une (ou plusieurs) revue(s) de question, cette partie ayant très souvent une vie autonome par rapport au reste du mémoire.

Enfin, deux derniers fichiers pourront rendre les plus grands services, là encore, il peut s’agir de fichiers électroniques ou sur papier :

* un fichier « À faire », comportant les différentes tâches à réaliser, avec mention des dates-butoir ;

* un fichier « Journal » dans lequel le déroulement de l'activité de recherche, chronologiquement et de façon plus distanciée, est transcrit régulièrement. Le contenu de ce journal pourra éventuellement figurer en abrégé dans une Annexe du mémoire.

Il existe de nombreux logiciels gérant les plans (*outliners*) ou les cartes de concepts (*mindmaps*), pouvant être particulièrement utiles pour structurer votre travail, voir le document :ref:`ressources_veille`.

La Figure 1 ci-dessous représente ces différentes étapes.

Figure 1 - Organisation et provenance des différents types de données pour l'écriture d'un mémoire.

.. image:: images/org_donnees.jpg
	 :scale: 50 %
	 :alt: Organisation et provenance des différents types de données
	 :align: center


