.. _tuto_references_apa:

******************************
Tutoriel -- Les références APA
******************************

.. index::
    single; auteurs; Le Hénaff, Benjamin

.. admonition:: Informations

   * **Auteur** : `Benjamin Le Hénaff <https://www.researchgate.net/profile/Benjamin_Le_Henaff>`_, LaRAC, Univ. Grenoble Alpes.

   * **Date de création** : Juin 2018.

   * **Date de modification** : |today|.

   * **Statut du document** : Terminé.

   * **Résumé** : Ce document présente l'essentiel des normes des références bibliographiques de la 6\ :sup:`e`  édition du manuel de l'*American Psychological Association*, très usitées dans le domaine de la psychologie et plus largement des sciences humaines.

   * **Voir aussi** : Les :ref:`tuto_tab-fig_apa`, :ref:`tuto_problematique`, le :ref:`tuto_recueil-donnees`, et le :ref:`tuto_organiser_memoire`. 

   * **Licence** : Ce document est placé sous licence *Creative Commons* : `BY-NC-SA <https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr>`_.



Avertissement
=============

Avant toute chose, il est évident que ce petit document ne remplace en aucun cas le manuel APA 6\ :sup:`e` édition (American Psychological Association, 2010), d’où sont extraites ces informations. Il a pour vocation d’être un petit aide-mémoire pour les occurrences les plus fréquentes de normes APA que l’on peut rencontrer lors de la rédaction d’une communication (*e.g.*, article, conférence, …), d’une thèse, ou d’un mémoire. Néanmoins, pour les règles moins fréquentes ou plus précises, je ne peux que vivement vous encourager à vous procurer le manuel dans son intégralité.

Je souhaite en profiter pour remercier les personnes qui m’ont aidé dans la rédaction de ce document, à savoir Camille Sanrey (Ph.D., LaRAC (EA 602), Université Grenoble Alpes), Philippe Dessus (Professeur, LaRAC) et Anne-Laure de Place (Ph.D., LP3C (EA 1285), Université Rennes 2) pour les relectures, commentaires et éléments qui ont été intégrés à ce document. Merci également aux membres de mon laboratoire, qui au bout de la 10\ :sup:`e` relecture APA m’ont motivé à écrire ce mémo.


Quelques règles de base à suivre obligatoirement
================================================

1)	Si vous citez quelque chose (un article, un résultat, un outil, un concept, une phrase…), *il faut obligatoirement citer la source*.

* Sinon, c’est du plagiat ! Y compris si vous faites une paraphrase (*i.e.*, que vous modifiez la phrase mais gardez l’idée d’un ou de plusieurs auteurs).
* De plus, donner une référence donne du crédit à cet élément que vous apportez, permettant au lecteur de savoir si ce que vous avancez possède une base (plus ou moins) solide, ou si vous le sortez de votre chapeau.

2)	Les références (dans le texte comme dans la bibliographie) sont toujours d’abord dans l’ordre alphabétique, puis chronologique en cas de redondance alphabétique (par exemple, deux fois le même auteur).

3)	Avant de soumettre votre document (à une revue, aux organisateurs d’un colloque, à votre directeur.trice de thèse…) faites au moins deux relectures APA.

* La première pour vous assurer que toutes les citations dans le texte se retrouvent bien dans la bibliographie finale.
	
	* Mon conseil ici est d’imprimer votre bibliographie, et de relire votre texte. À chaque référence rencontrée, vous la cochez dans votre bibliographie. Cela permet trois choses : 1) vous repérez immédiatement si c’est la première fois ou non que cette référence apparait dans le texte (ce qui change des choses par rapport aux « et al. », voir plus loin), 2) vous repérez de suite si une référence est manquante et 3) une fois que vous avez fini, un seul tour de votre bibliographie permet de voir si elle contient des références en trop [#]_.

* La seconde, plus formelle, pour vérifier que vous avez bien suivi les règles de formatage : ordre alphabétique puis chronologique, l’usage des bons signes de ponctuation, le bon usage de l’italique… 

Mon ordinateur peut-il faire l’APA à ma place ?
===============================================

Oui et non. Il existe effectivement des logiciels de gestion de normes bibliographiques qui facilitent le travail considérablement, bien que nécessitant toujours de sérieuses vérifications après coup. En effet, les logiciels font pas mal d’erreurs.

Cela veut-il dire que, à titre personnel, je recommande ce type de logiciel ? Clairement pas, non, ou alors seulement une fois que l’on a une réelle maitrise des normes. Comme souligné précédemment, il faut repasser derrière ces logiciels pour vérifier qu’il n’y a pas d’erreur, ce qui nécessite de déjà maitriser les normes. Or, on les maitrise plus vite en les utilisant et en les rédigeant à la main.

Si néanmoins vous ressentez le besoin de faire appel à des logiciels, j’en recommande deux : `Zotero <https://www.zotero.org>`_ (logiciel libre, donc éthiquement meilleur) et `Mendeley <https://www.mendeley.com>`_ (logiciel propriétaire gratuit, mais paraît-il plus efficace que Zotero). 

Références [#]_ dans le corps de texte, citations, et mots étrangers
====================================================================

Lorsque le besoin émerge de citer une référence (après avoir parlé d’une théorie, d’un outil, d’un concept, etc.), il est possible de les citer entre parenthèses ou hors parenthèses.

Références entre parenthèses
----------------------------

Entre parenthèses, vous placez la référence juste après l’élément avancé, sans interrompre le texte et sans l’intégrer dans la construction de la phrase
	
	* e.g., Citer des références entre parenthèses c’est chouette (Alpha & Beta, 2010).

La référence change en fonction du nombre de ses auteurs :

* Si deux auteurs ou moins, toujours citer tous les auteurs, séparés par un « & », avec la date de publication
	
	* e.g., Le respect des normes APA est bon pour la santé (Alpha & Beta, 2010). 

* Si trois, quatre, ou cinq auteurs
	
	* Première citation : citer tous les auteurs, le dernier séparé des autres par un « & », avec la date de publication. Il s’agit de la première citation de cette référence, que cela soit entre parenthèses ou en-dehors.
		
		* e.g., Les normes APA préviennent la chute des cheveux (Alpha, Beta & Gamma, 1965).
		* e.g., Les normes APA sont vos amies (Alpha, Beta, Gamma, Delta & Epsilon, 2016).
	
	* Citation suivantes : ne citer que le premier auteur, suivi de « et al. » et de la date de publication
		
		* e.g., pour les travaux de Alpha, Beta, Gamma, Delta et Epsilon (2016) : Les normes APA sont aussi votre famille d’adoption (Alpha et al., 2016)

* Si six auteurs ou plus
	
	* Dès la première citation, ne citer que le premier auteur, suivi de « *et al.* » et de la date de publication
		
		* e.g., pour les travaux de Alpha, Beta, Gamma, Delta, Epsilon et Zeta (2001) : L’auteur de ce mémo est à mourir de rire (Alpha *et al.*, 2001). 


Références hors parenthèses
---------------------------

Hors parenthèses, la référence doit être intégrée directement dans la construction de la phrase et mis en lien clair avec l’élément avancé.
	
	* e.g., Comme l’ont montré Alpha et Beta (2010), citer des références hors parenthèse c’est chouette aussi.

Là encore, la référence change en fonction du nombre de ses auteurs.

* Si deux auteurs ou moins, toujours citer tous les auteurs, séparés par un « et »/« and », avec la date de publication
		
		* e.g.¸ Les travaux de Alpha et Beta (2010)… 
		* e.g., En 2010, Alpha et Beta ont… 
		* e.g., Comme les travaux de Gamma, en 1985, ont montré… 

* Si trois, quatre, ou cinq auteurs
	
	* Première citation : citer tous les auteurs, le dernier séparé des autres par un « et »/« and » [#]_ , avec la date de publication. Il s’agit de la première citation de cette référence, que cela soit entre parenthèses ou en-dehors.
		
		* e.g., Les travaux de Alpha, Beta et Gamma  (1965)… 
		* e.g., En 2016, Alpha, Beta, Gamma, Delta et Epsilon ont… 
	
	* Citations suivantes : ne citer que le premier auteur, suivi de « et al. » et de la date de publication
		
		* e.g., pour les travaux de Alpha, Beta, Gamma, Delta et Epsilon (2016) : Les travaux de Alpha et al. (2016) ont montré… 

* Si six auteurs ou plus, ne citer que le premier auteur, suivi de « et al. » et de la date de publication, qu’il s’agisse de la première citation ou non
		
		* e.g., pour les travaux de Alpha, Beta, Gamma, Delta, Epsilon et Zeta (2001) : Les travaux de Alpha et al. (2001) ont montré… 

Cas particuliers
================

Publications par le(s) même(s) auteur(s) sur la même année
----------------------------------------------------------

Si un ou plusieurs auteurs ont publié plusieurs articles la même année et qu’ils sont cités dans votre document, ajouter des lettres après l’année pour les différencier, dans l’ordre d’apparition dans le texte. Ces lettres seront à rapporter correctement dans les références bibliographiques.
	
	* e.g., Comme le montrent les travaux de Alpha (2002a, 2002b, 2002c)… 

Que faire quand plusieurs références sont citées en même temps ?
----------------------------------------------------------------

* Lorsque les références citées sont regroupées dans le même enchainement (généralement entre parenthèses), c’est l’ordre *alphabétique* qui prime, suivi de l’ordre chronologique, séparé par des points-virgules (« ; ») [#]_.
	
	* e.g., … et ceci a été montré par de nombreux travaux (Alpha, 2005 ; Beta, 1989 ; Gamma & Delta, 2000, 2002, 2010).


Référence à une traduction d’un article ou d’un ouvrage
-------------------------------------------------------

* Si vous placez la référence d’une œuvre originalement écrite dans une langue étrangère puis traduite dans votre langue de rédaction, il faut indiquer la date de publication de l’œuvre originale puis la date de publication de sa traduction, séparées par une barre oblique « / »
	
	* e.g., comme le montrent les travaux de Klaus Kappa (1906/1958)…

Citations in texto
------------------

Si vous citez un extrait d’un autre document, il doit être placé entre guillemets[#]_ et la (ou les) page(s) où se trouvent les propos originaux doivent être ajoutées à la référence.
	
	* e.g., Ce concept est présenté comme étant « un concept trop révolutionnaire » (Alpha, 1991, p. 12).
	* e.g., Selon Alpha, en 1991, ce concept est « un concept trop révolutionnaire » (p. 12).

Mots étrangers
--------------

Si vous utilisez des mots d’origine étrangère n’ayant pas d’usage commun dans la langue de rédaction, ils doivent être placés en italique
	
	* e.g., Cette situation mène à un fort sentiment de *Schadenfreude*.
	* e.g., Personne n’a autant de *swag* que moi. 

Si vous utilisez des mots d’origine étrangère ayant un usage commun dans la langue de rédaction, ils ne sont pas placés en italique. Les latinismes couramment utilisés en science rentrent dans cette catégorie : e.g., et al., a priori, ad lib, per se, etc…
	
	* e.g., Les participants sont partis en week-end ensemble.
	* e.g., A minima, nous avons… 

Rédiger sa bibliographie
========================

**Important 1** : les références bibliographiques sont rangées dans l’ordre alphabétique des auteurs puis chronologique dans le cas de deux références par les mêmes auteurs.
	* e.g., Alpha (2016), Beta (1998) et Beta (2005) serait le bon ordre.

**Important 2** : dans les exemples qui suivent, les points, virgules, parenthèses, tirets et italiques sont capitaux et font partie des normes APA. Il faut donc les respecter.

Articles de revues
------------------

Si un seul auteur
`````````````````
COMMENTAIRE Je trouve les 2 premiers items redondants. Ne serait-il pas plus simple d'en mettre 1 des 2 ?
* Nom de l’auteur, Initiale du prénom [#]_. (Année de publication). Titre de l’article. *Nom de la revue*, *numéro de la revue*(numéro du magazine), numéro première page-numéro dernière page. DOI [#]_  de l’article
	
	* e.g., pour l’article d’Alphonse Alpha de 2016, intitulé « L’APA pour les Nuls », publié dans le volume 3 et magazine 12 de la revue « *Revue des normes APA* », couvrant les pages 5 à 18, et ayant 4578AX-12 comme DOI :
	* Alpha, A. (2016). L’APA pour les Nuls. *Revue des normes APA*, *3*(12), 5-18. DOI : 4578AX-12

Si plusieurs auteurs
````````````````````

* Nom de l’auteur 1, Initiale du prénom., Nom de l’auteur 2, Initiale du prénom 2., [répéter cet enchainement pour tous les auteurs] & Nom du dernier auteur, Initiale du dernier prénom. (Année de publication). Titre de l’article. *Nom de la revue*, *numéro de la revue*(numéro du magazine), numéro première page-numéro dernière page. DOI de l’article

	* e.g., pour l’article d’Alphonse Alpha, Bernard-Henri Beta et Gérard Gamma de 2015, intitulé « L’APA pour les Pros », publié dans le volume 4 et magazine 18 de la revue « *Journal des APAistes* », couvrant les pages 143 à 150, et ayant 4875VC-45 comme DOI :
	* Alpha, A., Beta, B.-H., & Gamma, G. (2015). L’APA pour les Pros. *Journal des APAistes*, *4*(18), 143-150. DOI : 4875VC-45

Livres
------
JE TROUVE PEU UTILE DE FAIRE LA DISTINCTION 1/plusieurs auteurs; peut-être seulement dire que la liste des auteurs est la même que pour les articles; ça évite des sous-sous-sections.

Si un seul auteur
````````````````` 

* Nom de l’auteur, Initiale du prénom. (Année de publication). *Titre du livre*. Lieu de publication : Nom de la maison d’édition.
	* e.g., pour le livre d’Alphonse Alpha de 1995, intitulé « *L’APA et moi* », édité à Paris par la maison d’édition « Les éditions APAistes » : 
	* Alpha, A. (1995). *L’APA et moi*. Paris : Les éditions APAistes.

Si plusieurs auteurs
````````````````````

* Nom de l’auteur 1, Initiale du prénom 1., Nom de l’auteur 2, Initiale du prénom 2., [répéter cet enchainement pour tous les auteurs] & Nom du dernier auteur, Initiale du dernier prénom. (Année de publication). *Titre du livre*. Lieu de publication : Nom de la maison d’édition [#]_.
	* e.g., pour le livre d’Alphonse Alpha, Bernard-Henri Beta et Gérard Gamma de 2018, intitulé « *APA d’problème* » édité à Kermoroc’h par la maison d’édition « Ker Embanner » :
	* Alpha, A., Beta, B.-H., & Gamma, G. (2018). *APA d’problème*. Kermoroc’h : Ker Embanner.

Livres traduits
---------------

Si vous faites référence à la version traduite d’un livre dont la version originale est dans une langue étrangère.

	* Nom de l’auteur, Initiale du prénom. (Année de publication de la traduction). *Titre traduit du livre* (Initiale du prénom du traducteur 1., Nom du traducteur 1., & Initiale du prénom du traducteur 2., Nom du traducteur 2, Trad.). Lieu de publication de la traduction : Nom de la maison d’édition de la traduction. (Réimpression de Titre du livre original, année de publication originale, lieu de publication original : Nom de la maison d’édition originale)
	* e.g., pour le livre « *Der APA* » de Klaus Kappa de 1906 édité à Berlin par « Der Verlag », traduit en français sous le titre « *L’APA* » en 1958 par Zébulon Zeta, chez « Dunod » à Paris :
	* Kappa, K. (1958). *L’APA* (Z. Zeta, Trad.). Paris : Dunod. (Réimpression de *Der APA*, 1906, Berlin : Der Verlag)

En anglais, « Trad. » est remplacé par « Trans. », et « Réimpression de » par « Reprinted from »

Livres réédités
---------------

Si vous faites référence à une édition précise d’un ouvrage ayant été réédité.

* Les précisions concernant l’édition d’un ouvrage doivent se rajouter après son titre.
* e.g., pour le livre de Alphonse Alpha, « Mettre le bon APA au bout de sa ligne », qui en est à sa 3\ <sup>e</sup>  édition qui est sortie en 2016, édité à Kermoroc’h par la maison d’édition « Ker Embanner », on mettrait donc :
Alpha, A. (2016). *Mettre le bon APA au bout de sa ligne* (3\ <sup>rd</sup> Edition). Kermoroc’h : Ker Embanner.

Chapitres de livre
------------------

Si un seul auteur
`````````````````

* Nom de l’auteur, Initiale du prénom. (Année de publication). Titre du chapitre. Dans Initiale du prénom du rédacteur en chef  1. Nom du rédacteur en chef 1, Initiale du prénom du rédacteur en chef 2. Nom du rédacteur en chef 2, [répéter cet enchainement pour tous les rédacteurs en chef] & Initiale du prénom du dernier rédacteur en chef. Nom du dernier rédacteur en chef (Eds.), *Titre du livre* (pp. première page du chapitre-dernière page du chapitre). Lieu de publication : Nom de la maison d’édition.
* e.g., pour le chapitre d’Alphonse Alpha, intitulé « Retourne à ton APAnier », extrait du livre « *Jeux de mots et tricot* » dirigé par Bernard-Henri Beta et Gérard Gamma, couvrant les pages 15 à 26, édité en 1956 à Lannion par la maison d’édition « Funiculaires » :
* Alpha, A. (1956). Retourne à ton APAnier. Dans G.-H. Beta & G. Gamma (Eds.), *Jeux de mots et tricot* (pp. 15-26). Lanion : Funiculaires.

Si plusieurs auteurs
```````````````````` 
* Nom de l’auteur 1, Initiale du prénom 1., Nom de l’auteur 2, Initiale du prénom 2., [répéter cet enchainement pour tous les auteurs] & Nom du dernier auteur, Initiale du dernier prénom. (Année de publication). Titre du chapitre. Dans Initiale du prénom du rédacteur en chef [#]_ 1. Nom du rédacteur en chef 1, Initiale du prénom du rédacteur en chef 2. Nom du rédacteur en chef 2, [répéter cet enchainement pour tous les rédacteurs en chef] & Initiale du prénom du dernier rédacteur en chef. Nom du dernier rédacteur en chef (Eds.), *Titre du livre* (pp. première page du chapitre-dernière page du chapitre). Lieu de publication : Nom de la maison d’édition13.
* e.g., pour le chapitre d’Alphonse Alpha Bernard-Henri Beta et Gérard Gamma, intitulé « It’s fun to stay at the A.P.A. », extrait du livre « *Laughters & Songs* » dirigé par Bernard-Henri Beta et Gérard Gamma, couvrant les pages 54 à 72, édité en 2001 à San Francisco par la maison d’édition « Funiculars » :
* Alpha, A., Beta, B.-H., & Gamma, G. (2001). It’s fun to stay at the A.P.A. In G.-H. Beta & G. Gamma (Eds.), *Laughters & Songs* (pp. 54-72). San Francisco: Funiculars.

Pour un livre traduit
````````````````````` 
* Nom de l’auteur, Initiale du prénom. (Année de publication de la traduction). Titre traduit du chapitre livre (Initiale du prénom du traducteur 1., Nom du traducteur 1., & Initiale du prénom du traducteur 2., Nom du traducteur 2, Trad.). Dans Initiale du prénom du rédacteur en chef 1. Nom du rédacteur en chef 1, Initiale du prénom du rédacteur en chef 2. Nom du rédacteur en chef 2, [répéter cet enchainement pour tous les rédacteurs en chef] & Initiale du prénom du dernier rédacteur en chef. Nom du dernier rédacteur en chef (Eds.), *Titre du livre traduit* (pp. première page du chapitre-dernière page du chapitre). Lieu de publication de la traduction : Nom de la maison d’édition de la traduction13. (Réimpression de Titre du livre original, pp. première page du chapitre-dernière page du chapitre, par Initiale du rédacteur en chef original., Nom du rédacteur en chef original, Eds., année de publication originale, lieu de publication original : Nom de la maison d’édition originale)

* e.g., pour le chapitre « Ich mag Würstchen » de Olga Omicron du livre « *Der APA* » de Klaus Kappa de 1906 édité à Berlin par « Der Verlag » et couvrant la page 12 à la 17, livre traduit en français sous le titre « *L’APA* » et chapitre traduit en français par « *L’APA c’est bien même en allemand* » en 1958 par Zébulon Zeta, chez « Dunod » à Paris, couvrant les pages 58 à 61 :
* Omicron, O. (1958). L’APA c’est bien même en allemand (Z. Zeta, Trad.). Dans K. Kappa (Eds.), *L’APA* (pp. 12-17). Paris : Dunod. (Réimpression de *Der APA*, pp. 58-61, par K. Kappa, Ed., 1906, Berlin : Der Verlag.
* En anglais, « Trad. » est remplacé par « Trans. », « Réimpression de » par « Reprinted from », et « Dans » par « In »

Thèses
`````` 
Les thèses de doctorat tout comme les mémoires de master suivent les mêmes règles. Considérons deux cas. Premièrement, si ce document est disponible dans une base de données (type theses.fr).

* Nom de l’auteur, Initiale du prénom. (Année de soutenance). Titre du document (Type de document). Repéré sur URL.
* e.g., pour la thèse de Murielle Mu, soutenue en 2015, sous le titre « *Être une APAtride* », publiée sur le site « Viens, voir, viens voir le Docteur » :
* Mu, M. (2015). *Être une APAtride* (Thèse de doctorat). Repéré sur http://www.viensvoirviensvoirledocteur.fr/4269/muriellemu2015

Si ce document n’est pas disponible dans une base de données :

* Nom de l’auteur, Initiale du prénom. (Année de soutenance). *Titre du document* (Type de document). Nom de l’institution de rattachement, Lieu de l’institution.
* e.g., pour le mémoire de master de Philippe Phi, soutenu en 2014, sous le titre « *Où t’es, APA où t’es ?* », soutenue à l’Université de Troarn, France :
* Phi, P. (2014). *Où t’es, APA, où t’es* ? (Mémoire de master). Université de Troarn, France.

Sources sur Internet
````````````````````

Dans le cas d’un article ou d’un livre, voir les sections correspondantes. La seule différence repose sur le fait qu’à la fin de la référence (mais avant le DOI, si DOI il y a), il faut rajouter la mention « Retrieved from »/ « Repéré sur » suivi de l’adresse du lien.

* e.g., pour l’article d’Alphonse Alpha de 2016, intitulé « *APA trouvé* », publié sur le site « Cherche mieux ! » :
* Alpha, A. (2016). *APA trouvé*. Cherche mieux ! Repéré sur http://www.cherchemieuxpointdexclamation.fr/1412/apatrouve.htm



.. [#] 4) Comme ça ne demande que peu de ressources cognitives, vous pouvez le faire en regardant un film ou en attendant que les pâtes cuisent.
.. [#] Dans cette section, le terme « auteur » sera utilisé au sens large. Ainsi, il peut s’agir d’une personne, mais aussi d’un organisme (e.g., PISA, Commission Européenne, etc.).
.. [#] Rappel : en français, le « et » final d’une liste n’est jamais précédé d’une virgule. En anglais, en revanche, cette virgule est recommandée (ce que l’on appelle la « `virgule d’Oxford <https://fr.wikipedia.org/wiki/Virgule_de_série>`_ »). Comme moi, vous pouvez militer pour la virgule d’Oxford en français…
.. [#] Rappel : en français, le « ; » est précédé et suivi d’une espace ; en anglais, il n’est *pas* précédé d’une espace, mais est bien suivi d’une espace. En passant, il s’agit bien « d’une » espace, et non « d’un » espace !
.. [#] Rappel : en français, nous utilisons les guillemets français (« guillemets »), alors qu’en anglais il faut utiliser les guillemets anglais (“*quotation marks*”).
.. [#] Si prénom il y a. Un organisme peut-être référencé comme auteur (e.g., PISA, Commission Européenne…), auquel cas on met seulement le nom de l’organisme.
.. [#] Pour *Digital Object Identifier*, il s’agit d’un code alpha-numérique ou d’une adresse web permettant de localiser un article. Il se trouve généralement sur l’article lui-même ou sur sa page web sur le site de l’éditeur. Les vieux articles n’ont pas tous un DOI.
.. [#] Si vous avez récupéré l’œuvre sur un site le mettant officiellement à disposition (i.e., pas le site où vous l’avez piraté…), il faut rajouter un lien internet suivi de la mention « Retrieved from »/ « Repéré sur ». De même, si le DOI est disponible, il faut le rajouter, après l’URL.
.. [#]  i.e., la personne qui a dirigé la rédaction de l’ouvrage ; généralement il s’agit du ou des nom(s) sur la couverture, souvent précédé de la mention « sous la direction de ».
