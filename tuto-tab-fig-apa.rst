
.. _tuto_tab-fig_apa:

***********************************
Tutoriel -- Tableaux et figures APA
***********************************

.. index::
    single; auteurs; Le Hénaff, Benjamin

.. admonition:: Informations

   * **Auteur** : `Benjamin Le Hénaff <https://www.researchgate.net/profile/Benjamin_Le_Henaff>`_, LaRAC, Univ. Grenoble Alpes.

   * **Date de création** : Juin 2018.

   * **Date de modification** : |today|.

   * **Statut du document** : Terminé.

   * **Résumé** : Ce document présente l'essentiel des normes à propos de la présentation de tableaux et figures de la 6\ :sup:`e`  édition du manuel de l'*American Psychological Association*, très usitées dans le domaine de la psychologie et plus largement des sciences humaines.

   * **Voir aussi** : Les :ref:`tuto_references_apa`, :ref:`tuto_problematique`, le :ref:`tuto_recueil-donnees`, et le :ref:`tuto_organiser_memoire`. 

   * **Licence** : Ce document est placé sous licence *Creative Commons* : `BY-NC-SA <https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr>`_.

Tableaux
========

Le titre d'un tableau suit des règles précises :

* Il doit être numéroté, en partant de 1, et dans l'ordre [#]_ 
	* Les figures aussi sont numérotées en partant de 1, et les deux numérotations sont séparées. Il y a donc un Tableau 1 et une Figure 1.
* Il doit toujours être *au-dessus* du tableau
* Il doit être suffisamment détaillé et clair pour que sa lecture suffise à bien comprendre les informations contenues dans le tableau

La construction d'un tableau suit aussi certaines règles :

* Les colonnes et les lignes doivent être clairement nommées
* Seules les lignes horizontales séparant les sections doivent être visibles (par exemple, il faut une ligne séparant les titres des colonnes de leur contenu, mais les lignes séparant les entrées suivantes ne doivent pas être visibles)
* Aussi peu de lignes verticales que possible (l'idéal étant aucune)
* Si des notes ou des légendes sont nécessaires, il faut les rajouter à la fin du tableau, sous les colonnes

Voici un exemple de tableau aux normes APA :

.. figure:: images/apa-tab-1.jpg
	:scale: 70 %
	:alt: Tableau à la norme APA

Figures
=======

Le titre d’une figure suit des règles précises

* Il doit être numéroté, en partant de 1, et dans l’ordre
	* Les tableaux aussi sont numérotés en partant de 1, et les deux numérotations sont séparées. Il y a donc un Tableau 1 et une Figure 1.
* Il doit toujours être *en-dessous* de la figure
* Il doit être suffisamment détaillé et clair pour que sa lecture suffise à bien comprendre ce que la figure contient comme information
* Il doit également informer du type de figure présentée (boite à moustache, histogramme, courbe de régression, capture d’écran, schéma, etc.)

La construction d’une figure de type *graphique* suit aussi des règles très précises

* Tous les axes doivent avoir un titre
* La graduation doit être claire et lisible et adaptée aux données présentées (e.g., inutile de numéroter chaque modalité, de 5 en 5 ou de 10 en 10 peut par exemple suffire)
	
	* S’il existe des variations de couleur ou de type de trait utilisé, il faut obligatoirement le légender ; ces variations ne doivent être faites *que* si elles ont un sens
	* Conseil : attention aux couleurs utilisées, assurez-vous qu’elles ne gênent pas la lisibilité du texte ou des traits : pensez que votre document sera vraisemblablement imprimé tôt ou tard en noir et blanc, donc utilisez des couleurs et des traits qui, une fois en noir et blanc, restent clairement différenciables
	* Conseil : attention également au sens des couleurs. Il est de très mauvais goût, par exemple et surtout en sciences humaines, d’utiliser du rose pour les femmes et du bleu pour les hommes.

* Certains logiciels de statistique, notamment `R <http://www.r-project.org>`_, proposent des scripts permettant de générer des figures automatiquement aux normes APA (avec les mêmes précautions que pour les logiciels de gestion bibliographique ; voir l’introduction de ce document)
	
	* Exemple de script R permettant de réaliser des figues aux normes APA : https://rdrr.io/cran/jtools/man/theme_apa.html

Voici trois exemples de figures aux normes APA :

.. figure:: images/apa-fig-1.jpg
	:scale: 70 %
	:alt: Fig 1 à la norme APA

.. figure:: images/apa-fig-2.jpg
	:scale: 70 %
	:alt: Fig 2 à la norme APA



.. [#] Je sais, ça semble couler de source, mais au bout d'un moment on voit tellement de choses…
