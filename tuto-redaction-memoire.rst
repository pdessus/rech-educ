
.. _tuto_redaction_memoire:

*************************************
Tutoriel -- La rédaction d'un mémoire
*************************************

.. todo https://fr.wiktionary.org/wiki/maximes_de_Grice
.. https://thesiswhisperer.com/2017/06/28/8346/?platform=hootsuite
.. https://medium.com/advice-and-help-in-authoring-a-phd-or-non-fiction/how-to-write-paragraphs-80781e2f3054
.. http://www.dansimons.com/resources/Simons_on_writing_1.5.pdf


.. index::
    single; auteurs; Dessus, Philippe

.. admonition:: Informations

   * **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, Univ. Grenoble Alpes.

   * **Date de création** : Janvier 2015.

   * **Date de modification** : |today|.

   * **Statut du document** : Terminé.

   * **Résumé** : Ce document donne des conseils généraux pour la rédaction d'un mémoire de recherche en sciences humaines et sociales. Il est séparé en quatre sous-sections. La première décrit les différentes parties du mémoire. La deuxième s'intéresse au texte principal (le corps du mémoire). La troisième s'intéresse aux annexes, dont le contenu alourdirait le texte principal, mais est suffisamment importants pour être mentionné. La quatrième et dernière se centre sur quelques aspects formels de présentation. 
   
   * **Avertissement** : Se référer aux consignes propres à l'établissement ou au directeur (à la directrice) lorsqu'elles sont différentes de celles présentées ci-dessous.

   * **Voir aussi** : Le :ref:`tuto_problematique`, le :ref:`tuto_recueil-donnees`, et le :ref:`tuto_organiser_memoire`. 

   * **Licence** : Ce document est placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Pour paraphraser `George Burns <https://fr.wikipedia.org/wiki/George_Burns>`_ (qui parlait des sermons), un mémoire doit avoir un bon début, une bonne fin, et les deux doivent être aussi proches que possible. Mais peut-on en dire un peu plus ?

L'organisation du mémoire
=========================

Cette première section s'intéresse à l'organisation générale du mémoire. Elle est organisée selon les différentes parties : introduction, partie théorique, partie empirique, discussion.

Introduction
------------

L’introduction présente l’état de la recherche en quelques exemples-clés, tirés de la revue de question, puis expose le plan général du mémoire. Mieux vaut l’écrire quand tout le mémoire est terminé.

Partie théorique
----------------

La partie théorique définit le propos du mémoire, elle peut se séparer en plusieurs chapitres si les revues de question sont longues. Elle comprend 

* la (les) revue(s) de question, recensant les recherches sur le thème du mémoire et commentant leurs apports de façon critique. Il importe ici de présenter des recherches :

	* *significatives*, on présentera des travaux d’auteurs reconnus, publiés dans des revues non moins reconnues, ou bien des ouvrages importants. Ici encore, les conseils du directeur de mémoire sont capitaux. Autant que faire se peut, on présentera des travaux de première main, et non des citations (de citations, parfois !) de travaux ;
	
	* *dont on a pu reprendre certains éléments dans sa propre étude*, il est tout à fait intéressant de reprendre, pour les améliorer et les tester, certains aspects de méthode, de problématique issus des travaux lus. Cela montre que les argumentations ne sont pas bâties à partir d'élucubrations ;
	
	* *assez récentes*, un mémoire qui présenterait seulement des recherches datant de plus de dix ans pourrait être jugé peu à jour.

* *la problématique, l’énoncé du problème et les hypothèses de recherche* : le thème de recherche doit être clairement indiqué, tiré des lacunes analysées à partir de la revue de question, ainsi que les hypothèses qui seront testées dans la partie expérimentale. L’intérêt de la recherche peut être mentionné. Il importe ici de bien circonscrire le problème (qui doit faire apparaître les lacunes, incohérences, carences de la littérature sur le point discuté), de définir les termes en jeu, d’indiquer les limites que se pose l’auteur du mémoire, ainsi que celles qui lui sont imposées par le sujet. Les hypothèses (relation entre deux faits que l’on se propose de vérifier) sont testables et précises (voir Document :ref:`tuto_problematique` pour plus d'informations).


Partie empirique
----------------

La partie empirique du mémoire décrit l'étude spécifique qui a été entreprise, et qui est censée apporter de nouvelles informations sur la question posée. Elle comprend :

* La méthode, qui décrit le plus précisément possible les conditions d’expérimentation, pour d’éventuelles réplications :

	* L’hypothèse de recherche et l’hypothèse statistique,
	* la population, l’échantillon concerné, ainsi que la méthode d’échantillonnage des sujets,
	* l’instrument de mesure (le matériel utilisé pour recueillir les données qui vont être ensuite traitées),
	* la procédure de recueil de données,
	* le plan d’expérience (éventuellement).
	
* Les résultats, qui sont exposés suivant l’ordre des hypothèses, vérifiés au besoin par un appareillage statistique. Ne pas oublier de mentionner le nom des tests statistiques utilisés, le cas échéant.

Discussion
----------
La discussion comprend traditionnellement les parties suivantes :

* énoncé des limites de l’étude,
* discussion des résultats les plus significatifs, interprétation,
* recherches à venir, motivées par les résultats de ce travail.

Conclusion
----------
Souvent bien plus brève que la discussion, la conclusion, optionnelle, permet de clore le mémoire par une vision encore plus élevée du sujet du mémoire (notamment, avec une dimension professionnelle).

Rédiger le corps du mémoire
===========================

Le corps du mémoire devra être l’objet de soins particuliers car il sera lu très attentivement par le jury. Les qualités principales que les membres de ce dernier peuvent espérer y trouver sont les suivantes. Bien évidemment, l’ordre ci-dessous ne reflète pas une hiérarchie, où l’exactitude serait moins importante que la clarté. Le texte du corps du mémoire doit donc avoir les qualités suivantes :

Clarté
------

* On évitera notamment l’usage d’un jargon et, si l’on ne peut faire autrement, on définira les termes obscurs.
* Le jargon et les sigles sont définis à leur première utilisation ; ils sont de plus utilisés à bon escient. Les étudiants pensent parfois que le langage universitaire doit être sophistiqué, contourné, avec des structures syntaxiques complexes. Au contraire, il vaut mieux écrire clairement et avec exactitude.
* Le mémoire n’est pas envahi par le franglais. Les mots étrangers sont écrits en italiques et toujours traduits, en note par exemple.
* On écrit aussi en italiques les titres d’ouvrages et les mots que l’on veut mettre en valeur. L’usage du souligné est donc tombé en désuétude et devra être évité.
* Il n’y a pas d’abréviations dans le texte (hormis les locutions latines et unités courantes).
* Les énumérations courtes sont écrites dans le même paragraphe, on revient à la ligne pour les longues, en indentant négativement. Le tiret (—-) est utilisé, et non le trait d’union (-),  plus court.

Exactitude
----------

* On prendra garde, notamment, à faire des citations exactes ; les différents calculs, graphes, tableaux de chiffres présentés dans le mémoire devront être exempts d’erreurs.
* Chaque citation dans le corps du texte est appropriée, courte (couper par des […] au besoin) et se démarque du texte si elle dépasse cinq lignes (espace avant et après, ou citation en italiques ou encore citation en caractères plus petits). On n’oubliera pas d’en mentionner la source dans les références bibliographiques. Dans le cas de textes en langue étrangère, on traduira la citation dans le corps du texte, la version originale pouvant se lire en note. Enfin, on évitera de convoquer les grands auteurs à une assemblée étayant vos propos ; les mentions du type « l’auteur *X* a écrit ceci et l’auteur *Y* ajoute (*sic*) cela » ne sont pas toujours du meilleur effet, surtout quand *X* et *Y* ne se sont jamais rencontrés et, si cela avait été le cas, auraient eu toutes les chances de ne pas être d’accord.

Voici trois exemples de citations dans le texte principal :

	(1) Comme l’indique Richard (1990, p. 121) : « Les problèmes expérimentaux et, d’une façon générale, tous les problèmes qu’on pose à quelqu’un d’autre, y compris à des élèves, sont, pratiquement tous, par définition, des problèmes à information incomplète : en effet, par les conventions de la communication, celui qui pose le problème est supposé dire tout ce qui est pertinent. »

	(2) Comme l’indique Richard, « Les problèmes expérimentaux et, d’une façon générale, tous les problèmes qu’on pose à quelqu’un d’autre, y compris à des élèves, sont, pratiquement tous, par définition, des problèmes à information incomplète : en effet, par les conventions de la communication, celui qui pose le problème est supposé dire tout ce qui est pertinent. »

	(3) Les problèmes que l’on pose aux élèves peuvent être considérés comme des problèmes à information incomplète (Richard, 1990). 

Simplicité et concision
-----------------------

La syntaxe et le contenu du mémoire devront être simples, les phrases assez courtes et les graphiques lisibles. Un des autres pièges de la rédaction est la digression, où le rédacteur, de fil en aiguille, arrive dans des domaines qui ne concernent plus du tout son propos initial. On sera donc particulièrement vigilant sur ce point, car un mémoire trop long risque d’être lu superficiellement. Autre avantage de la concision, il sera alors plus facile de tirer un article d’un mémoire relativement court.

* Il n’y a pas de phrases de plus de trente mots. Certains correcteurs grammaticaux peuvent signaler les phrases trop longues.
* Il n’y a pas de longs textes entre parenthèses ou entre tirets.
* Chaque paragraphe exprime une idée, il est plus long qu’une phrase et plus court qu’une page. Il est de plus lié avec le précédent et le suivant.
* La forme passive n’est pas trop utilisée. Attention notamment aux traductions de l’anglais, qui font un usage abondant de cette forme.

Complétude et significativité
-----------------------------

* Le texte doit être concis mais pas incomplet. On fera attention, en croisant les sources bibliographiques, à ne rien oublier d’important.
* Dans la revue de question, par exemple, on veillera à ne présenter que des travaux importants et en rapport avec les hypothèses annoncées ; de même, on veillera à mettre en valeur les résultats significatifs d’une expérimentation.
* Il est utile, pour que le lecteur puisse au besoin s’y référer, de mentionner dans le corps du mémoire les données sur lesquelles s’appuie votre argumentation (par exemple sous la forme d’un tableau). Les résultats vraiment importants pourront même faire l’objet de graphiques, alors que les données moins centrales pourront figurer en annexe. On prendra garde à ne pas noyer le lecteur dans un flot de données non commentées et surtout non centrales dans la problématique du mémoire.

Distanciation
-------------

* Le jury attend du rédacteur une certaine hauteur de vue. Ce dernier devra donc montrer, dans son mémoire, qu’il parvient à décrire, synthétiser et critiquer les courants majeurs de son domaine de recherche.
* L’auteur utilise le « nous » qui permet de le (la) distancier. Ne pas oublier d’accorder selon le genre de l'auteur (par exemple : « Nous nous sommes rendue à l’université de… ») : il s'agit d'un "nous" de modestie, non de majesté.

Cohérence
---------

* Il faut s’attacher à ce que les textes soient cohérents avec les illustrations, que les commentaires et les conclusions soient cohérents avec les résultats, etc. Cela signifie que chacun apporte des informations complémentaires aux autres.
* Ils doivent aussi être dépourvus d’erreurs orthographiques (intramot), grammaticales (erreurs d’accord et de conjugaison), syntaxiques (erreurs de construction de phrase), typographiques (erreurs d’espacement entre ponctuation et mots, erreurs de majuscules) et de style (longueur de phrase, barbarismes, amphibologies, anglicismes, clichés, lieux communs, poncifs.

Textes et illustrations d'accompagnement
========================================

Les textes et les illustrations d’accompagnement sont tous les textes qui ajoutent une information optionnelle, non essentielle pour la compréhension du texte, mais qu’il peut être utile de consulter pour complément d’information. Nous décrivons ici les références et les annexes.

Les références
--------------

Tout d'abord, il ne faudra mentionner que les ouvrages cités dans le corps du mémoire (et donc pas les ouvrages lus, sans avoir été cités). Ensuite, ce n'est pas tant un nombre absolu de références qui est important (on peut citer de nombreuses références sans les avoir vraiment lues ni comprises), mais plutôt les références évoquées en détail, et directement reliées au sujet du mémoire. 

De ce point de vue, il est acceptable que le rédacteur sélectionne environ 5 articles/ouvrages vraiment en lien avec le sujet et dont le compte rendu montre qu'ils ont été compris, et qu'on en tire des enseignements pour sa propre étude.

Enfin, le format des références doit être utilisé de manière cohérente.

Les annexes
-----------

On reporte traditionnellement en annexe les textes, tableaux ou illustrations trop longs pour figurer dans le corps du texte et dont la lecture est secondaire, sans être pour autant inutile à la compréhension du mémoire. Les annexes sont en général écrites en simple interligne. On peut mentionner dans une annexe :

* la liste des participants de l'étude empirique, ainsi que leurs caractéristiques,
* le dessin du matériel utilisé ainsi que le détail de la procédure de l'étude empirique,
* les consignes données aux participants,
* la grille de recueil de données ou d’observation utilisée,
* une feuille de réponse d’un participant, à titre d’exemple,
* les tableaux de résultats bruts,
* un exemple de calcul statistique,
* les textes intégraux auxquels on a souvent fait référence dans le corps du texte,
* Un lexique ou glossaire définissant les principaux termes spécialisés, la source des définitions est donnée si elles ne sont pas de l'auteur. Dans le corps du mémoire, les mots du lexique ont un attribut spécifique (gras, italique, suivis d'un astérisque...). Le lexique peut être fondu avec l’index.

Les différentes annexes sont référencées par A, B, C ou en chiffres romains et mentionnées dans la table des matières. Chacune a un titre. Toute annexe est annoncée dans le corps du mémoire (par exemple : « Le lecteur pourra trouver en Annexe C les tableaux de résultats bruts »).

Présentation du mémoire : aspects formels
=========================================

Le document du mémoire est folioté (paginé) de la première à la dernière page, en incluant bien sûr les annexes. Des encadrés, optionnels, peuvent être insérés dans le mémoire pour présenter une technique, un fait, trop longs pour être intégrés au texte et trop importants pour figurer en annexe. Ces encadrés peuvent être écrits en caractères plus petits et/ou dans un interlignage plus étroit. Un résumé peut être fait à la fin de chaque chapitre.

Page de titre
-------------

Selon la norme AFNOR Z 41-006, d’octobre 1983, régissant la présentation des thèses et documents assimilés, la page de titre doit comporter, dans l’ordre indiqué :

* le titre complet du mémoire ou de la thèse et son sous-titre, s’il y a lieu,
* le nombre total de volumes, s’il y en a plus d’un et le numéro du volume considéré,
* le nom complet du ou des auteur(s), prénom en minuscules, nom en majuscules,
* la dénomination et la localisation de l’institution devant laquelle le mémoire est soutenu,
* le nom de l’organisme (U.F.R., laboratoire) dans lequel la recherche a été menée,
* le grade postulé et la spécialité,
* la date de soutenance,
* les noms du directeur de mémoire ou de thèse, ainsi que ceux du président et des membres du jury.

Page d’identification
---------------------

La page d’identification, comprenant le titre du mémoire, le nom et le prénom de l’auteur, les mots-clés et un résumé se trouvent sur la quatrième de couverture (au dos), ou bien après la page de titre. Elle facilite le rangement de l’ouvrage dans les bibliothèques et permet à ses futurs lecteurs d’en comprendre rapidement le contenu. Le résumé est d’environ deux cents mots, il décrit précisément les grandes lignes du mémoire, y compris les résultats.

Exergue
-------

Une exergue est la citation d’un auteur, elle indique au lecteur l’état d’esprit du chapitre ou du mémoire. Elle se place avant le titre du chapitre, généralement écrite en italique, alignée à droite, en un corps (taille) et un interlignage plus petits. L’auteur de la citation n’est pas suivi d’un point et s’écrit en romain (pas en italiques), petites capitales, hormis la première lettre, en majuscule. 

Notes de bas de page
--------------------

Une note de bas de page peut renvoyer :

* à divers ouvrages apportant un complément d’information,
* à une autre partie du mémoire,
* à un commentaire non indispensable pour la bonne compréhension du texte (précision, exemple, détail de calcul ou de procédure),
* à un texte original dont la traduction est dans le texte.

Les notes ne sont pas trop longues ni trop nombreuses. Il faut éviter de les rassembler en fin de mémoire ou en fin de chapitre, cela les rend très difficilement lisibles. Les notes sont en caractères de taille plus petite (par exemple inférieure au texte principal de deux points) et sont écrites en simple interligne. Les notes sont numérotées à partir de 1 à chaque page, ou chaque chapitre. Ne pas les numéroter en continu dans tout le mémoire. Les appels de note en exposant sont collés au mot auquel elles font référence.

L'encadré ci-dessous reprend l'organisation matérielle du mémoire. Les parties entre parenthèses sont optionnelles. Les pages de folio impair sont sur une « belle page », c’est-à-dire à droite, dans le cas d’une impression recto-verso. Pour une impression recto seul, on ne suivra que les items impairs ci-dessous.

.. note:: **La liste des différentes sections du mémoire.**

	0. Couverture.
	1. Page blanche, ou faux-titre, page reprenant seulement le titre de l’ouvrage.
	2. Page blanche.
	3. Reprise de la page de titre.
	4. Page blanche.
	5. (Page d’identification puis 6. Page blanche).
	6. (Dédicace puis 8. Page blanche).
	7. (Exergue générale puis 10. Page blanche).
	8. Remerciements, peuvent être écrits sur la page de dédicace, s’il y en a une.
	9. (Synopsis, écrit sur une page paire, pour faire face au sommaire, s’il y en a un), sinon, page blanche.
	10. (Sommaire), complet s’il n’y a pas de table des matières, ramené à une page dans le cas contraire. Il y a obligatoirement une table des matières s’il n’y a pas de sommaire, puis 14. Page blanche.
	11. (Table des illustrations) —tableaux et figures —, lorsqu’il y a un sommaire mais pas de table des matières.
	12. **Corps du mémoire**.
	13. Annexes.
	14. Glossaire.
	15. Références.
	16. (Index des auteurs), optionnel mais vivement conseillé.
	17. (Index des sujets).
	18. (Table des illustrations).
	19. (Table des matières), il y a obligatoirement un sommaire complet s’il n’y a pas de table des matières, et cette dernière est toujours complète lorsqu’elle est présente.

