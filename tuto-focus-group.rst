.. _tuto-focus-group:

**********************************
Tutoriel -- Mener un *focus group*
**********************************

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Armitt, Gillian
	single: auteurs; Kuniavsky, M.

.. admonition:: Information

	* **Auteur** : `Gillian Armitt <https://www.research.manchester.ac.uk/portal/gillian.armitt.html>`_, d'après Kuniavsky (2003), traduit par `Philippe Dessus <http://pdessus.fr/>`_, Espé & LaRAC, Univ. Grenoble Alpes.

	* **Date de création** : Décembre 2018.

	* **Date de modification** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce document décrit comment mener un *focus group*, c'est-à-dire une forme d'entretien de groupe dans lequel il n'y a pas d'aller-retours aussi fréquents que dans un interview classique, amenant le groupe à donner son avis collectif, plutôt que somme d'avis individuels (Cohen *et al*., 2011). Les *focus groups* sont assez souvent utilisés dans la recherche en éducation, comme un moyen qualitatif d'avoir des informations sur ce que pense un collectif d'usagers ou d'utilisateurs. 

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

Introduction
============

Comme le signale Bilocq (1999), un *focus group*, ou groupe de discussion est intéressant à mettre en place dès lors qu'on pense que les échanges qui vont s'y dérouler apportent une plus-value par rapport aux enquêtes traditionnelles et individuelles. Bilocq signale les buts d'un tel groupe :

* “Étudier les hypothèses concernant les sujets couverts par l'enquête ;
* explorer les connaissances, opinions, attitudes, perceptions, etc. des participants envers ces sujets ;
* évaluer la compréhension du vocabulaire et des concepts utilisés dans le questionnaire ;
* déterminer si les tâches (processus cognitifs) à accomplir pour répondre à une question sont réalisables […] ;
* évaluer la confiance des participants concernant les politiques de confidentialité et l'utilisation des données.”

Le groupe ne doit pas être trop important (6-10 personnes) et composé, si possible, de partcipants ayant des expériences et opinions les plus diverses possible. Le chercheur doit, au préalable, être au clair sur les buts de la discussion et avoir déjà une idée à propos des points ci-dessus. Il est utile d'enregistrer (audio ou vidéo) l'ensemble de la discussion à des fins d'analyse.


Les étapes du *focus group*
===========================

Formuler des questions
----------------------

* Chaque question doit être centrée sur l’expérience immédiate et éviter de conduire à des extrapolations de la part de l’interviewé. Par exemple « Cette fonctionnalité est-elle utile pour la tâche que vous êtes en train d’effectuer ? » est une meilleure question que « Cette fonctionnalité est-elle utile ? ». La seconde question peut être interprétée comme pouvant être nécessaire *a priori* à quelqu’un pour une certaine tâche. 
* Les questions doivent être neutres. Le répondant ne doit pas percevoir qu’une réponse particulière est attendue. « Ne pensez-vous pas que cette fonctionnalité serait meilleure si elle était disponible »  est un exemple de question induisant une réponse.
* Les questions doivent être focalisées sur un seul sujet. Il faut éviter les « et » « ou » dans la formulation des questions.
* Les questions doivent être ouvertes. Les questions fermées risquent de forcer les personnes à sélectionner l’un des choix offerts même si leur opinion diffère des choix proposés. 
* Éviter les questions binaires qui induisent des réponses en “oui/non, vrai/faux, ceci/cela… 

Réaliser un interview semi-structuré
------------------------------------

* Définir les termes. Chaque fois que possible, utiliser la définition d’un terme utilisée par les répondants (même si ce n’est pas votre acception du terme) mais assurez-vous que vous comprenez bien la définition (on peut demander aux répondants de définir). 
* Ne pas forcer à prendre parti, à une opinion donnée. Parfois, les gens n’ont simplement pas d’opinion par rapport à un point particulier. 
* Reformuler les réponses des répondants en utilisant d’autres mots. C’est l’une des meilleures façons d’identifier les problèmes potentiels soulevés par la formulation des questions. Cela permet de vérifier que vous comprenez la réponse et que le répondant a compris la question (vois aussi la section précédente relatives aux questions emphatiques). Eviter aussi la substitution immédiate de la terminologie employée par l’utilisateur par la terminologie « correcte » que vous utilisez.
* Être conscient de ses propres attentes. Vous avez vos propres attentes de l’interaction avec les participants et cela peut affecter la manière dont vous conduisez l’interview. Lorsqu’on en est conscient il est plus facile d’éviter des biais.
* Ne jamais dire à un participant qu’il a tort. Même si la compréhension d’un participant de la manière dont fonctionne le logiciel est complètement différente de ce que vous attendez, ne jamais dire à une personne qu’elle se trompe. Analysez et essayez de comprendre d’où vient cette interprétation et pourquoi elle est survenue.
* Écouter attentivement les questions posées par les participants et essayez de comprendre pourquoi ils les posent. Ces questions révèlent la manière dont les gens comprennent le logiciel ou la situation et sont importantes pour comprendre les expériences et les attentes des gens. 
* Utiliser des questions pour faire émerger les attentes, hypothèses et perceptions et non pour prouver des choses ou justifier des actions. Une bonne question fait le minimum nécessaire pour susciter une perspective ou une opinion, et rien de plus. 

Problèmes fréquents
-------------------

* Questions fermées qui devraient être des questions ouvertes.
* Opinions qui appellent des réponses complexes et qui sont explorées avec des questions binaires.
* Questions comprenant des mots difficiles, chargés ou polysémiques
* Demander aux gens de prédire le futur
* Pression d’autorité (« la plupart des gens disent que… est ce que vous pensez aussi ?… »)
* Supposer que vous connaissez la réponse ; celle-ci peut aller dans une direction à laquelle vous n’êtes pas préparé. Il est nécessaire d’écouter attentivement chaque mot.

Références
==========

* Armitt, G. (2009). *Validation activities–Information for LTfLL facilitators*. Manchester : Université de Manchester, Document de travail du projet FP7 LTfLL non publié.
* Bilocq, F. (1999). Conception et évaluation de questionnaires. In G. Brossier & A.-M. Dussaix (Eds.), *Enquêtes et sondages. Méthodes, modèles, applications, nouvelles approches* (pp. 261–272). Paris: Dunod.
* Cohen, L., Manion, L., & Morrison, K. (2011). *Research methods in education* (7th ed.). London: Routledge.
* Kuniavsky, M. (2003). *Observing the user experience: A practitioner’s guide to user research*. San Francisco: Morgan Kaufmann.

