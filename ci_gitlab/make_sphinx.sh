#!bin/bash

mkdir ./output
pip3 install -U -r ./requirements.txt
cd output
sphinx-build ../ .