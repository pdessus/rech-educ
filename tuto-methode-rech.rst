.. _tuto_methodo_rech:

********************************************
Tutoriel -- Définir une méthode de recherche
********************************************

.. Pequegnat, W., Stover, E., & Boyce, C. A. (Eds.). (2011). How to write a successful research grant application. voir Stinchcombe 1968
.. http://cescup.ulb.be/a-compendium-of-useful-stats-pages-for-social-psychologists/

.. http://www.educavox.fr/accueil/debats/histoire-de-la-methode-scientifique
.. http://discovery.ucl.ac.uk/1558776/1/A-Connected-Curriculum-for-Higher-Education.pdf

.. voir aussi http://mikedillinger.com/SJSUpapers/WriteupMethodsSection.pdf


.. index::
	single: auteurs; Dessus, Philippe

.. admonition:: Informations

 * **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, Espé & LaRAC, Univ. Grenoble Alpes.

 * **Date de création** : Mars 2017.

 * **Date de modification** : |today|.

 * **Statut du document** : Terminé. 

 * **Résumé** : Ce tutoriel donne des conseils pour spécifier la méthode d'une recherche. **Attention : ce tutoriel ne peut se substituer aux conseils d'un-e directeur-e de recherche**.

 * **Voir aussi** : Le doc. :ref:`veille_peda`, le :ref:`tuto_problematique`, le :ref:`tuto_recueil-donnees`, et le :ref:`tuto_organiser_memoire`.

 * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.



Introduction
============

Dans un mémoire de recherche, la section qui détaille la méthode employée dans la recherche est essentielle, puisqu'elle assurera (ou pas) la reproductibilité de l'étude, dans des contextes similaires ou différents. Pour autant, ces sections ne sont pas toujours bien documentées dans les mémoires, et parfois même les articles de recherche. Les auteurs d'une recherche savent exactement ce qu'ils ont fait, et ne voient donc pas toujours pas l'utilité de détailler le processus de leur recherche.

Ce document donne une méthode pour définir, et donc concevoir et documenter, une méthode de recherche. Il peut se lire et s'utiliser indépendamment de la méthode utilisée (qu'il est préférable d'avoir définie au préalable). Il s'inspire très fortement de O'Donnell (2015).

Les étapes du processus d'écriture (et de conception) de la section "méthode" sont les suivantes. Elles seront ensuite détaillées :

* Ecrire ce qu'on sait,
* Appeler un(e) ami(e),
* Faire un plan de travail,
  

Ecrire ce qu'on sait
====================

Commencer par écrire le plus simplement possible, sous une forme de liste d'items, ce que l'on sait de l'étude en projet, par exemple :

* son contexte (lieu, niveau d'école/établissement, etc.) ;
* ses participants (principales caractéristiques) ;
* les données que vous comptez récolter ?
* l(es) analyse(s) prévue(s) sur ces données ?

Appeler un(e) ami(e)
====================

Contacter un(e) ami(e) (si possible, mais pas nécessairement, un peu au fait de la recherche dans le domaine visé) et autour d'une tasse de café, et lui expliquer rapidement le but de la recherche. 

Lui présenter ensuite la liste ci-dessus, en l'expliquant, et lui demander d'ajouter ou de suggérer des items qui auraient été oubliés. Il (ou elle) peut aussi  donner d'utiles conseils sur les autres parties du projet de recherche. Les points manquants sont ajoutés à liste.

Faire un plan de travail
========================

À cette étape la liste devrait être assez détaillée pour :

* transformer les différents items en tâches, le cas échéant ;
* leur allouer un ordre et une durée.

Cela donne l'agenda précis de la recherche.

Les données et leur analyse
===========================

Cette étape va permettre de préciser quelles données vont être recueillies, et quelles analyses vont être réalisées.

Reprendre chaque activité du plan de travail et :

* déterminer les activités qui consistent en un recueil de données (si oui, lesquelles ?) ;
* déterminer les activités qui consistent en une analyse de données, s'assurer qu'elles seront disponibles au moment prévu ;
* définir plus précisément les processus de recueil et d'analyse pour chacune.

Justifier les décisions
=======================

Jusqu'à présent, chaque décision a été prise plutôt rapidement, sans vraiment se poser trop de questions. Il s'agit maintenant de reprendre chaque décision depuis le début et de les justifier, en expliquant pourquoi elle a été prise (pour des raisons théoriques ou plus pratiques) : 

* pourquoi avoir choisi cette population ? les analyses sont-elles pertinentes ? pourquoi ?
* en quoi chaque élément permet de répondre au mieux à la question de recherche ?

La nouveauté
============

Terminer en expliquant en expliquant pourquoi la méthode permet de répondre à la question de recherche d'une manière plus intelligente et efficace que celles précédemment tentées.


Références
==========

* O'Donnell, J. (2015). `How to write a simple research method section <https://theresearchwhisperer.wordpress.com/2015/03/10/writing-research-methods>`_.  Billet de blog du site "The research whisperer".
* Dillinger, M. (s.d.). `Writing up the Methods Section <http://mikedillinger.com/SJSUpapers/WriteupMethodsSection.pdf>`_.
* van der Maren, J.-M. (1996). `Méthodes de recherche pour l'éducation <https://www.pum.umontreal.ca/catalogue/methodes-de-recherche-pour-leducation>`_. Bruxelles : De Boeck.