.. _veille_peda:

*******************************************************
Veille pédagogique et académique : outils et stratégies
*******************************************************

.. index::
	single; auteurs; Dessus, Philippe
	single; auteurs; Mermet, Jean-Michel

.. admonition:: Informations

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes et Jean-Michel Mermet, SIMSU, Univ. Grenoble Alpes.

	* **Date de création** : Janvier 2012.

	* **Date de modification** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : L'enseignant est souvent vu comme un travailleur (jardinier) de la connaissance, passant du temps à butiner, sélectionner, compiler de nombreuses ressources (électroniques, sur papier, etc.) afin de trouver les plus appropriées pour ses étudiants, élèves ou production. En outre, cette question de veille est devenue de plus en plus cruciale car, les connaissances évoluant très vite, il est nécessaire que l'enseignant mette en œuvre les stratégies les plus efficaces possible pour mettre à jour ces dernières. Ce document donne quelques indications théoriques et pratiques pour toute personne voulant réfléchir et s'initier à ces questions (notamment, mais pas exclusivement, dans le cadre du C2i2e).

	* **Voir aussi** : Doc. SAPP :ref:`ressources_veille`.

	* **Note** : ce document est principalement issu d'un `cours de veille pédagogique <http://pdessus.fr/cours/diasveillepeda.pdf>`_ réalisé à l'IUFM de Grenoble à partir de 2010 et d'éléments du document :ref:`ressources_veille`, qui recense de très nombreuses ressources en reprenant le classement présenté ci-dessous.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Clara est une enseignante qui aime se tenir au courant de l’actualité dans l’enseignement de sa discipline. Tous les soirs, avant de préparer ses cours pour le lendemain, elle ouvre son portail personnel et regarde rapidement les nouvelles dépêches. Après avoir jeté un coup d’œil aux nouveautés elle ajoute celles qui lui paraissent les plus intéressantes dans son site de liens. Cet ajout déclenche automatiquement une action, l'envoi d'un tweet (message court) pour le signaler aux personnes de son réseau. Lorsqu’un contenu lui paraît particulièrement pertinent, elle l’intègre dans son cours et elle en parle dans son blog, qu’elle alimente régulièrement.

Ce genre de manipulations est maintenant presque banal, tant les outils pour la mettre en œuvre ont progressé (en étendue de fonctionnalités et en interconnexions), mais de la même façon qu'une liste d'ingrédients ne suffit pas à énoncer une recette, les démarches de Clara présupposent une réflexion approfondie sur son dispositif de veille : 

* Comment Clara a-t-elle sélectionné les sites qu’elle consulte régulièrement ?
* Comment d’autres collègues peuvent-ils s’informer de ce que lit Clara ?
* Pourquoi ces collègues sélectionnent (ou pourraient sélectionner) Clara comme source d’information ?
* Comment Clara utilise-t-elle certaines parties de son portail avec ses élèves ?
* Comment considérer son action ? Est-ce de la surveillance ? de la curation (comme on le dit de plus en plus) ?
* Ces pratiques ne posent-elles pas des questions de protection de la vie privée ?

Toutes ces questions sont relatives à la question de la veille pédagogique (ou académique) et la pratique même de veille pédagogique est de plus en plus communément rencontrée. L'enquête réalisée récemment par IPSOS-MediaCT :cite:`ipsos11` auprès d'un panel d'enseignants plutôt technophiles puisque recrutés via un site pédagogique montre que la préparation de cours, la recherche de documents et la documentation/formation/recherches personnelles sont à la fois les activités jugées les plus intéressantes par les enseignants, mais aussi celles auxquelles ils consacrent le plus de temps, près de 13 h en moyenne par semaine).

Ce document a pour but de donner quelques pistes théoriques et pratiques à son propos, en détaillant quelques outils et stratégies. La veille pédagogique (:cite:`pinte05` p. 1, trad. libre) est un « [...] processus régulier de recherche, analyse et sélection d'informations pertinentes dans le champ de l'éducation [...]. Fondé sur des critères utilisés par des agents intelligents ou des méta-moteurs de recherche, le but de la veille pédagogique est de détecter et faciliter les recherches documentaires, identifier des réseaux d'expertise qui esquissent la carte des principaux concepts d'un champ. » Cette définition, à notre avis, ne dévoile qu'une partie de l'activité de veille, celle de la recherche et sélection d'informations et néglige le but-même de la veille, qui est de diffuser (pour le chercheur) et réaliser un enseignement (pour l'enseignant). De la même manière qu'on ne veille pas pour rester éveillé, on réalise une activité de veille pour produire de l'information (ou même de la connaissance) nouvelle.

Pour finir de présenter ce sujet très complexe, il nous faut insister sur l'idée que le but du chercheur ou de l'enseignant faisant de la veille n'est pas un but si défini que cela, ce qui a amené Bernstein :cite:`bernstein93` cité par :cite:`boubee10` à nommer cette activité "jardinage" (*information farming*), ou "culture de l'information comme activité continue et collaborative, conduite par des groupes de personnes travaillant à la réalisation de buts changeants, individuels et communs". Il nous semble qu'on ne peut mieux qualifier cette activité que cela, en insistant sur ses aspects continus (on veille tout le temps), collaborative, avec des buts changeants. Voyons maintenant de plus près en quoi consiste cette veille, qui peut être utilement réalisée avec des outils informatiques.

Ce que l'on sait
================

Accéder aux informations vs. connaître
--------------------------------------

Il est de plus en plus évident, au moins depuis que les enseignants et les  chercheurs ont un accès intensif à Internet, que le fait d'avoir accès  à un nombre très élevé (on parle souvent de croissance exponentielle  de l'information :cite:`weller11` n'implique pas nécessairement qu'on les comprenne, voire les traite (voir, à ce sujet, la notion de  construction de connaissances dans le document SAPP :ref:`constr_conn`). Intuitivement,  nous pensons que cette idée est nouvelle, au moins contemporaine à  Internet ou, à tout le moins, à la diffusion massive de livres. En  réalité ce n'est pas le cas. Blair :cite:`blair10` montre, d'une part, que ce  point était déjà débattu avant la Renaissance (entre autres, par  Sénèque et Erasme), alors que la diffusion de l'écrit imprimé  débutait. Les outils de référence créés au Moyen Âge, par exemple,  sont encore largement utilisés de nos jours : – florilèges (meilleures  pages d'ouvrages, compilées pour pallier la rareté de certains  ouvrages) ; – dictionnaires ; – encyclopédies (conçues initialement  pour donner les connaissances nécessaires à l'interprétation des  Ecritures, voir Eco :cite:`eco11`). Et le problème du "filtrage" de  l'information était déjà une préoccupation des encyclopédistes  médiévaux (:cite:`eco11` p. 129).

D'autre part, Blair montre que les techniques de traitement de l'information (les 4 *S* : *storing, sorting, selecting, and summarizing*, ou collationner, trier, sélectionner et résumer) étaient déjà connues et bien peu différentes de celles des chercheurs et enseignants contemporains, même si fondées de nos jours sur d'autres supports que le livre. Gardey :cite:`gardey08` montre que dans une période plus récente (juste avant l'ère de l'informatique) les méthodes de classement de l'information (classer les idées par thème, sur des fiches cartonnées) étaient elles aussi suffisamment avancées et systématisées au tout début du XXe siècle, soit bien avant l'invention de l'ordinateur.

1, 2, 3, Web ! Quelques définitions
-----------------------------------

Il est maintenant courant de faire les distinctions suivantes entre différentes versions du web, apparues dans cet ordre :

* **le web 1.0**, dont l'activité principale est « se connecter à », avec    des moteurs de recherche "classiques" comme Google, Yahoo, etc., dont    les buts sont de permettre une recherche et un accès à l'information    (information tirée, *pulled*), à la demande.

* **le web 2.0**, dont l'activité principale est « se connecter au travers » et dont les buts des utilisateurs sont de partager, participer, collaborer, au travers de sites comme Facebook, YouTube, Delicious ou Wikipedia. Ici, l'information est principalement poussée (pushed), mise à jour sans requête explicite, à des groupes préalablement formés et des sites préalablement sélectionnés comme étant d'intérêt.

* **le web 3.0** utilise la métaphore de « se connecter dans », via des environnements plus immersifs et/ou sémantiques (*Second Life*, *Evri*), l'information est disponible sous forme très structurée (analyse sémantique préalable) et le plus authentique possible (immersion). Le web 3.0 étant encore à ses débuts, l'enseignant et le chercheur se connecte principalement à des sites web 1.0 ou 2.0 pour réaliser sa veille pédagogique ou scientifique.

En quoi consiste-t-elle plus précisément ? Les huit principales   activités du chercheur :cite:`weller11` citant Unsworth :cite:`unsworth00`, que ce dernier appelle joliment "primitives", sont les suivantes, et on peut utilement les rattacher à l'une des quatre phases du cycle SLED   (sélectionner–Lire–Ecrire–Diffuser) décrit plus bas : 

* S – Sélectionner, choisir, récupérer des échantillons appropriés ;
* S – Sélectionner, comparer, des données, des textes de différentes langues, etc. ;
* L – Lire, découvrir des connaissances (par des archives ou une recherche) ;
* L – Lire, annoter, ajouter des niveaux d'interprétation ;
* E – Ecrire, référer à des sources, pointer sur elles pour un cours, une recherche ou pour y accéder ultérieurement ;
* E – Ecrire, illustrer, clarifier, élucider, expliquer ;
* D – Diffuser, publier, communiquer, faire des exposés, des cours.

La description de l'activité du chercheur ou de l'enseignant en termes   de cycle ou de flux n'est pas la seule intéressante. Il convient   également de réfléchir à des paramètres caractérisant l'information   elle-même. Weller (:cite:`weller11` chap. 5) montre que quelques concepts   deviennent pleinement pertinents si l'on essaie de réfléchir à un   travail académique : 

* la granularité des informations traitées : de la même manière que dans l'industrie musicale l'unité de vente est progressivement passée de l'agrégat organisé, le disque (un ensemble de chansons dont l'enchaînement logique et tonal est longuement réfléchi par le producteur) à la chanson, le chercheur se retrouve de plus en plus fréquemment conduit à traiter des articles (voire des news) plutôt que des ouvrages organisés ou composés. Et ce changement de granularité a bien sûr un impact sur la manière de faire de la veille (les éléments d'information sont beaucoup plus nombreux, de taille et de niveaux de complétude et de méta-description différents), mais aussi de l'appréhender ; 

* la revanche des news-médias: permettre la publication rapide de  billets courts sur internet permet une diffusion plus rapide de  certains résultats de la recherche, et accélère aussi la réaction à  ces résultats. Elle donne également un poids plus important à ce  nouveau type de médias (voir le rôle qu'ils ont joué dans les  récentes révolutions en Egypte ou Tunisie, même s'il a sans doute été surévalué) ;

* l'externalisation de la connaissance (*crowdsourcing*) : l'expérience de la wikipédia (voir :cite:`reagle10` et le document SAPP :ref:`wikipedia`, pour une description de la manière dont la communauté s'est doté de règles de fonctionnement), mais aussi les nombreux projets partageant les ressources de calcul des ordinateurs individuels sont des exemples d'externalisation réussie de partage (de ressources matérielles, d'expertise) de tous, au service de quelques projets. La veille peut bien évidemment aussi bénéficier de cette caractéristique. 

* innovation rapide : le simple fait d'accéder à des exemples d'innovations peut donner des idées à de nombreuses personnes d'y  participer, ce qui contribue à accentuer le phénomène (voir aussi la  vidéo de Johnson en références).

Ce que l'on peut faire
======================

On l'a vu plus haut, veiller, c'est se tenir informé et alerté à   propos d’informations liées à sa pratique professionnelle (dans notre   cas, l’enseignement ou la recherche), pour les réutiliser dans cette   pratique. Cette pratique se décline comme suit :

* préparer des cours, se tenir informé sur sa discipline, trouver de nouvelles idées, pas simplement pour la collecte d'informations

* diffuser ses propres idées et son travail d'enseignant, p. ex. en créant un site informant de ses centres d’intérêt (sur un thème, un contenu, pour un projet, un niveau donnés) pour se les voir commentés (par élèves ou collègues)

*  Repérer des sites s’intéressant aux même sujets que nous et les référencer.

Un cycle de veille pédagogique assisté par quelques outils
----------------------------------------------------------

Nous décrivons rapidement un cycle de veille pédagogique en prenant   pour exemple quelques outils pouvant l'assister (ce cycle est inspiré du travail de Hull et ses collègues :cite:`hull08`). Le document SAPP :ref:`ressources_veille` contient l'ensemble de la démarche et un plus grand choix d'outils. Le   développement ci-dessous fait référence à certaines de ses pages et la Figure 1 ci-dessous résume le flux d'activités, qui est ensuite détaillé.

.. image:: /images/sled.png
   :scale: 80 %
   :align: center

**Figure 1** – Le flux d'activités SLED : Sélectionner–Lire–Ecrire–Diffuser (Dessus & Mermet).


1. **Sélectionner des informations**. (voir la Section :ref:`veille_select`) La première phase de la recherche est celle de la sélection d'informations pertinentes pour son travail d'enseignement ou de recherche. En effet, tout chercheur ou enseignant a besoin de connaître précisément les travaux de ses collègues, à la fois pour s'y appuyer, mais aussi pour vérifier l'originalité de sa propre réflexion. Ce point nécessite qu'il ait une politique d'archivage pérenne de ses propres productions (travaux, cours, etc.).

2. **Lire des travaux**. (voir la Section :ref:`veille_lire`) Une fois des références sélectionnées, il convient de se faciliter la lecture, mais aussi (et cela va être utile uniquement pour le chercheur) d'analyser leur importance d'un point de vue scientométrique (niveau de citation, facteur d'impact). Les bibliothèques universitaires sont également munies de très bons répertoires de tels sites.

3. **Ecrire un document**. (voir la Section :ref:`veille_ecrire`) Après avoir sélectionné et lu du matériel, il est souvent nécessaire de passer par une phase de production (travail de synthèse, articles, livre, notes de cours) sans laquelle la démarche globale serait quelque peu stérile (voir notre commentaire en introduction). Cette phase est vraisemblablement la plus difficile des quatre, et il est important d'avoir à sa disposition des outils qui la facilitent.

4. **Diffuser ses travaux**. (voir la Section :ref:`veille_diffuser`) Une fois qu'un article, un ouvrage ou un cours est écrit, il s'agit de faire en sorte que le plus possible de collègues puissent en prendre connaissance.


Réfléchir à son propre flux de veille
-------------------------------------

Une fois les outils choisis et leur fonctionnement assimilé, il est important de réfléchir à la connexion de ces outils et à la manière dont ils formeront une chaîne de veille utile et efficace. Pena-Lopez :cite:`pena08` nous fournit un moyen tout à fait utile pour cela. La Figure 2 ci-dessous contient un exemple de flux : à gauche les sites délivrant de l'information, au mieux ceux dans lesquels on traite principalement l'information, Pour chacun de ces outils, il faudra se poser la question de leur pertinence, de la manière d'y accéder (personnellement ou avec un pseudo), de la manière dont l'information en output va être accessible (en privé ou public), et surtout quel va être le traitement réalisé par l'enseignant ou le chercheur (principalement au centre et à droite de la Figure).


.. image:: /images/flux-pena.png
   :scale: 100 %
   :align: center

**Figure 2** – Un exemple de flux de veille avec divers outils web 2.0 (d'après Pena-Lopez :cite:`pena08`).

Analyse de pratiques
--------------------

#. Utiliser un moteur de recherche pour chercher du matériel libre de droits (p. ex., sous Licence Creative Commons NC).

#. Construire son propre flux de veille en répondant aux questions posées dans la Section "Réfléchir à son propre flux de veille".


Références
==========

Vidéos
------

* Johnson, S. (s.d.), Where good ideas come from. Accessible à http://www.youtube.com/watch?v=NugRZGDbPFU
* Podcast veille pédagogique et scientifique (IUFM-UJF Grenoble) : http://www.grenoble.iufm.fr/c2i2e/formations/veille/index.html
* Podcast "Les outils web 2.0 pour la recherche (J.-M. Mermet) : http://podcast.grenet.fr/episode/09-les-outils-web-2-0-pour-la-recherche-jean-michel-mermet-ujf-grenoble-pdf/

Site internet
-------------

Entrée Veille (wiki de l'université Paris-V) : http://wiki.univ-paris5.fr/wiki/Veille

Références
----------

.. bibliography:: veillepeda.bib