.. _tuto_lire_article:
.. Faire aussi un papier sur la revue de litt.
.. todo: 1.	Chojnacki, G., Resch, A., Vigil, A., Martinez, I., & Bates, S. (2016). Understanding types of evidence: A guide for educators. Washington: Mathematica Policy Research.

.. https://explorationsofstyle.com/2011/02/09/reverse-outlines/
.. http://www.raulpacheco.org/2017/04/how-to-undertake-a-literature-review/
.. https://patthomson.net/2017/09/11/avoiding-the-laundry-list-literature-review/?utm_content=buffer3d0c5&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer

========================================
Tutoriel -- Lire un article de recherche
========================================

.. index::
	single: auteurs; Dessus, Philippe

.. admonition:: Informations

	* **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, Espé & LaRAC, Univ. Grenoble Alpes.

	* **Date de création** : Avril 2017.

	* **Date de modification** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce tutoriel décrit les stratégies pour lire et analyser les articles de recherche en sciences humaines et sociales. Il peut également être utilisé pour structurer l'écriture d'un article de recherche. **Attention : ce tutoriel ne peut se substituer aux conseils d'un-e directeur-e de recherche**.

	* **Voir aussi** : Documents :ref: `veille_peda` et :ref:`atelier_poster_sci`.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_


Introduction
============

Les chercheurs passent une partie non négligeable de leur temps à lire et à évaluer le travail de leurs collègues. Ils doivent donc développer des stratégies les plus efficaces possible pour optimiser ce temps de lecture. Ce document donne quelques pistes et stratégies pour lire un article de recherche.

Ce document ne s'intéresse pas aux méthodes pour trouver des travaux selon un thème donné. Pour cela, voir les Documents :ref:`veille_peda` et :ref:`ressources_veille`.


La structure d'un article de recherche (CCC)
============================================

La structure de tout article de recherche est standardisée, ce qui permet à tout lecteur d'y retrouver plutôt aisément ce qu'il recherche. Kording et Mensh (2016) signalent que tout article de recherche raconte une histoire et a une structure en trois parties, qui sont toutes les trois indispensables à sa compréhension :

#. *Contexte*. Le contexte est présenté dans l'introduction de l'article. Sans contexte, le lecteur peut se demander "Pourquoi dire tout ça ?". 
#. *Contenu*. Le contenu détaille l'ensemble de la démarche des auteurs (l'histoire).
#. *Conclusion*. La conclusion. Sans conclusion, le lecteur peut se demander "Et alors ?".
 
Ces auteurs disent aussi que la structure en CCC est une manière d'écrire qui peut aussi s'appliquer à chaque section (voir plus bas), mais aussi à chaque paragraphe de l'article. En effet, la première phrase d'un paragraphe est censée expliquer son contexte alors que la dernière conclut sur ce qui a été présenté dans le paragraphe. Détaillons maintenant chaque partie de l'article en expliquant son contenu.

Le résumé
---------

Le résumé est censé intégrer les trois éléments CCC, de manière à ce qu'un lecteur intéressé par l'article puisse se faire une idée complète de son contenu. Le tout début de l'article doit donc indiquer :

* un élément de *Contexte* (dans quel champ cette recherche se situe ?) ;
* des éléments de *Contenu* (ce que contient l'article : "ici, nous avons adopté telle approche") ;
* des éléments de *Conclusion* (qui expliquent en quoi ces résultats sont importants et signifiants dans le Contexte, quelles conclusions on peut en tirer).

L'introduction
--------------

L'introduction est à prendre au sens large, elle peut donc intégrer la partie "état de l'art", qui détaille ce qui a déjà été écrit sur le sujet, et en quoi le contenu du papier est novateur. En bref, elle indique l'écart entre ce qui s'est déjà fait sur le sujet dans le champ concerné, et en quoi et comment) l'article va remplir cet écart. Il comprend les parties suivantes (qui reprennent la structure en CCC), qui, selon la taille de l'article, peuvent être distribuées sur un ou plusieurs paragraphes ou sections. Bien évidemment, tous les articles ne respectent pas strictement cette structure, mais il peut être intéressant et formateur de repérer ces thèmes dans les articles que nous lisons.

* *Un grand problème (Contexte)*. Le tout début de l'introduction explique un grand problème, en montrant pourquoi il peut être considéré comme important, et pourquoi il est également important d'essayer de le résoudre.
* *Un problème plus précis* (Contexte). Le deuxième paragraphe détaille un problème plus précis, partie du grand problème.
* *Un problème encore plus précis à résoudre dans l'article* (Contenu). Le troisième paragraphe détaille exactement le propos de l'article, c'est-à-dire à quel problème il s'attaque.
* *Résumé* (Conclusion). La fin de l'introduction détaille l'approche suivie dans l'article et, très sommairement, son principal résultat.

Les résultats
-------------

La section "résultats" est également à prendre au sens large, et le Document :ref:`tuto_methodo_rech` en donne une structure plus précise. Comme l'indiquent Kording et Mensh (2016), son but est de montrer au lecteur que le problème considéré est traité avec les données et la logique nécessaires. Elle contient les paragraphes (ou sous-sections) suivants, qui concourent à détailler la logique de l'argumentation (chaque élément logique est justifié) et qui peuvent être assez différents :

* *Résumé de la méthode*, qui donne un aperçu général de ce qui est présenté,
* *Elément logique 1* (p. ex., données recueillies),
* *Elément logique 2* (p. ex., traitement des données),
* ... *Elément logique* n (p. ex., résultats).

Les figures, tableaux et légendes doivent permettre aisément d'accéder aux résultats principaux.

La discussion
-------------

La section "discussion" explique en quoi les résultats obtenus permettent de résoudre le problème auquel les auteurs se sont attaqués, et en quoi il ouvre de nouvelles opportunités de recherche. Il peut comprendre les paragraphes ou sous-sections suivants :

* *Les principaux résultats et la conclusion*, reprend les principaux résultats et explique le problème qui a été résolu.
* *Les limites*, explique quelles sont les limites de la méthode employée, comment on peut interpréter les résultats en fonction de ces limites, et quelles pistes peuvent être suivies dans de futures recherches pour les pallier.
* *La contribution de l'article au problème traité*,  explique la contribution majeure de l'article au champ de recherche, au problème exposé dans l'introduction.
  

La méthode QAMRI
=================

L'acronyme QAMRI (repris et simplifié de Brosowsky & Parshina, 2017), est détaillé ci-dessous, et permet d'avoir en tête les principaux éléments d'un article. Il peut être utile de se centrer sur ces éléments pour comprendre un article, en faire une synthèse, une relecture ou une recension.

* *Question* : Quelle est la question (problématique) posée par la recherche décrite dans l'article ?
* *(Hypothèses) Alternatives* : Quelle est l'hypothèse principale évoquée dans l'article ? Quelle serait son hypothèse alternative ? Les articles empiriques exposent souvent au moins deux réponses possibles à la question de recherche, chaque réponse étant une hypothèse. Parfois, seule l'hypothèse principale est spécifiée dans l'article et l'on peut essayer de formuler une hypothèse alternative, qui pourrait tout aussi bien être une issue au problème de recherche. Chaque hypothèse va être formulée en SI *X* ALORS *Y* (Si l'hypothèse envisagée est vraie, que devrait-il se passer ?) (voir Document :ref:`tuto_problematique` pour plus d'informations).
* *Méthode* : Détaille ce que les chercheurs ont fait dans leur étude pour tenter de répondre à la question. Il faut déjà se rendre compte du type de méthode utilisée (expérimentale, quasi-expérimentale, corrélationnelle, etc.), de la manière dont les participants ont été recrutés et distribués dans les différents groupes, la procédure de l'étude, le type de données recueillies, leur traitement, etc.
* *Résultats* : Détaille les réponses aux hypothèses. Il est important ici de se rendre compte de la performance des participants dans leurs tâches, et de repérer les principaux résultats (assez souvent, la section résultats contient de nombreuses données et tests et il n'est pas toujours aisé de s'y repérer).
* *Inférences* : Détaille les principales conclusions (ou inférences) que les auteurs font à partir de leurs résultats. Ils doivent déterminer quelle(s) hypothèse(s) peuvent être validé(s) en fonction des résultats obtenus. Le lecteur peut reprendre la chaîne QAMRI en formulant, en quelques lignes, le principal résultat que les auteurs ont mis au jour.

Écrire un article en une journée
================================

Bien sûr, les informations ci-dessous permettent aussi d'écrire un article. Nous expliquons ci-dessous une méthode collaborative "éclair" d'écriture d'article tirée d'un `fil de discussion <https://twitter.com/mcmullarkey/status/1071912349068611585>`_ Twitter (déc. 2018), dont les inventeurs sont les possesseurs des comptes Twitter suivants : @JnfrLTackett, @cmbrandes, @kathleenwade, et @allisonshieldsy.

* Les auteurs s'installent dans la même pièce (pour faciliter les interactions).
* Tous se connectent à un même document partagé. Favoriser un système permettant l'utilisation d'un gestionnaire.
* bibliographique (p. ex., Google Docs permet l'`extension Zotero <https://www.zotero.org/support/fr/google_docs>`_). Chacun peut voir la progression de l'article en défilant les pages.
* Tous se connectent à un gestionnaire de tâches en ligne (un simple logiciel de notes collaboratives suffit, comme `framanotes <https://mes.framanotes.org/>`_). Cela permet de créer et attribuer rapidement les tâches de chacun, et de surveiller leur accomplissement. 

Des collaborateurs ont des tâches plus spécialisées :

* Un (ou plusieurs) collaborateur(s) spécialisé(s) dans le traitement et l'analyse de données ouvrent un logiciel de statistiques (p. ex., `R <https://www.r-project.org>`_). Ils s'occupent prioritairement des parties "méthode" et "résultats" de l'article.
* Un (ou plusieurs) collaborateur(s) spécialisé(s) dans la littérature ouvre un dossier (ou un gestionnaire de références bibliograhiques) dans lequel tous les articles en lien avec le sujet se trouvent. Il(s) s'occupen(t) de la partie théorique et de l'introduction.

L'écriture de l'article peut ainsi se réaliser en une seule journée, selon les auteurs de cette discussion (il est conseillé d'amener son déjeuner pour ne pas perturber le travail).

Analyse de pratiques
====================

#. Prendre un article de recherche et distinguer ce qui est de l'ordre du Contexte, du Contenu, ou des Conclusions.
#. Réaliser la même chose avec l'acronyme QAMRI.
#. Se servir de ces analyses pour réaliser une fiche de lecture, un poster, résumant les principales données de l'article. Voir le Document :ref:`atelier_poster_sci` pour plus d'informations sur cette dernière activité.

Références
==========

* Brosowsky, N., & Parshina, O. (2017). `Using the QALMRI method to scaffold reading of primary sources <http://teachpsych.org/resources/Documents/ebooks/gstaebook.pdf>`_. In R. Obeid, A. Schwartz, C. Shane-Simpson & P. J. Brooks (Eds.), *How we teach now. The GSTA guide to student-centered teaching* (pp. 311–339). s. l..
* Kording, K. & Mensh, B. (2016). *Ten simple rules for structuring papers*. Preprint bioRxiv accessible à https://doi.org/10.1101/088278.

