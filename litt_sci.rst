.. _litt_sci:

**************************************
Comprendre la littérature scientifique
**************************************

.. index::
	single: auteurs; Dessus, Philippe

.. admonition:: Informations

	* **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, Univ. Grenoble Alpes.

	* **Date de création** : Mars 2017.

	* **Date de modification** : |today|.

	* **Statut** : En travaux.

	* **Résumé** : Ce document donne des pistes pour lire et interpréter un article scientifique en sciences humaines.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Un mémoire de recherche ou un article comportent nécessairement une ou plusieurs section(s) décrivant ce que l'on nomme "l'état de l'art", c'est-à-dire un ensemble de références déjà publiées, que l'on a lues et sur lesquelles ont s'est appuyé pour écrire son propre travail. Ces références peuvent avoir servi à de nombreuses choses, comme XXX.

Pourquoi a-t-on à lire des articles ? Cela peut être à des fins de localiser une information précise (retrouver une donnée, repérer un argument), ou bien à des fins, plus élaborées, de comprendre une notion. Les lecteurs d'articles scientifiques, d'une part, ont des besoins et des objectifs qui leur sont propres et, d'autre part, engagent des stratégies de lecture pour évaluer le contenu des articles selon certains critères scientifiques (qualité du raisonnement proposé, plausibilité des faits présentés)(Britt *et al*., 2014).

Si l'on se centre sur cette dernière activité, il est difficile de lire des articles à propos de science.Comme le signalent Britt et ses collègues (2014), la raison en est multiple.

Premièrement, les articles *décrivent des phénomènes complexes* : même le phénomène le plus simple en apparence peut en réalité cacher un réseau de causalité complexe (comment apparaît-il, en fonction de quelles causes, et quelles conséquences peut-il déclencher ?). Pour le lecteur, cela nécessite de connecter ces différentes causes et conséquences dans un réseau cohérent. De plus, le type de vocabulaire utilisé dans les articles est technique, peu répandu dans les textes habituellement lus et souvent dense, ce qui oblige le lecteur à redoubler d'attention pour en comprendre le sens.

Deuxièmement, et notamment lorsqu'on fait des recherches, il est nécessaire de se pencher sur *de multiples sources et documents*.

Références
==========

Britt, M. A., Richter, T., & Rouet, J. (2014). Scientific literacy: The role of goal-directed reading and evaluation in understanding scientific information. *Educational Psychologist*, *49*(2), 104-122. doi:10.1080/00461520.2014.916217
