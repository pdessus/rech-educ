.. _tuto_concevoir_quest:

======================================================================
Tutoriel --  Concevoir un questionnaire : des dimensions aux questions
======================================================================


.. index::
	single: auteurs; Dessus, Philippe

.. admonition:: Informations

	* **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, Univ. Grenoble Alpes.

	* **Date de création** : Février 2019.

	* **Date de modification** : |today|.

	* **Statut** : Terminé.

	* **Résumé** : Ce Tutoriel indique une procédure pour définir les différents items d'un questionnaire, de la spécification des dimensions à l'élaboration des questions. **Attention : ce tutoriel ne peut se substituer aux conseils d'un-e directeur-e de recherche**.
	
	* **Lire avant** : Il est conseillé de lire le :ref:`tuto_problematique` pour préciser les éléments en amont des dimensions et des questions.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.
	  

Introduction
============

Ce Tutoriel est dédié à la réalisation de questionnaires pour des enquêtes (par exemple, *via* internet), qui sont un moyen rapide et parfois efficace de recueillir des opinions sur une ou des question(s). Tout d'abord, un questionnaire s'inscrit dans une recherche, avec des buts généraux et objectifs plus précis, et nous conseillons le lecteur de parcourir le document :doc:`tuto-problematique`, qui donne des indications sur une méthode pour déterminer un problème de recherche, une problématique et des hypothèses. Ensuite, une fois ces éléments définis, il est nécessaire de définir de plus près les dimensions et indicateurs. Avant cela, prenons un exemple.

Un exemple
==========

Prenons un exemple simple : on veut faire un questionnaire pour déterminer les motivations des visiteurs d'un site internet (proposant des ressources éducatives). Connaître ces motivations est important pour le gestionnaire du site, pour pouvoir mieux connaître son public et continuer à lui proposer un contenu qui l'intéresse. Notre problématique est donc : quelle est l'expérience des utilisateurs de notre site ? On peut déjà produire des hypothèses schématiques sur des différences de comportement dans le site en fonction du statut du lecteur : un lecteur enseignant aura un comportement, des motivations à utiliser le site, et une opinion sur son contenu différentes de celles d'un élève.

Dimensions
==========

Quelles sont les principales dimensions qui caractérisent la pratique d'un visiteur d'un site de ressources ? Comme on peut le comprendre, une dimension est un concept central lié à la connaissance, la fréquentation ou la pratique du phénomène étudié dans l'enquête. On peut déjà avoir une idée intuitive des concepts en jeu dans notre sujet d'étude, mais une recension de la littérature sur le sujet peut aider. Dans notre exemple, nous pouvons lister les dimensions suivantes :

- *Compréhensibilité* : le lecteur pense que le contenu présenté est compréhensible, pour lui et/ou pour les utilisateurs finaux (élèves, pour des questionnés enseignants) ;
- *Crédibilité* : le lecteur pense que le contenu présenté dans le site est crédible, qu'il peut lui accorder une probabilité de véracité suffisante, notamment s'il pense que le contenu est en lien avec des éléments théoriques, issus de la recherche ;
- *Réflexivité* : le lecteurs pense que le contenu présenté le fait réfléchir sur ses pratiques, lui en fait envisager de nouvelles ;
- *Réutilisabilité*, ou transférabilité du contenu du site : le lecteur pense que le contenu du site pourra être réutilisé dans sa pratique (p. ex., dans des cours) ;
- *Volonté de collaborer*, dans le cas d'un site collaboratif (comme un wiki), le lecteur peut avoir envie de participer à l'élaboration du contenu du site.

Nous avons pris soin, à chaque dimension, de bien préciser “le lecteur pense que...”, afin de bien mettre en évidence qu'un questionnaire ne recueille que des opinions, ou des pratiques déclarées, et non des pratiques avérées. 

Le problème est qu'il ne suffit pas, en général, de poser directement des questions à propos de ces dimensions. On pourrait, par exemple, directement demander : “pensez-vous que vous comprenez le contenu du site ?”, mais on peut se heurter à différents biais des participants, notamment celui de désirabilité sociale (ce n'est pas socialement agréable de s'avouer ignorant). Par exemple, leur réticence à avouer qu'ils ne comprennent pas le contenu. C'est là qu'interviennent les indicateurs, qui sont des “comportements spécifiques, [des] opinions significatives, [des] jugements, [des] préférences qui pourront être l'objet de questions” (Berthier, 2010, p. 48).

Indicateurs
===========

Dans la phase qui suit, on reprend chaque dimension et on trouve des indicateurs comportementaux qui lui sont liés à chaque dimension, en voici des exemples. Bien sûr, il est préférable de trouver plusieurs indicateurs pour chaque dimension, de manière à mieux caractériser les opinions.

* *Compréhensibilité* : le lecteur s'est déjà questionné sur le sens d'un paragraphe d'une ressource indicateur inversé : si cet indicateur est de niveau haut, le niveau de compréhensibilité des ressources est bas) ;
* *Crédibilité* : le lecteur s'est déjà questionné, à la lecture d'une ressource, de la véracité d'un passage (indicateur inversé : si cet indicateur est de niveau haut, le niveau de crédibilité des ressources est bas) ;
* *Réflexivité*, le lecteur a déjà essayé d'utiliser, dans le cadre de son enseignement, un conseil prescrit dans une ressource ;
* *Réutilisabilité*, le lecteur a déjà cité, ou copié, tout ou partie d'une ressource dans le cadre de son enseignement ; 
* *Volonté de collaborer*, le lecteur s'est déjà dit, à la lecture d'une ressource, “elle est incomplète, on pourrait y ajouter ceci”.
 
Une fois les indicateurs spécifiés, on peut passer à la formulation des questions. On peut varier le questionnement en se centrant (voir Berthier, 2010, Chap. 5) sur les opinions (ce que le lecteur dit qu'il pense), sur ses actions (ce que le lecteur dit qu'il fait), sur ses projets (ce que le lecteur dit qu'il a l'intention de faire), sur ses connaissances (ce que le lecteur dit qu'il sait). On n'oubliera pas des questions sur des caractéristiques personnelles, pouvant influer les opinions (âge, niveau de compétences dans tel ou tel domaine relié, genre, etc.)

Questions
=========

La démarche se continue au niveau des questions. On prend chaque indicateur et on le formule en une question non ambiguë. Voici quelques conseils pour concevoir des questions pertinentes et compréhensibles (issues de Berthier, 2010, chap. 5) :

* les questions sont compréhensibles, écrites dans un langage clair ;
* les concepts principaux sont explicités pour éviter les confusions ;
* les questions ne comportent pas de mots polysémiques ;
* chaque question ne concerne qu'une idée principale ;
* les questions sont neutres : en cas d'alternative, présenter les 2 éléments de l'alternative ;
* toute question est essentielle à la compréhension de la problématique (ne pas poser de question qu'on n'est pas sûr de pouvoir traiter) ;
* il est préférable de poser les questions plus personnelles (âge, profession, etc.) à la fin du questionnaire.

Le format de ce document interdit d'aller plus loin sur ce point. On consultera les documents en références pour en savoir plus.

Exemples de questions
=====================

Pour continuer avec notre but de concevoir un questionnaire sur les pratiques d'utilisation d'un site de ressources, voici des exemples de questions possibles :

Questions sur la connaissance du site de ressources
---------------------------------------------------

* Vous connaissez le site depuis (proposition de durées)
* Au cours des 6 derniers mois, vous avez accédé au site (proposition de fréquences)
* Vous estimez avoir une connaissance du site (proposition de qualité de la connaissance)
 
Questions sur les pratiques du site
-----------------------------------

* Pour quelle(s) utilisation(s) avez-vous accédé au site ? (proposition de raisons)
* Quels sont les types de documents que vous utilisez le plus, en règle générale (proposition de types de documents)
* Quelles est (sont) la (les) section(s) que vous appréciez et lisez le plus attentivement, en règle générale (proposition de types de sections)
* Cela vous arrive-t-il de recommander le site ou des documents du site à vos pairs ? (Oui/Non)

Questions sur l'opinion des lecteurs du site
--------------------------------------------

* Voici quelques propositions concernant votre opinion à propos de ces ressources. Indiquez votre degré d’accord pour chacune (proposition d'opinions)

	- Les sujets traités dans les documents sont faciles à comprendre
	- Les documents sont clairement écrits
	- Les documents traitent de thèmes proches de mes préoccupations
	- Les documents sont directement applicables à ma pratique 
	- Les documents m’incitent à réfléchir à ma pratique 
	- Les documents m’incintent à approfondir le sujet *via* les références complémentaires
	- Les documents développent ma compréhension de la recherche en sciences de l’éducation
	- Les documents développent ma compréhension du milieu scolaire

Références
==========

* Berthier, N. (2010). *Les techniques d'enquête en sciences sociales. Méthodes et exercices corrigés* (4e ed.). Paris: Colin.
* Maisonneuve, H. & Fournier, J.-P. (2012). `Construire une enquête et un questionnaire <https://archive-ouverte.unige.ch/unige:88809>`_. *E-Respect*, *1*(2), 15-21. 
* Porst, R. (2009). `Question wording-Concernant la formulation des questions pour des questionnaires <https://www.gesis.org/fileadmin/upload/forschung/publikationen/gesis_reihen/howto/How-To_2_rp_frz.pdf>`_ (Trad. par A. Trilling). Mannheim : ZUMA.