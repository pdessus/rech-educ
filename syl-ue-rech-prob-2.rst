.. _syl-ue-rech-prob-2:

*************************************************************************
Syllabus du cours UE 205 Recherche “Problématisation” – MEEF-PE (2019-20)
*************************************************************************

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Charroud, Christophe

.. admonition:: Informations

	* **Auteurs** : Christophe Charroud, Inspé, `Univ. Grenoble Alpes <http://www.univ-grenoble-alpes.fr>`_ et `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, `Univ. Grenoble Alpes <http://www.univ-grenoble-alpes.fr>`_.

	* **Date de création** : Décembre 2018.

	* **Date de modification** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce Document décrit l'organisation du cours de l'UE 205 du Master MEEF 1\ :sup:`re` année, Inspé, `Univ. Grenoble Alpes <http://www.univ-grenoble-alpes.fr>`_ sur la problématisation. 

	* **Voir aussi** : Documents :doc:`tuto-problematique` et :ref:`gene:innovation`.
	
	* **Matériel à télécharger** : :download:`Diapositives du cours <images/ue206-19.pdf>` et :download:`Fiche descriptive de travail de recherche <images/res-design-canevas-fr.pdf>`.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Nous décrivons ici l'organisation d'un cours de 4 h, partie de l'U.E. 205 “Recherche” du Master 1 MEEF, visant à permettre à de futurs enseignants de réfléchir à la notion de problématisation de la recherche.


Déroulement de la séance
========================

Éléments théoriques (30 min)
----------------------------

Pouquoi problématiser ? Quelles étapes ? Source :  :doc:`tuto-problematique`.

Phase 1 – Vulgarisation (1 h 30 min)
------------------------------------

Les étudiants, en binôme, choisissent un article de conférence (liste en fin de document) et réalisent une analyse du texte, en se centrant sur le problème de la recherche, et les moyens que les chercheurs ont mis en œuvre pour y répondre.
Ils utilisent le canevas descriptif de recherche téléchargeable :download:`ici <images/res-canevas.pdf>`.
Ils produisent 2 diapositives sur papier, qui seront numérisées et projetées, et seront ensuite présentées à l'assistance par leurs auteurs.

Phase 2 – Recherche (1 h 30 min)
--------------------------------

Le but de cette phase est d'organiser un “mini-colloque” permettant de réfléchir plus profondément aux travaux présentés. Il s'agit de reprendre le problème de recherche de l'article et de formuler 4 nouveaux problèmes de recherche en s'inspirant de celui proposé dans l'article choisi, de manière créative et pragmatique. Pour cela, les binômes se regroupent en 2 (donc 4 étudiants) et choisissent l'un des 2 articles qu'ils ont travaillé durant la Phase 1.

Cette Phase 2 se déroule ainsi :

* **1. Formulation des problèmes (45 min)** : Chaque groupe doit formuler 4 problèmes (problématique, question de recherche, hypothèses, idées d'études) en lien avec la thématique choisie, et les présenter sur 2 feuilles (2 problèmes par feuille), avec les contraintes suivantes : 
	
	* un des problèmes devra inclure une étude avec du numérique ; 
	* un des problèmes devra inclure une étude en maternelle ;
	* un des problèmes devra inclure une étude en lien avec l'inclusion scolaire

Les étudiants seront affectés à différents rôles :

* Un groupe de 4 étudiants aura la fonction de comité de sélection et il devra veiller à la cohérence entre la thématique et les problèmes proposés, ce comité organisera le mini colloque de fin de TD.
* Un étudiant jouera le rôle de *Joker*, il tournera de groupe en groupe afin de proposer de nouvealles idées de problèmes ;
* Un étudiant jouera le rôle de l’*avocat du diable*, il tournera de groupe en groupe en essayant de trouver les points négatifs ;
* Le reste des étudiants seront des chercheurs, regroupés par 4 en choisissant l'un de leurs articles de la Phase 1. 


* **2. Mini-colloque (45 min)**: Diapositives des problèmes (problématique, question de recherche, hypothèses, idée d’expérimentation), présentées pendant le mini-colloque en fin de séance.


Temps réservé pour la préparation de l'évaluation (30 min)
----------------------------------------------------------

Ce temps est dédié à la préparation de l'évaluation de l'UE (présentation de poster).

Liste des travaux proposés à la lecture
=======================================

.. bibliography:: syl-ue-rech-prob-2.bib
	:all:
	:style: apa
 