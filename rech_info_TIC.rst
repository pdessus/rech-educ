.. _rech_info_TIC:

*********************************************
Faire des recherches sur l'usage du numérique
*********************************************

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Charroud, Christophe
	single: auteurs; Besse, Émilie

.. admonition:: Informations

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes, avec des contributions de Christophe Charroud, Espé, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

	* **Date de création** : Septembre 2016.

	* **Date de modification** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce document présente une stratégie pour rechercher des informations sur l'usage des technologies éducatives, et pourra être utile pour des chercheurs débutant dans le domaine. Il complète le Document `Veille pédagogique et académique <http://espe-rtd-reflexpro.u-ga.fr/docs/sciedu-general/fr/latest/veillepeda.html>`_, qui lui s'intéresse aux outils de veille (comment et avec quoi chercher). Ce document s'intéresse à "où chercher, et quoi" dans le domaine. 

	* **Voir aussi** : Le Document :ref:`innovation` contient des éléments plus théoriques sur l'innovation et le numérique scolaire.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Il est maintenant très aisé, avec internet, de récupérer de nombreuses informations sur la recherche en éducation et psychologie de l'éducation. Le problème est plutôt que ces informations sont tellement abondantes, et de niveau de qualité différent, qu'il importe de réfléchir à une stratégie qui permet de trouver des informations pertinentes et de qualité. C'est le but de ce document.

Pour commencer, il faut noter que la lecture et la compréhension (même minimale) de l'anglais est nécessaire. En effet, la recherche dans ce domaine s'écrit en anglais. Cela ne signifie pas qu'il faut traduire les textes lus de A à Z, mais tout simplement qu'il faudra les parcourir, à la recherche d'informations signifiantes (au début, cela peut être le résumé, des tableaux, des figures contenus dans un document).

Ce document est structuré en deux parties. La première détaille une série d'étapes à prendre en compte à propos de recherche sur le numérique. La seconde détaille des pistes de travail possibles.

Etapes dans la recherche
========================

Principes
---------

Les principes ci-dessous nous paraissent importants à suivre quand on s'intéresse à cette question.

* S'intéresser aux usages d'un outil plutôt qu'à l'outil lui-même.
* S'intéresser aux problèmes (d'enseignement, d'apprentissage) que cet outil permet de régler.
* Penser que les effets d'un outil peuvent varier selon le contexte (niveau de classe, type d'organisation), ou même le niveau des élèves qui l'utilisent.
* Ne pas oublier les aspects éthiques et légaux, directement liés à l'utilisation du numérique, qui sont trop souvent négligés.

Formuler la question de recherche
---------------------------------

Il est possible de formuler simplement sa question de recherche, qui se compose de la manière suivante : SI *X* ALORS *Y*. Plus précisément, vous décrivez l'occurrence d'un événement *X*, dans un contexte que vous définirez également. Cet événement peut être qualifié de déclencheur d'un effet (à court ou plus long terme), sur des éléments du contexte (p. ex., apprentissage des élèves). Il s'agira également de caractériser au mieux cet effet.

Les événements-déclencheurs *X* peuvent être, par exemple :

* L'introduction de tablettes, de tableaux blancs interactifs, etc., dans la classe ;
* L'utilisation d'exerciseurs pour s'entraîner ;
* La mise en œuvre d'une nouvelle politique de formation des enseignants au numérique ;

Les événements-résultats *Y* peuvent être :

* l'apprentissage des élèves, leur motivation, etc.
* l'opinion des enseignants, des élèves, à propos de l'événement déclencheur.

Questionner son propre niveau de pratique
-----------------------------------------

La première démarche est de s'intéresser à caractériser sa pratique. On peut penser, à tort ou à raison, avoir soi-même une pratique des technologies éducatives au-dessus (ou au-dessous) de la moyenne des enseignants. Mais cet avis peut-il être confirmé par des indicateurs plus objectifs ? Il existe un classement des pratiques d'intégration des technologies éducatives dans la classe (voir :ref:`romero_taxonomie`) qui pourra être utilisée, et approfondie, pour répondre à cette question.

* Qu'a-t-il déjà été mis en place ? À quelles fins ?
* Quels résultats cela a-t-il donné ?
* Comment poursuivre ?
  
Ensuite, on peut s'intéresser à ce qui marche, que ce soit *via* la littérature de recherche ou en questionnant directement les experts. Il est assez facile, en recherchant sur internet, d'identifier les experts et de récupérer une partie de leur production.

Définir de plus près ce qu'on veut étudier
------------------------------------------

Imaginons qu'on s'intéresse à ce que recouvre une "classe intelligente" ou un "fablab" (en termes d'équipement ou de fonctionnalités). Une recherche rapide montre que beaucoup de notions utilisées dans la recherche sur le numérique éducatif sont polysémiques, surtout lorsqu'elles sont récentes.  Et des termes plus théoriques, comme "instrument", sont utilisés avec des significations différentes selon les chercheurs (p. ex., selon Simondon ou Rabardel).

Il est donc nécessaire, une fois déterminé le sujet d'étude, de faire une recherche bibliographique sur les différentes notions qui y sont reliées, et de vérifier leur signification selon les auteurs. Vous déterminerez quelles sont les revues qui procurent les articles ayant les réponses les plus claires et crédibles.

Chercher les modèles de processus
---------------------------------

Imaginons qu'on s'intéresse à l'intégration du numérique dans les classes. Ce qui pourrait être un processus relativement simple (relié à l'utilisation effective d'appareils numériques dans diverses activités d'apprentissage et d'enseignement) a été en réalité décrit de multiples manières. On ne peut donc faire l'économie, là aussi, de faire une recherche bibliographique, avec les mots-clés appropriés, pour déterminer les différentes manières de décrire le processus étudié, et trouver la manière de le décrire qui paraît le mieux convenir au contexte d'étude et/ou aux présupposés théoriques.

Voir ce qui marche avec les méta-analyses
-----------------------------------------

Soit deux pratiques pédagogiques, *X* et *Y*. Est-il préférable de pratiquer *X* plutôt que *Y* ? Les recherches en éducation abondent pour promouvoir l'une ou l'autre. Y a-t-il des recherches qui compilent ce type de résultats ? De la même manière qu'en médecine il existe des "protocoles standard" prescrivant des médicaments, des gestes, pour telle ou telle maladie, de telles "données probantes" commencent à être aussi compilées dans le domaine de la recherche en éducation. Il est donc possible de consulter les sites suivants et de se centrer sur les effets des technologies éducatives.

* `Teaching & Learning Toolkit <https://educationendowmentfoundation.org.uk/evidence/teaching-learning-toolkit>`_ (Robert Coe).
* `Visible learning <http://www.evidencebasedteaching.org.au/hattie-his-high-impact-strategies/>`_ (John Hattie).
* `What works Clearinghouse <http://ies.ed.gov/ncee/wwc/>`_.

Décrire des réseaux de recherche
--------------------------------

Un travail également possible est de mettre au jour des réseaux de recherche, à partir d'un logiciel simple d'analyse des co-citations. Cela permet, pour un sujet ou un chercheur donnés, d'avoir une liste de publications et d'auteurs par laquelle commencer le travail. Un logiciel en ligne comme `WhoCite? <https://mystudentvoices.com/scraping-google-scholar-to-write-your-phd-literature-chapter-2ea35f8f4fa1>`_, de Jimmy Tidey, peut être utilisé. 
  
Les aspects éthiques
--------------------

Les aspects éthiques ne sont pas à oublier. Il peut arriver que l'utilisation de tel ou tel outil soit subordonnée à l'implication d'une entreprise récupérant les informations de ses usagers à des fins publicitaires, ou autres. De plus, certains usages peuvent parfois ne pas être compatibles avec les principes éducatifs des chercheurs (voir le Document :ref:`droit_info_soc`).

Rendre compte du travail
------------------------

Enfin, il est toujours intéressant de communiquer à d'autres le résultat de son travail, il sera question de déterminer quel est le meilleur support pour les réflexions menées. Dans beaucoup de cas, cela est guidé, souvent même imposé. Enfin, ce compte rendu pourra, même si c'est parfois difficile, questionner les pratiques d'enseignement à la lumière de ce qui aura été trouvé.
  
Thèmes de recherche
===================

Cette section liste une série de Thèmes de réflexion, ou de recherche, qu'il est possible de suivre dans tout atelier de recherche en technologies éducatives. La figure ci-dessous organise les différents thèmes.

.. graphviz::

	graph G {
		node [fontsize=50, ranksep=10]
		rankdir=LR
		N [label="Le numérique"]
		U [label="Usages"]
		I [label="Innover"]
		M [label="Mythes"]
		E [label="Effets sur l'apprentissage"]
		J [label="Ethique et juridique"]
		H [label="Aspects éthiques"]
		L [label="Aspects légaux"]
		C [label="Cognition"]
		O [label="Charge cognitive"]
		S [label="Stratégies"]
		P [label="Stratégies pédagogiques"]
		D [label="Elèves en difficulté"]
		X [label="Exemples"]
		V [label="Jeux vidéo"]
		R [label="Exerciseurs"]
		W [label="Wikipédia"]
		A [label="Attention/addiction"]
		F [label="Fake news"]
		N -- U;
		U -- I;
		U -- E;
		U -- M;
		U -- A;
		U -- F;
		N -- H;
		H -- J;
		H -- L;
		N -- C;
		C -- O;
		N -- S;
		S -- P;
		S -- D;
		N -- X;
		X -- V;
		X -- R;
		X -- W;		
	}


1. Innover, oui mais comment ?
------------------------------

Quelles sont les pratiques pédagogiques innovantes utilisant les technologies ? Cela, non pas pour les répliquer aveuglément, mais pour comprendre les questionnements actuels, au lieu de repenser des choses qui ont pu être déjà réalisées. Deux sources peuvent être consultées :

* `L'expérithèque <http://eduscol.education.fr/experitheque/carte.php>`_
* Les rapports '`innovating pedagogy <https://iet.open.ac.uk/innovating-pedagogy>`_', édités par M. Sharples (éditions 2012-19)

Consulter ces documents pour répondre à cette question, et le document :ref:`innovation` donnera d'utiles précisions.	

2. Analyser les effets des technologies éducatives ?
----------------------------------------------------

Innover, c'est bien, mais encore faut-il peser les effets des innovations sur l'apprentissage et l'enseignement. Il n'est jamais aisé d'analyser concrètement les effets des technologies éducatives, notamment sur l'apprentissage, comme le montre le document SAPP :ref:`effets_medias`. À cette fin, il convient, d'une part, d'analyser de près les différents composants de la technologie utilisée (voir :ref:`ce_qu_est_media`) ; d'autre part, il sera ensuite possible de détailler, dans un tableau de ce type (voir :ref:`tice_comparaison`), ce qu'apporte vraiment la technologie utilisée sur l'apprentissage. Cette démarche analytique est réalisable pour toute technologie numérique.

3. Quelle est la part du mythe dans ces effets ?
------------------------------------------------

L'éducation, comme tous les domaines de recherche, n'est pas exempte de mythes, c'est-à-dire de croyances injustifiées envers les mérites de telle ou telle pédagogie, méthode, mais aussi outil numérique. Le Document :ref:`mythes_education` recense quelques-unes de ces pensées, parfois assez répandues, sans avoir été toujours validées scientifiquement. Le travail sur cette question consistera à analyser et débusquer certains mythes liés à l'usage du numérique. 

* Amadieu, F. & Tricot, A. (2014). *Apprendre avec le numérique*. Paris : Retz.
* Tricot, A. (2017). *L'innovation pédagogique*. Paris : Retz.

4. Usages du numérique, attention et addiction
----------------------------------------------

On le sait maintenant, l'usage d'internet, des réseaux sociaux, et du numérique dans son acception large, a aussi des effets négatifs, comme la capture de l'attention des utilisateurs (notamment à des fins de publicité), l'interférence avec d'autres tâches (*via* les notifications), ce qui peut amener les utilisateurs à une forme d'addiction. Le Document :ref:`medias_attention` recense ces questions. Le travail sur cette question consistera à mieux comprendre les incidences négatives du numérique sur l'attention, et les risques d'addiction relatifs.

5. Les *fake news* 
------------------

Les *fake news* tirent profit d'internet pour diffuser des informations ayant un rapport lointain avec la vérité ; les élèves y sont donc confrontés et ont souvent des difficultés à démêler le vrai du faux. Le Document :ref:`fake-news` détaille les types de calembredaines que l'ont peut lire, et pourquoi nous sommes tant attirés par elles.


6. Pour un usage éthique du numérique éducatif
----------------------------------------------

N'importe quelle technologie, et usage de technologie, peuvent-ils prendre place dans le système scolaire ? Sans doute non. Il convient donc de s'interroger sur de simples règles éthiques pouvant être mises en œuvre en contexte scolaire. Le document :ref:`droit_info_soc` donnera d'utiles précisions. 

7. Le droit français est-il un obstacle à l'usage du numérique en classe ?
--------------------------------------------------------------------------

L'espace scolaire n'échappe pas à la loi. Comment, alors, appréhender cette contrainte forte afin de ne pas enfreindre la loi ni de renoncer aux usages du numérique en classe ? Deux sources peuvent êtres consultées pour alimenter cette réflexion.

* `Guide du droit d'auteur : usage et création des ressources numériques <http://www.sup-numerique.gouv.fr/cid94535/guide-du-droit-d-auteur.html>`_
* `Apport des droits numériques pédagogiques <http://www.cfcopies.com/auteurs-editeurs/copie-numerique/apport-des-droits-numeriques-pedagogiques>`_
* `Photos, enregistrements vidéos et sonores à l’école, que dit la loi ? <https://ressourcesccharroud.wordpress.com/a-propos/photos-enregistrements-videos-et-sonores-a-lecole-que-dit-la-loi/>`_

8. Numérique et charge cognitive
--------------------------------

Le numérique (et, de manière générale, tout outil d'apprentissage) est souvent crédité de nombreux avantages sur l'apprentissage : accès facilité à de grandes quantités d'information, rétroactions plus immédiates et sophistiquées, visualisations multimédia riches, etc. Pour autant, cette richesse ne peut avoir des effets bénéfiques(et donc les informations traitées) que si les élèves ne sont pas en surcharge cognitive. En quoi consiste ce concept ? De quelle manière tenir compte de la charge cognitive des élèves pour concevoir et diffuser des documents ? Le document :ref:`charge_cognitive` donne quelques éléments de réflexion et d'action en ce sens.

9. Stratégies d'enseignement et numérique
-----------------------------------------

Une fois qu'on a analysé les effets de quelques stratégies d'enseignement avec le numérique (voir item 2 ci-dessus), on peut les scénariser en classe, c'est à dire les décomposer de manière à les rendre opérationnelles. Comment cela se fait ? Quelles stratégies efficaces peuvent se dégager ? Comment les expliquer aux élèves ? Quelle est la part de l'usage du numérique ? La série de documents conçue par le groupe `Learning Scientists <http://www.learningscientists.org>`_ est utile pour cela (voir notamment la section "*Downloadable Materials>Posters in other languages*"). 

10. Comment aider les élèves en difficulté ?
-------------------------------------------

Il est possible que l'usage (raisonné) des technologies éducatives puisse assister les enseignants dans leur aide envers les élèves en difficulté, si bien sûr leurs difficultés sont analysées convenablement. En quoi peut consister cette aide ? Le document :ref:`difficultes-TIC` donne quelques éléments de réflexion et d'action en ce sens.

11. Peut-on apprendre avec les jeux vidéos ?
--------------------------------------------

Le jeu est un des principaux et plus puissants moteurs de l’apprentissage naturel, avec l’exploration de l’environnement ou des objets et les relations sociales (Geary, 2008). Cette idée d’apprentissage par le jeu a bien évidemment été reprise par les pionniers de l’informatique pédagogique dans les années 1980, suivis ensuite par les éditeurs commerciaux, qui ont depuis développé des produits dits « ludo-éducatifs » à destination du grand public.

Aujourd’hui l’omniprésence d’écrans et objets connectés amène à réfléchir à l’éventuelle utilisation de jeux vidéo en classe,mais il faut s’interroger sur la notion d’efficacité des jeux vidéos sur l’apprentissages en prenant en compte *a minima* l’autonomie, la motivation, le contexte de classe, le scénario pédagogique. Trois sources peuvent êtres consultées pour nourrir cette réflexion.

* Berry, V. (2011). `Jouer pour apprendre : est‐ce bien sérieux ? Réflexions théoriques sur les relations entre jeu (vidéo) et apprentissage <http://cjlt.csj.ualberta.ca/index.php/cjlt/article/view/606/313>`_.
* Blaesius, N. & Fleck, S. (2015). `Quinze minutes de jeu vidéo : apports pour la prise en charge de la dyslexie <https://hal.archives-ouvertes.fr/hal-01219074/document>`_. 
* Sutter, W., Denise, J., Szilas, N. (2015). `Déterminants motivationnels et qualité de l’expérience dans un jeu vidéo en algèbre <http://archive-ouverte.unige.ch/unige:82408/ATTACHMENT01>`_.


12. Exerciseurs pour apprendre
------------------------------

Les exerciseurs (logiciels permettant des exercices systématiques, répétés, et avec rétroaction immédiate) sont devenus d'usage courant avec internet. Comment concevoir des documents de cours comprenant des rétroactions efficaces ? Avec quels outils ? Le document :ref:`exerciseurs`, ainsi que le cours complet `Évaluer l'apprentissage par ordinateur <http://espe-rtd-reflexpro.u-ga.fr/docs/scied-cours-qcm/fr/latest/>`_ donne des éléments de réflexion sur ce sujet.

13. Usages pédagogiques de Wikipedia
------------------------------------

Wikipédia est devenue une source incontournable d'informations. Pourtant, la plupart des enseignants restent encore réticents à l'utiliser à des fins pédagogiques. Il apparaît qu'une meilleure connaissance de son fonctionnement, et de ses limites, peut amener élèves et enseignants à une meilleure utilisation de cette ressource. Le document :ref:`wikipedia` donne des éléments de réflexion à ce sujet.


Les sources
===========

Internet
--------

L'utilisation de `Google Scholar <http://scholar.google.com>`_ pour trouver des articles de recherche est la plus efficace.

Les réseaux sociaux
-------------------

Twitter, pour peu qu'on suive des chercheurs actifs dans le domaine auquel on s'intéresse, est un outil tout à fait pertinent pour se tenir à jour de l'actualité de la recherche.

Les revues
----------

Là encore, il existe tellement de revues, en anglais et même en français, que nous ne pouvons ici qu'effleurer le sujet ; toutefois les revues suivantes peuvent être intéressantes à parcourir :

* `ALSIC (apprentissage des langues) <http://alsic.revues.org/>`_
* `Bulletin de l'EPI <http://www.epi.asso.fr/revue/articsom.htm>`_
* `Distances et Médiations des Savoirs <http://dms.revues.org/76>`_
* `MathémaTICE (ens. des maths) <http://revue.sesamath.net/>`_
* `Médiations et médiatisations <https://revue-mediations.teluq.ca/index.php/Distances/index>`_
* `STICEF <http://sticef.univ-lemans.fr/index.htm>`_
* `RITPU (enseignement supérieur) <http://www.ritpu.org/>`_

Les conférences
---------------

Des actes de conférences intéressantes sont également disponibles sur internet :

* `TICE 2012 <http://gdac.uqam.ca/tice2012/>`_
* EIAH : taper EIAH dans le moteur de recherche du site `HAL <http://hal.archives-ouvertes.fr/index.php>`_ pour accéder aux actes de cette conférence.


Les sites internet de vulgarisation
----------------------------------- 

* `Que dit la recherche ? Canopé <http://www.agence-usages-tice.education.fr/que-dit-la-recherche/>`_
* `Learning Scientists <http://www.learningscientists.org>`_
* `Le site des dossiers de veille de l'Ifé <http://ife.ens-lyon.fr/vst/DA/ListeDossiers.php>`_


Webographie
===========

* Sur les données probantes et les méta-analyses : http://rire.ctreq.qc.ca/2016/09/donnees-probantes-dt/
* Le site de signets de l'un des auteurs de ce document : https://www.diigo.com/user/pdessus

Fiche récapitulative des éléments
=================================

#. Quelle est ma question de recherche ?
#. Chercher sur internet (et `Google Scholar <http://scholar.google.com>`_):	

     #) Qui sont les experts du domaine
     #) Quelles sont les revues les plus importantes du domaine

Quizz
=====

.. eqt:: Rech_info_TIC-1

	**Question 1. 	Quel est le pré-requis nécessaire pour effectuer des recherches sur l'usage pédagogique du numérique ?**


	A) :eqt:`I` `Connaître la programmation`
	B) :eqt:`C` `Lire et comprendre l'anglais`
	C) :eqt:`I` `Savoir ce qu’est un outil`
	D) :eqt:`I` `Connaître les grands thèmes de la recherche`

.. eqt:: Rech_info_TIC-2

	**Question 2. 	Quel est l'un des principes lorsqu'on fait de la recherche sur l'usage du numérique ?**

	A) :eqt:`I` `S'intéresser à l'outil plutôt qu'à son contexte`
	B) :eqt:`I` `S'intéresser à l'outil plutôt qu'à ses aspects éthiques et légaux`
	C) :eqt:`C` `S'intéresser aux usages d'un outil plutôt qu'à l'outil lui-même`
	D) :eqt:`I` `S'intéresser à l'outil lui-même plutôt qu'aux usages`
