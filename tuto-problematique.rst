
.. _tuto_problematique:

****************************************************
Tutoriel -- Problématique et hypothèses de recherche
****************************************************

.. https://www.academic-toolkit.com/downloads
.. http://katdaley.blogspot.fr/?utm_content=buffer486f8&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer&view=classic


.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Charroud, Christophe

.. admonition:: Informations

 * **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, Espé & LaRAC, Univ. Grenoble Alpes & Christophe Charroud, Espé, Univ. Grenoble Alpes

 * **Date de création** : Janvier 2015, augmenté en janvier 2017 et janvier 2019.

 * **Date de modification** : |today|.
 
 * **Statut du document** : En travaux. 

 * **Résumé** : Ce tutoriel donne des indications pour élaborer une problématique de recherche et formuler des hypothèses, dans un contexte de recherche en sciences de l'éducation. **Attention : ce tutoriel ne peut se substituer aux conseils d'un-e directeur-e de recherche**.

 * **Voir aussi** : Le :ref:`tuto_recueil-donnees`, le :ref:`tuto_organiser_memoire`, le :ref:`tuto_methodo_rech` et le :ref:`tuto_redaction_memoire`. Le :doc:`tuto_concevoir_quest` donne des éléments pour préciser les dimensions, indicateurs et questions d'une enquête.

 * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


.. epigraph::

	"Je préfère des questions auxquelles on ne peut répondre que des réponses qui ne peuvent être questionnées. (Richard P. Feynman)"
	"Il est difficile d'attraper un chat noir dans une pièce sombre, surtout lorsqu'il n'y est pas" :cite:`firestein14`.

Introduction : Attraper un chat noir dans une pièce sombre
==========================================================

Tout chercheur est dans cette disposition : il se donne la tâche de mieux comprendre quelque chose (le chat noir) au sein d'un contexte (la pièce) et il est très rarement sûr de l'issue de cette entreprise (le chat n'est peut-être pas là). Quelles sont les méthodes qui s'offrent à lui pour y parvenir ? Il peut exister plusieurs méthodes pour tenter d'attraper le chat (et, à tout le moins, déterminer s'il est bien dans la pièce) :

* effectuer de nombreuses mesures de la pièce pour essayer de déterminer où il pourrait être ;

* construire un modèle réduit de la pièce (notamment grâce aux mesures précédentes), y mettre un animal à sa taille (*e.g.*, un chaton) pour essayer de deviner le comportement du chat ;

* trouver une pièce identique (mais éclairée) et y installer un chat noir, qui aurait le plus possible de points communs avec le chat noir à trouver. Observer ce chat de près pourrait nous permettre d'en inférer où le chat noir de la pièce sombre se trouve ;
 
* trouver des personnes qui auraient, par le passé, attrapé un chat noir dans une pièce sombre et solliciter leur témoignage sur leur manière de procéder ;

* observer de très nombreux chats dans des pièces sombres (un peu moins sombres que la pièce-cible, pour qu'on puisse les voir) et en déduire un modèle mathématique prédisant son emplacement (ou de son déplacement) dans la pièce ; 

Ce document n'est pas conçu pour traiter de manière académique, c'est-à-dire extensivement, la question de la problématique de recherche, qui est trop dépendante du contenu du mémoire pour être traitée correctement, mais donne quelques éléments pratiques que nous espérons suffisants pour avancer dans la spécification de recherches en éducation.

La démarche traditionnellement préconisée dans ces ouvrages (voir :cite:`cohen11,delandsheere76,leon77,ouellet81,quivy88,vandermaren95`) est de type « dirigée par les buts » (*top-down*) : on commence par échafauder une problématique, puis des questions et on termine par des hypothèses. Même si de nombreux auteurs reconnaissent que cela n’est pas si simple et que de nombreux aller-retours entre ces éléments sont nécessaires, et que la  démarche réelle d’un chercheur est moins linéaire. 

Problématiser, problématisation
===============================

Depuis quelques années, la “problématisation” est à la mode, et avec ce mot, tous ceux dérivés de “problème”, Benoît :cite:`benoit05` en fait un intéressant historique. Il montre que le premier terme est “problématique”, employé dès 1949 par Bachelard et Ricœur, mais d'émergence plus récente (les années 1980), viennent ensuite “problématiser” et “problématisation”. Il montre aussi que, paradoxalement, ces mots couramment utilisés dans le champ de l'éducation (du lycée à l'université), sont absents des dictionnaires de philosophie et d'éducation.

Qu'est-ce que problématiser ? Fabre (:cite:`fabre15`, chap. 4) en donne une définition :

1) il s’agit d’un processus multidimensionnel impliquant position, construction et résolution de problèmes ; 
2) d’une recherche de l’inconnu à partir du connu (points d'appui) ; 
3) d’une dialectique de faits et d’idées, d’expériences et de théories 
4) d’une pensée contrôlée par des normes intellectuelles, éthiques, techniques, ... (définies ou à construire) ;
5) “d’une schématisation fonctionnelle du réel qui renonce à tout embrasser et à reproduire la réalité mais vise plutôt à construire des outils pour penser et agir”.

Vérité et apodicité
===================

Chercher à résoudre un problème, ce n'est pas découvrir des “vérités”, mais découvrir des *nécessités* et comprendre comment elles s'organisent entre elles. Orange :cite:`orange05` montre que faire comprendre le mécanisme de la digestion au Cycle 3, ce n'est pas juste faire apprendre des concepts, c'est montrer que certains mécanismes sont *nécessaires* : distribution de la nourriture, tri, transformation. Sans eux, cela ne fonctionne pas.
Apodictique : *Qui a une évidence de droit et non pas seulement de fait. Une proposition apodictique est nécessairement vraie, où que l'on soit.* (Wikipedia, `art. apodictique <https://fr.wikipedia.org/wiki/Apodictique>`_). Les réponses aux problèmes sont des connaissances nécessaires, pas seulement possibles : “savoir n'est pas simplement “savoir que”, mais savoir que cela ne peut pas être autrement” (:cite:`orange05`, p. 78, citant Reboul).


Démarche de recherche
=====================


Trouver un sujet
----------------

Tout d'abord, il faut se fixer un sujet de recherche intéressant. Pour continer de filer la métaphore du chat, la perspective de recherche du chat choisie peut-elle être à la fois fructueuse (arriver à l'attraper), sécure (sans blesser ni le chat ni le chercheur), efficace et efficiente (elle ne demandera pas des moyens inconsidérés, en temps ou en argent), mais aussi assez originale (elle différera un peu des méthodes existantes). 

Voici quelques idées pour en trouver un (les idées peuvent évidemment se combiner) :

* *chercher à côté* : transposer une notion d’un domaine à un autre, (p. ex. la notion de système dynamique, c’est-à-dire de système qui évolue sans l’intervention de l’opérateur qui le contrôle, a fait l’objet de nombreuses études dans le champ de l’ergonomie, quelle serait sa transposition dans le domaine de l’enseignement ?) ;

* *chercher ce qui dysfonctionne* : étudier une situation éducative qui dysfonctionne, concevoir un moyen de remédier à cela et le tester ;

* *chercher ce qui va bien* : tester un moyen d’enseignement/apprentissage qui semble particulièrement efficace, penser à utiliser un groupe-contrôle, utilisant par exemple une méthode "standard", ou réalisant une tâche neutre ;

* *chercher à comprendre ce qui se passe* : étudier un événement qui vous semble curieux, car il donne des résultats contre-intuitifs, essayer de le faire se reproduire et proposer une explication ;

* *chercher les liaisons* : mettre en relation deux variables qui semblent liées et en étudier la corrélation, *i.e.*, déterminer si les deux variables varient ensemble ou de manière opposée (sans qu'on puisse déterminer, avec cette méthode, un lien de cause à effet) ;

* *chercher dans la littérature* : deux auteurs proposent chacun leur manière de voir un événement éducatif, étudier cet événement et proposer sa propre vision des choses ; de plus, les auteurs proposent souvent, dans leurs conclusions, des pistes de recherches que l’on peut reprendre ;

* *chercher dans le prêt-à-penser* : si le bon sens comprend souvent des jugements adéquats, certains sont peut-être non fondés, vous pouvez en trouver un et le tester (et être possiblement surpris de ne pas avoir confirmation de l'évidence) ;

* *chercher en utilisant un autre matériel* : reprendre une étude en utilisant un autre matériel (ordinateur, télévision) et en vérifiant son impact sur certaines variables ; 

* *chercher dans ses dadas* : utiliser sa propre expertise dans un domaine pour monter une expérimentation utilisant une de ses productions, par exemple un logiciel, une capacité à animer les groupes, etc.

Faire un constat (décrire un problème)
--------------------------------------

Une démarche de recherche ressemble beaucoup à une enquête, :cite:`fabre09`, dans laquelle, comme vu plus haut, l'enquêteur doit *trouver* quelque chose, et entamer un processus de problématisation. Ce processus peut être aidé, guidé, par le directeur de la recherche, des pairs, des ouvrages, *via* ce que Fabre appelle des *inducteurs*, qui sont des questions amenant une recherche plus profonde, précise. Par exemple, dans une recherche sur les élèves petit-parleurs, on pourra avoir les inducteurs suivants :

* Qu'est-ce qu'un élève petit-parleur ? 
* Quel(s) est (sont) le(s) critère(s) permettant de les repérer ?
* Quel(s) est (sont) le(s) facteur(s) qui amènent un élève à être petit-parleur ?

Ensuite, une fois qu'on s'est centré sur un sujet, il est nécessaire de faire un constat (en forme de problème), et d'énoncer quelques pistes de recherche (la suite provient de :cite:`robinson06`, p. 6) qui expliquent le phénomène décrit dans le constat. Par exemple : 

* Constat : "Les élèves ont un bas niveau de lecture"
* Cause : "Les causes de leurs bas scores de lecture est un manque d'habiletés en prélecture et sociales à l'entrée à l'école"

Cela nous amène à préciser les éléments suivants :

* quelles sont les preuves que leur niveau est bas ?
* est-ce une preuve fiable ?
* la cause est-elle plausible ? L'école a-t-elle évalué ces habiletés incriminées ? 
* peut-on avancer d'autres explications ?

Les contraintes du problème
---------------------------

Il convient ensuite de préciser, toujours en citant Robinson et Lai :cite:`robinson06`, les contraintes agissant sur ce problème. Les contraintes peuvent être humaines (liées aux élèves, aux enseignants, aux parents), matérielles et temporelles (budget, temps), cognitives (problèmes de compréhension repérés).

Formuler l'action
-----------------

Ensuite se formule l'action d'enseignement (l'intervention) qu'on estime réalisable en fonction des contraintes imparties.

Formuler une problématique
--------------------------

Une problématique permet de rassembler les éléments ci-dessus. Elle formule un écart constaté entre une situation de départ, insatisfaisante, et une situation d’arrivée, désirable. Ce doit être un énoncé sous forme de question exigeant une réponse logique. Et traite d'une relation entre au moins deux variables. Il faut avoir la possibilité de vérifier la/les relation(s) entre ces variables, que nous nommerons *X* et *Y*.

* L’utilisation par les élèves de tablettes tactiles (*X*) améliore-t-elle leur compréhension du contenu *Y* ?
* Le discours d’un enseignant à distance est-il différent de celui d’un enseignant en présence ? (discours (*X*), contexte présence/distance (*Y*)).

La question posée doit être (des éléments de Daley, 2016) :

* *poser vraiment une question*, donc se terminer par un point d'interrogation ;
* *être non biaisée*, c'est-à-dire avoir le moins de présupposés possible. Même si tout chercheur, en tant qu'humain, a des présupposés, la question ne doit tenir pour acquis que le moins de choses possibles. Par exemple (issu de Daley, 2016) : plutôt que de se demander “Quelles sont les expériences de solitude et isolement éprouvées par les étudiants internationaux de Melbourne” il est préférable de poser “Quelles sont les expériences de connexion sociale entre étudiants internationaux de Melbourne”. Assumer que les étudiants sont *nécessairement* seuls et isolés est de trop.
* *heuristique* : elle apporte du nouveau. « La moyenne de la taille des garçons est supérieure à celle des filles » n'est pas une question heuristique. 
* *testable* : on peut la vérifier par l'expérience, ou au moins par un raisonnement « La taille de la classe est liée aux redoublements » est testable.
* Elle n'est pas *ad hoc* (circulaire) : on doit la tester indépendamment du phénomène à expliquer. « Les enseignants experts sont-ils des enseignants meilleurs que les autres ? », « Les outils de discussion en ligne sont-ils des moyens de faire discuter les étudiants ?», sont des questions circulaires.

Formuler la question de recherche
---------------------------------

De la problématique, il est possible de dériver une question de recherche plus précise. Voici un exemple de formulation (tirée de Hunt & Sullivan, :cite:`hunt74`) de la question de recherche, avec le triplet {C,S,E} : Le **Comportement** (C) est fonction du **Sujet** (S) qui l'exprime et de l'**environnement** (E) dans lequel ce dernier l'exprime. 

Exemple : Les élèves (S) bénéficiant de la méthode de soutien (E) ont de meilleurs résultats (C) que ceux n’en bénéficiant pas. Le Tableau I ci-dessous présente un ensemble de données éducatives selon cette formulation, et la suite de cette section présente quelques formulations selon ce cadre, issues de recherches antérieures.

**Tableau I - Des données sur l'éducation selon la méthode CSE** .

+------------------+---------------+-------------------+
| Comportement     | Sujet         | Environnement     |
+==================+===============+===================+
| Comportement     | Elèves        | Méthodes          |
+------------------+---------------+-------------------+
| Attitudes        | Enseignant    | Contextes         |
+------------------+---------------+-------------------+
| Objectifs        | Parents       | Groupes           |
+------------------+---------------+-------------------+
|                  |               | Niveaux           |
+------------------+---------------+-------------------+

* Comment des préparations pourraient être en partie réutilisées (E) par des élèves (S), à des fins d’apprentissage (C) ?
* Comment faire en sorte que des règles de gestion de la classe (E) puissent être aisément rappelées et surtout prises en compte (C) par les élèves (S), notamment avec l’aide du numérique ?
*  Comment l’évaluation (C) par les pairs (S) pourrait être réalisée par l’intermédiaire du numérique (E) ?
* Comment les évaluations formatives (C) de l’enseignant (S) pourraient être consignées (E) pour en garder des traces réflexives ? 
* Comment faire en sorte qu’un public d’étudiants hétérogène (informaticiens et statisticiens) (S) d’un cours de statistiques (E) soient intéressés (C) par le cours ?
* Comment l'adaptation des pratiques de l'enseignant (structurer, expliciter, laisser du choix) (E) peut-elle favoriser la motivation (C) des élèves (S) ?
* “L’album écho” (E) peut-il favoriser la production orale (C) d’élèves en difficulté (S) ?
* Le jeu (E) peut-il favoriser l’autorégulation comportementale (C) d’élèves de Petite Section (S).
* Quel modelage langagier de l’enseignant (E) mettre en place pour aider l’entrée en communication (C) d'élèves de Petite section (S).
* Quel est l'effet de la directivité conversationnelle de l’enseignant (E) sur l’expression (C) des élèves (E)

Formuler des hypothèses opérationnelles
---------------------------------------

Une fois la question de recherche principale énoncée, il est assez facile de formuler une ou plusieurs hypothèses opérationnelles. Une hypothèse est une réponse anticipée au problème de recherche, exprimant une relation entre deux ou plusieurs concepts. C’est la déclinaison de la problématique en termes opérationnels, et une prédiction doit être faite à propos d'un résultat. Elle doit (certains items sont de Daley, 2016) :

* *être précise et courte*, c'est-à-dire qu'elle donne des informations nécessaires et suffisantes pour qu'on comprenne la relation entre les variables ;
* *être explicable* (au moins en partie) par des résultats de recherche antérieurs, qui sont citables dans la section "état de l'art". Ainsi, il faut éviter de faire des prédictions farfelues mais les fonder, dans la mesure du possible, sur des recherches déjà réalisées ;
* *être vérifiable*, sa formulation doit être testable, et donc laisser la place à une possible non confirmation de l'hypothèse.

Dans le cas présent, cette formulation nécessitera de préciser chaque élément du triplet ci-dessus :

* préciser l'Environnement (*E*), c'est-à-dire dans quel contexte, environnement, situation, le Comportement des participants va se produire, et pourquoi. Une description précise de l'environnement est donc importante, d'une part pour comprendre l'étude en question, mais également si d'autres chercheurs veulent reprendre le travail ;
* préciser les caractéristiques des participants (*S*). Il est parfois nécessaire de cibler une catégorie particulière de participants, et non la classe entière.
* préciser le comportement étudié (*C*). Cela est notamment utile si on a à l'observer : le définir de manière non ambiguë permettra de le repérer.

Webographie
===========

* Daley, K (2016). `What makes a good research question <http://katdaley.blogspot.com/?utm_content=buffer486f8>`_.
* Depover, C. `Méthodes et outils de recherche en sciences de l'éducation (Univ. de Mons, Belgique) <http://ute.umh.ac.be/methodes/>`_.


Références
==========

.. bibliography:: tuto-problematique.bib
 :cited:
 :style: apa