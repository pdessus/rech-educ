.. _syl_ue-rech_prob:

**********************************************************************************
Syllabus du cours UE Recherche “Problématisation & innovation” – MEEF-PE (2017-18)
**********************************************************************************

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Charroud, Christophe

.. admonition:: Informations

	* **Auteurs** : Christophe Charroud, Espé, `Univ. Grenoble Alpes <http://www.univ-grenoble-alpes.fr>`_ et `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, `Univ. Grenoble Alpes <http://www.univ-grenoble-alpes.fr>`_.

	* **Date de création** : Décembre 2017.

	* **Date de modification** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce Document décrit l'organisation du cours de l'UE 206 du Master MEEF 1\ :sup:`re` année, Espé, `Univ. Grenoble Alpes <http://www.univ-grenoble-alpes.fr>`_. 

	* **Voir aussi** : Document :ref:`innovation`.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Nous décrivons ici l'organisation d'un cours de 4 h, partie de l'U.E. “Recherche” du Master 1, visant à permettre à de futurs enseignants de réfléchir à la notion d'innovation pédagogique, de pratique innovante, à la fois actuelles et futures, et de les relier à des problématiques de recherche.


Déroulement de la séance
========================

1. Fouille de l'expérithèque (45 min)
-------------------------------------

Les étudiants, par groupes de 3, se connectent à l'Expérithèque du site du MEN `Eduscol <http://eduscol.education.fr>`_ (http://eduscol.education.fr/experitheque/carte.php), présentant plus de 5 000 projets innovants. Après avoir fixé un domaine et un niveau, l'explorent à la recherche de 2 ou 3 projets, dont un qu'ils considèrent unanimement comme très innovant. En parallèle, ils répondent aux deux questions suivantes :

* Quel(s) est (sont) le(s) critère(s) que vous avez pris en compte pour choisir ces projets innovants ?
* À quel(s) problème(s) éducatif(s) (d'enseignement et/ou apprentissage) ces projets répondent-ils ?


2. Présentation du projet le plus innovant et exposé des critères (45 min)
--------------------------------------------------------------------------

Un représentant de chaque groupe vient présenter oralement aux autres leur projet le plus innovant, en expliquant pourquoi son groupe  l'a choisi, et à quel(s) problème(s) éducatif(s) ce projet se confronte. Les critères sont listés au tableau pour mémoire.

Chaque groupe d'étudiant, à la fin de chaque présentation, évalue son intérêt (*i.e.*, explique dans quelle mesure il aimerait la mettre en œuvre dans sa classe), à l'aide d'un système de vote.

3. Aspects théoriques de l'innovation (30 min)
----------------------------------------------

Un bref aperçu de la problématique de l'innovation en éducation est réalisé, reprenant des éléments du Document :ref:`innovation`.

4. Atelier de création d'une innovation (2 h)
---------------------------------------------

Le but de cette dernière activité et de susciter une créativité et un esprit critique de la part des participants. Par groupes de 3, les étudiants vont piocher, dans 3 piles des "cartes d'inspiration" : Actions, Comportements, et Outils (similaires à celles présentées dans le Document :ref:`conpa`), montrant chacune un mot censé les guider dans la phase de création d'une innovation, phase qui va se dérouler ainsi :

4.1. Brainstorming
``````````````````

Cette phase dure env. 30 min.

* Réflexion rapide (*brainstorming*) sur ce que les mots des cartes peuvent induire comme type d'innovation, sans que les membres du groupe se censurent. Par exemple, déterminer une *action* d'apprentissage ou d'enseignement, favorisant un *comportement* aidé par un *outil*, chacun de ces trois paramètres étant inspiré par la carte correspondante qui a été tirée par le groupe.
* Après le *brainstorming*, chaque groupe fait un bilan oral avec régulation, pour évaluer les meilleures idées et rejeter des idées moins intéressantes. 
* Formuler un problème éducatif (d'enseignement et/ou d'apprentissage) en lien avec l'idée (les idées) retenues. Une liste de domaines dans lesquels l'innovation pourrait intervenir est présentée ci-dessous.
* Un représentant de chaque groupe présente brièvement son idée aux autres groupes, pour susciter des commentaires et de possibles nouveaux raffinements.

**Liste de domaines d'innovation scolaire (tiré de OECD 2014b).**
 
* dans les manières d'enseigner
* dans l'organisation de la classe
* dans l'utilisation des manuels
* dans les méthodes d'évaluation
* dans l'utilisation du numérique
* dans la prise en compte des différences entre élèves
* dans la collaboration entre enseignants
* dans les rétroactions pour les élèves
* dans les relations avec l'extérieur (parents, etc.)
* dans l'utilisation de l'espace (mobilier, architecture)

4.2. Spécifier l'innovation
```````````````````````````

Cette phase dure env. 30 min. Chaque groupe détaille l'innovation et la manière dont elle pourrait régler tout ou partie du problème. Au besoin, décrire deux protagonistes l'élève et l'enseignant (ou parent/élève) et expliquer de manière narrative comment il pourrait utiliser cette innovation. Ses avantages, ses inconvénients. Se référer au Document :ref:`sbd`.

4.3. Présenter l'innovation
```````````````````````````
Cette phase dure env. 45 min.
Un représentant par groupe vient présenter l'innovation aux autres. Comme précédemment, chaque groupe d'étudiant, à la fin de chaque présentation, évalue son intérêt (*i.e.*, dans quelle mesure il aimerait la mettre en œuvre dans sa classe), à l'aide d'un système de vote.


Référence
=========

* OCDE (2014a). L'éducation, un secteur innovant ? Indicateurs de l'éducation à la loupe.

* OECD (2014b). Measuring innovation in education: A new perspective. CERI, OECD Publishing [`PDF en ligne <http://www.oecd-ilibrary.org/education/measuring-innovation-in-education_9789264215696-en>`_]
