.. _atelier_poster_sci:

==========================================
Atelier -- Réaliser un poster scientifique
==========================================

.. index::
   single: auteurs; Dessus, Philippe
   single: auteurs; Nurra, Cécile

.. admonition:: Information

	* **Auteur** : `Cécile Nurra <http://webcom.upmf-grenoble.fr/sciedu/cnurra>`_, Inspé & LaRAC, Univ. Grenoble Alpes, avec des ajouts de `Philippe Dessus <http://pdessus.fr/>`_, Espé & LaRAC, Univ. Grenoble Alpes.

	* **Date de création** : Avril 2017.

	* **Date de modification** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Cet atelier décrit les consignes pour la réalisation d'un poster scientifique (UE Recherche 206, M1 MEEF, Espé, Univ. Grenoble Alpes), pour des étudiants en physique-chimie (2\ :sup:`e` degré.

	* **Voir aussi** : Document :ref:`tuto_lire_article`.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_


Introduction
============

Le but de l'atelier est de concevoir un poster scientifique à partir d’un des articles mis à disposition. Toutes les informations nécessaires sont dans l’article choisi : il n'est donc pas demandé d'ajouter de l'information.

Le travail ci-dessous est réalisé en groupe de 2 ou 3 étudiants *uniquement* (pas de travail seul, pas de travail à 4, sans exception), en présence d’un enseignant qui pourra répondre à vos questions et vous guider dans le travail. Le travail demandé est réalisable en 4 h. L'article sera choisi dans la séance précédente de manière à laisser du temps pour en prendre connaissance.

Le travail sera livré au formateur, par courriel, à une date-butoir qu'il vous communiquera en cours. Un accusé de réception sera envoyé.

Tâche
=====

Voici le détail du travail demandé (le poster étant réalisé en séance et sur ordinateur, merci de prévoir un ordinateur par groupe de travail) :

#. Choisir un des articles scientifiques (voir liste ci-dessous).
#. Lire l'article en détail. Il peut être utile de prendre connaissance du Document :ref:`tuto_lire_article` pour en comprendre la structure. Certains textes peuvent paraître difficiles à cause des résultats qui paraissent complexes. Le formateur peut aider à leur compréhension. 
#. Concevoir, sur une feuille, l'organisation spatiale des différentes parties du poster. 
#. Utiliser un logiciel de présentation (p. ex., *Impress* de `LibreOffice <https://www.libreoffice.org>`_ ou `OpenOffice <http://www.openoffice.org>`_, *PowerPoint*, etc.) pour reproduire cette organisation.
#. Remplir les différents cadres du poster, en fonction du contenu de l'article.
#. Sur une deuxième page, écrire un bref argumentaire (maximum 600 mots) justifiant le choix, ainsi qu'une appréciation de l’article notamment en lien avec des questionnements professionnels. Cet argumentaire sera inséré dans une 2\ :sup:`e` page.


Qu'est-ce qu'un poster ?
========================

Le poster comme mode de présentation d’action ou de recherche est classique dans les différentes manifestations scientifiques et manifestations appliquées pour présenter des actions menées par des chercheurs ou des enseignants.

Un poster scientifique doit comprendre toutes les parties d’un rapport de recherche habituel (une introduction théorique, la méthodologie, les résultats, la discussion). Ce travail permet d’évaluer certaines des compétences développées dans le thème « lecture critique d’article ». Une attention particulière sera apportée :

#. à l’explicitation de la problématique et des hypothèses et du lien fait avec la théorie présentée (Thème « problématisation ») ; 
#. au choix des mesures (Thème « opérationnalisation ») – *cf*. la grille d’évaluation ci-dessous pour plus de détails. Etant donné son espace limité, le poster demande parfois de faire des choix. Il est donc parfois justifié de ne pas exposer tous les résultats, mais de choisir les plus pertinents.
 
Des exemples de posters
=======================

Plusieurs exemples de posters seront mis à disposition (le but de l’exercice étant la compréhension du texte et le choix des informations, et non de se centrer sur la présentation, le côté graphique). Il est conseillé d'adapter l'organisation du poster au contenu présenté, donc il n'est pas nécessaire d'adopter strictement l'organisation d'un poster existant.

Bien souvent ces posters ont été réalisés pour être expliqués. Essayer de faire en sorte que le poster soit compréhensible sans explications supplémentaires. 

* Gagnon, V. (2012). `Offrir un soutien personnalisé aux élèves par l’insertion d’échafaudages à leurs outils de collaboration en ligne <http://affordance.uqac.ca/publications/Poster-CLAIR2012-VG.pdf>`_. Clair 2012. Accessible à http://affordance.uqac.ca/publications/Poster-CLAIR2012-VG.pdf.

* Bianco, M., Lima, L., Massonié, J., & Bressoux, P. (2016). `Learning to read at Grade 1: Cognitive and motivational predictors <http://webcom.upmf-grenoble.fr/sciedu/lima/Porto.pdf>`_. Poster à SSSR Int. Conf., Porto. Accessible à http://webcom.upmf-grenoble.fr/sciedu/lima/Porto.pdf.


Grille d'évaluation
===================

Présentation générale (1 point)
-------------------------------

Sur le poster doivent figurer les informations suivantes :

* Les nom et prénom des étudiants ayant réalisé le poster.
* ESPE de l’académie de Grenoble, lieu du site, MEEF SD M1, année 2016-2017, UE recherche. 
* En titre : Le titre de l’article original et ses auteurs
* La référence complète aux normes APA du texte choisi (voir ci-dessous)

Introduction Théorique-état de l’art (4 points)
-----------------------------------------------

* Les éléments sélectionnés sont pertinents et utiles pour comprendre le contexte théorique de ou des études présentées
* Les éléments sélectionnés permettent de justifier la problématique et l’hypothèse
  
Problématique et hypothèses (4 points)
--------------------------------------

* La problématique et l’hypothèse sont présentes et semblent comprises
* La problématique est résumée de manière adéquate
* Les hypothèses/questions de recherche sont mentionnées et sont correctes

Méthode (matériel et procédure) (4 points)
------------------------------------------

* Le matériel utilisé est clairement explicité (on comprend comment ont été opérationnalisés les concepts mobilisés dans la problématique)
* On comprend suffisamment comment s’est déroulée l’étude (procédure)

Résultats (2 points)
--------------------

* La présentation de ces résultats est pertinente et adéquate (s’il y a eu sélection de certains résultats, ou d’une seule étude sur les 2 présentes dans le texte, cela est justifié dans la partie justification)

Discussion (2 points)
---------------------

* Les résultats sont discutés en regard des éléments théoriques sélectionnés
* Des ouvertures sont mentionnées

Respect de l'orthographe (1 point)
----------------------------------

* Le poster est exempt d'erreurs d'orthographe et de syntaxe, ce qui rend sa lecture fluide

Lien avec le métier (2 points)
------------------------------

* Le lien avec le métier est exposé et la justification du choix du poster est cohérente.


Liste des articles
==================

Un seul article est à choisir dans la liste. Des exemplaires sur papier seront fournis en cours :

* Calmettes, B. (2010). `Analyse pragmatique de pratiques ordinaires, rapport pragmatique à l’enseigner. Etude de cas : des enseignants experts, en démarche d’investigation en physique <https://rdst.revues.org/354>`_. *Recherches en Didactique des Sciences et des Technologies, 2*, 235-272. Accessible à https://rdst.revues.org/354 [*Comment des enseignants de physique experts mettent-ils en œuvre une séance d’investigation ?*]

* Cross, D., & Le Maréchal, J.-F. (2013). `Analyse de sujets de devoirs en chimie en classe de terminale : point de vue de la charge cognitive <http://rdst.revues.org/725>`_. *Recherches en Didactique des Sciences et des Technologies, 7*, 169–192. Accessible à http://rdst.revues.org/725 [*Comparaison de problèmes de chimie selon qu’ils sont issus d’annales de bac ou pas, en fonction de leur complexité cognitive*] 

* Kaminski, W., & Mistrioti, Y. (2000). `Optique au collège : le rôle de la lumière dans la formation d’image par une lentille convergente <http://www.lacambrecouleur.be/pdf/BUP11148.pdf>`_. *Bulletin de l'Union des Physiciens, 823*, 757-784. Accessible à http://www.lacambrecouleur.be/pdf/BUP11148.pdf [*Compare les explications d’élèves de 3e sur la formation d’images par une lentille convergente selon qu’ils ont suivi un enseignement plus ou moins fondé sur la compréhension des phénomènes*]

* Malonga Moungabio, F., & Beaufils, D. (2010). `Modélisation et registres sémiotiques : exemple d’étude de manuels de physique de terminale <http://rdst.revues.org/243>`_. *Recherches en Didactique des Sciences et des Technologies, 1*, 293-316. Accessible à http://rdst.revues.org/243 [*Analyse des registres sémiotiques utilisés dans des exercices en électricité de 7 manuels de TS)*]

* Maurines, L., Gallezot, M., Marie-Joëlle Ramage, M.-J., & Beaufils, D. (2013). `La nature des sciences dans les programmes de seconde de physique-chimie et de sciences de la vie et de la Terre <http://rdst.revues.org/674>`_. *Recherches en Didactique des Sciences et des Technologies, 7*, 19-52. Accessible à http://rdst.revues.org/674 [*Quelle conception des finalités des sciences est véhiculée dans les manuels de seconde en SPC et SVT* ?]

* Prieur, M., Monod-Ansaldi, R., & Fontanieu, V. (2013). `Réception des démarches d’investigation prescrites par les enseignants de sciences et de technologie <http://rdst.revues.org/685>`_. *Recherches en Didactique des Sciences et des Technologies, 7*, 53-76. Accessible à http://rdst.revues.org/685 [* Analyse textuelle d'un questionnaire auprès d'enseignants du secondaire de matières scientifiques à propos des démarches d'investigation*]


