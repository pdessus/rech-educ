*********************************************
Tutoriel -- La captation de vidéos de classes
*********************************************

.. http://www.learningscientists.org/blog/2018/12/9/weekly-digest-136
.. https://psyarxiv.com/ux29v

.. index::
	single: auteurs; Dessus, Philippe


.. admonition:: Information

	* **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, Espé & LaRAC, Univ. Grenoble Alpes.

	* **Date de création** : Décembre 2018.

	* **Date de modification** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce tutoriel préconise un dispositif standardisé de captation de vidéos de classe, pouvant être utilisé dans différents contextes scolaires et de formation.
	
	* **Remerciements** : Nous remercions Dominique Renault, rectorat de l'académie de Grenoble, pour ses commentaires d'une première version du document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

Introduction
============

Le dispositif est conçu pour éviter la présence d'une tierce personne dans la classe, qui serait source potentielle de gêne. Si le matériel le permet, il pourra être possible de commander la caméra à l'arrière à distance, d'une autre pièce. 

Matériel 
========

* 2 caméras numériques HD
* 2 pieds (permettant de placer la caméra à env. 1,40 m du sol)
* un micro-cravate (relié à un enregistreur audio porté par l'enseignant ou, ce qui est préférable, à la caméra à l'arrière)
* si possible, ajouter un micro d'ambiance général, placé au centre de la pièce, pour capter les échanges entre élèves.

Contrat de cession de droits
============================

Il est indispensable, avant la séance, que l'enseignant et les parents des élèves aient signé un contrat de cession de droits à propos des données, sur le modèle Eduscol (http://eduscol.education.fr/internet-responsable/ressources/boite-a-outils.html). 

Il est important de réfléchir à un dispositif sécurisé de transport et stockage des captations réalisées.

Disposition
===========

Le technicien installe les caméras en tenant compte du contexte de la classe et du placement des élèves et s'assure que l'ensemble du matériel fonctionne correctement, notamment que les batteries des caméras permettent l'enregistrement d'environ 2 séances (soit 1 h 30 env.).

* Placer une caméra à l'avant de la classe, en direction des élèves à env. 1,40 m du sol. La placer du côté où est la source de lumière principale (fenêtre) et, si possible, du côté opposé à la porte d'entrée (pour éviter les dommages). Cette caméra doit être équipée d'un grand angle pour capter le plus d'élèves possibles.

* Placer une caméra à l'arrière de la classe, du même côté que la première, à env. 1,40 m du sol. Il faut vérifier que le tableau est bien filmé en entier par la caméra.

* En plaçant les deux caméras, s'assurer qu'aucune source de lumière importante (fenêtre, lampe, soleil) se trouve derrière les sujets filmés. Faire en sorte que les arrière-plan soient plus sombres que les sujets. Régler les plans afin que la totalité des élèves soit observée par au moins l'une des deux caméras. 

* Installer le micro-cravate sur l'enseignant (au niveau du haut de son col de chemise).

Démarrage
=========

* Mettre les 2 caméras et l'enregistreur sur "Play".
* Faire un "clap" de départ pour synchroniser les caméras et l'enregistrement audio [Est-ce suffisant pour faire la synchro]

Visionnage
=========

- Le son du micro-cravate (le cas échéant) sera mixé avec la vue de l'arrière. Les captations vidéos peuvent être visualisées et codées par un outil d'analyse vidéo comme `Elan <https://tla.mpi.nl/tools/tla-tools/elan/>`_

Références
==========

* `Solutions choisies pour quelques situations par l'équipe COAST d'ICAR <http://blog.espe-bretagne.fr/visa/wp-content/uploads/dispositif_icar.pdf>`_

* Roschelle, J. (2000). Choosing and using video equipment for data collection. In A. E. Kelly & R. A. Lesh (Eds.), *Handbook of research design in mathematics and science education* (pp. 709–729). Mahwah: Erlbaum.

* Soubrié, P. (2012). *Étude sur les modalités d'utilisation de vidéos de situations de classe en formation professionnelle d'enseignants*. Grenoble : Mémoire de Master 2 en sciences de l'éducation.




