:orphan:

.. _tuto_rech_1-1:

=========================================================
Tutoriel : La recherche sur les ordinateurs 1:1 en classe
=========================================================


.. index::
	single: auteurs; Dessus, Philippe

.. admonition:: Informations
	* **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes.

	* **Date de création** : Octobre 2016.

	* **Date de modification** : |today|.

	* **Résumé** : Ce document décrit, pas à pas XXX.

	* **Note** : Ce document s'inspire de l'excellent document de N. Darbois, intitulé `Comment se faire rapidement une idée de l’efficacité d’une technique ou d’un outil utilisé en kinésithérapie ? <https://cortecs.org/activites/comment-se-faire-rapidement-une-idee-de-lefficacite-dune-technique-ou-dun-outil-utilise-en-kinesitherapie/>`_. Il en reprend notamment le plan.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-nd/3.0/fr/>`_


Introduction
============

Je suis un enseignante de collège intéressée par l'utilisation du numérique en classe. Il existe de nombreuses expérimentations récentes sur l'utilisation d'ordinateurs en classe, prêtés aux élèves pour une année complète.

J'aimerais savoir s'il existe des recherches mentionnant des effets (positifs ou négatifs) de cette utilisation sur l'apprentissage, et je voudrais aussi savoir, comme cet usage s'accompagne souvent de stockage d'informations dans les nuages, ce que la recherche indique à ce sujet. Mieux informée, je pourrais ensuite me décider à utiliser un tel dispositif dans mes classes.

La suite de ce tutoriel détaille la démarche que je pourrais réaliser. 

Il est à noter que les documents retrouvés sont valides à la date de création de ce document et peuvent changer selon la date des requêtes et le moteur de recherches utilisé (ici, `Qwant <https://www.qwant.com>`_). Il est aussi à noter que c'est une enquête *rapide*, c'est-à-dire devant être approfondie par la lecture et la réflexion. Néanmoins, nous pensons qu'en l'état elle peut être utile à toute personne voulant apprendre à mener des recherches sur l'intégration du numérique.

Formuler une question précise
=============================

**Temps estimé** : 1 minute

Je me demande quels sont les effets de l'utilisation quotidienne d'ordinateurs prêtés individuellement à des élèves de collège, sur l'apprentissage de matières littéraires.

Critères de sélection
=====================

**Temps estimé** : 1 minute

Comme je vais sans doute récupérer de très nombreux résultats, je vais me centrer sur les études laissant les ordinateurs aux élèves en prêt durant une période conséquente (disons, 6 mois) et laisser de côté les études utilisant des "classes mobiles" ou "classes nomades" (où les ordinateurs sont repris à la fin de chaque utilisation en cours).

Trouver les mots-clés
=====================

En français
-----------

**Temps estimé** : 

Les mots-clés en français ne sont pas trop difficiles à trouver (entre crochets, l'explication du choix).

* "ordinateurs portables" 		[le terme communément utilisé]
* intégration					[le terme utilisé, aussi, pour montrer une utilisation raisonnée]
* classes 						[précise l'utilisation en classe]
* collège OU "second degré" 	[précise le niveau]
* français						[précise la matière]

On pourrait imaginer pouvoir saisir une seule requête avec tous ces termes mais, en réalité, le faire va renvoyer plus probablement des documents sur l'enseignement du français dans le second degré, avec une composante d'inclusion d'élèves à besoins spécifiques (à cause du terme "intégration"). Il est donc préférable XXX

En anglais
----------

**Temps estimé** : 

Dans le domaine de l'étude de l'intégration du numérique, il est vraiment nécessaire d'aller regarder ce qu'il s'écrit en anglais, car c'est la langue véhiculaire de la recherche : formuler les mots-clés en anglais, ce n'est pas seulement être au courant du travail de ceux qui ont l'anglais pour langue première.