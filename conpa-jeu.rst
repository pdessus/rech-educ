.. _conpa:

****************************************************************
CONPA : Un jeu de création et réflexion sur l'usage du numérique
****************************************************************

.. index::
   single: auteurs; Dessus, Philippe
   single: auteurs; Jolivet, Sébastien


.. admonition:: Informations

   * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, Univ. Grenoble Alpes et Sébastien Jolivet, LIG-MeTAH & Espé, Univ. Grenoble Alpes.

   * **Date de création** : Novembre 2016.

   * **Date de modification** : |today|.

   * **Statut du document** : En travaux.

   * **Version** : v. 0.3.

   * **Résumé** : Ce document présente les règles d'un jeu de créativité dans le domaine de la recherche et développement (R&D) du numérique en situation éducative. Il part d'une question de recherche et permet aux joueurs de l'explorer, de la raffiner, et d'en trouver ensemble des solutions créatives, en tirant des cartes. Son nom vient des différents thèmes des cartes **C**\omportement, **O**\utils, **N**\otion, **P**\ensée, **A**\ction.

   * **Voir aussi** : Doc. :ref:`syl_ue-rech_prob`, qui présente un cours dans lequel une partie de ce jeu est utilisée.

   * **Licence** : Le document et le jeu décrits ci-dessous sont placés sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

   * **Note légale** : « Le Jeu CONPA s’est inspiré du jeu `MotivéSens <http://motivesens.fr>`_, imaginé et exploité par la société MotivéSens (817 404 908 R.C.S. Vienne) en matière de psychologie (Thérapie Comportementale et Cognitive et inspiré de la Psychologie Positive) qui a créé un jeu basé sur l’idée de Mesdames Carré et Valantin consistant en un travail de réflexion et de choix autour de cartes ressources pour atteindre un objectif positif préalablement défini.
   Il est rappelé que la notice du Jeu MotivéSens est soumise à droit d’auteur et que toute reprise ou citation, de tout ou partie du Jeu MotivéSens, comme toute utilisation du Jeu MotivéSens est strictement interdite, que ce soit pour un usage de formation ou commercial, sauf accord préalable et exprès des auteurs dudit Jeu. La reprise ou la citation du Jeu CONPA nécessitera obligatoirement la mention, en tant que source d’inspiration du concept de jeu du Jeu CONPA, de la société MotivéSens, développé par Mesdames Carré et Valantin en 2015.
    
   * **Remerciements** : Ce jeu a reçu l'aide du projet `Game2Learn <http://perform.grenoble-inp.fr/internationalisation/game2learn-generateur-de-jeux-tangibles-pour-l-apprentissage-des-langues-522679.kjsp>`_, financé par l'Idex de l'Univ. Grenoble Alpes. Merci à son responsable, John Kenwright, pour avoir permis que CONPA ressemble à un *vrai* jeu de cartes. Merci aux joueurs de CONPA de la première heure (de Canopé), et surtout à Catherine Bonnat, Marina De Simone, Nadine Mandran, et Laurence Osete pour leurs commentaires à propos d'une version précédente de ce jeu.  Nous utilisons ici le terme de “joueur” (au masculin) pour faciliter la lecture du document.

   * **Matériel** : Télécharger les :download:`cartes en PDF recto/verso zippées <images/cartes-conpa.zip>` ou en :download:`cartes en PDF, 1 page par carte, par Game2Learn <images/cartes-conpa-g2l.zip>`.

Généralités
===========

But et principe
---------------

Ce jeu de table permet de stimuler la conception de situations de recherche et développement (R&D) d'usage innovant du numérique en situations scolaires. L’activité des joueurs est de réaliser des liens (intégration) entre un problème qu’ils auront spécifié intialement, et chacune des cartes qu’ils tirent tour à tour. Cela leur permet de raffiner, explorer, ou même modifier si nécessaire, la question initiale. Les cartes apportent des éléments à intégrer au problème, et leur caractère aléatoire incite à “sortir du cadre”, imaginer des pistes de travail originales

Le thème du jeu peut être changé si des concepteurs produisent de nouveaux jeux de cartes (ils peuvent contacter les auteurs du jeu pour les informer), en suivant la règle explicitée ci-dessous. Par exemple, il est tout à fait possible d'utiliser ce jeu pour stimuler des idées de recherche en sciences humaines, pour évaluer un outil qui a été créé en groupe.


Thèmes des cartes
-----------------

.. image:: /images/panoc-comportement.png
   :alt: comportement
   :scale: 10 %
   :align: right


* **C Comportements** : Ces cartes donnent aux participants des idées de comportements mis en œuvre dans l'apprentissage. Elles reprennent principalement des items de la taxonomie de Bloom (Anderson *et al.*, 2001). Elles permettent aux joueurs de se figurer concrètement quelles activités les apprenants pourraient mettre en œuvre dans le projet résolvant leur question de recherche/développement.

.. image:: /images/panoc-outil.png
   :alt: outil
   :scale: 10 %
   :align: right

* **O Outils Matériels & Ressources** : Ces cartes donnent aux participants des suggestions d'outils matériels, de ressources, principalement mais pas uniquement numériques, pouvant jouer un rôle pour résoudre leur question.

.. image:: /images/panoc-notion.png
   :alt: notion
   :scale: 30 %
   :align: right

* **N Notions** : Ces cartes évoquent diverses notions issues des sciences humaines et sociales (de nombreux champs sont considérés). Elles permettent aux joueurs de faire des liens entre ces notions, potentiellement non envisagées, et leur question de recherche, et d'ajouter une profondeur théorique. 

.. image:: /images/panoc-pensee.png
   :alt: pensée
   :scale: 10 %
   :align: right


* **P Pensée** : Ces cartes donnent aux participants une certaine hauteur de vue, en les amenant à considérer leur problème à la lumière d'un aphorisme en lien avec l'éducation, la technique, ou la créativité.

.. image:: /images/panoc-action.png
   :alt: action
   :scale: 10 %
   :align: right

* **A Actions** : Ces cartes donnent aux participants des suggestions concrètes de possibilités de mettre à l'épreuve, de résoudre, leur problème. Comment concevoir, mesurer, évaluer, ou simplement argumenter, que la solution trouvée a un effet sur la situation de départ ? Par quelles actions ? Quelles méthodes ? Ces cartes  permettent d'élaborer quelques pistes.


Caractéristiques du jeu
=======================

**Nombre de joueurs** : 1 à 6.

**Durée d'une partie** : 30 min à 2 h.

Matériel
========

* Un dé standard (à 6 faces).
* Un plateau individuel vierge par joueur. Le plateau comprend une zone (en haut à gauche) pour écrire librement une question et rappelle la correspondance dé-cartes (1=C, 2=O, 3=N, 4=P, 5=A). Le reste du plateau est librement utilisable pour le dépôt des cartes. Des plateaux supplémentaires sont à prévoir pour agrandir les plateaux des participants.
* Un crayon à papier par joueur, divers crayons de couleur, des blocs de feuillets repositionnables (*post-it*) de différentes couleurs. 
* Les 5 jeux de cartes, téléchargeables en PDF zippé :download:`ici<images/cartes-conpa.zip>`.

Déroulement du jeu
==================

1. Mise en place du jeu (5 min)
-------------------------------

* Chaque joueur récupère un **plateau individuel vierge** (feuille A3). Les 5 piles de cartes, battues au préalable, sont disposées au centre de la table pour qu'elles soient accessibles à tous.

* La **durée du jeu** est fixée au préalable.

* Optionnellement, un participant (p. ex., formateur) peut guider le jeu et aider la réflexion des joueurs.

* Optionnellement, il peut être envisagé que les joueurs se mettent en binôme et réfléchissent à une question commune. 


2. Définir une question de recherche ou de développement (env. 10 min)
----------------------------------------------------------------------

Cette phase est composée de deux actions.

A. Chaque participant écrit individuellement, dans le cadre en haut à gauche de son plateau, une question de recherche ou développement (dorénavant, Question) qui lui tient particulièrement à cœur.

Le format de la Question doit être :samp:`"Comment X peut être Yé par Z, avec l'aide partielle ou totale du numérique ?`. Afin de ne pas influer la suite du processus, il est important que les participant, dans cette phase initiale, ne spécifient pas les outils numériques potentiellement utilisables. En voici des exemples.

* Comment des préparations (X) pourraient être en partie réutilisées (Y) par des élèves (Z), à des fins d'apprentissage ?
* Comment faire en sorte que des règles de gestion de la classe (X) puissent être aisément rappelées et surtout prises en compte (Y) par les élèves (Z), notamment avec l'aide du numérique ?
* Comment l'évaluation (X) par les pairs (Z) pourrait être réalisée (Y) par l'intermédiaire du numérique ?
* Comment les évaluations formatives (X) de l'enseignant (Z) pourraient être consignées (Y) pour en garder des traces réflexives ?
* Comment faire en sorte qu'un public d'étudiants hétérogène (informaticiens et statisticiens) (Z) d'un cours de statistiques (X) restent intéressés (Y) par le cours ?

B. Chaque participant lit tour à tour sa question, la commente rapidement, et elle est écrite sur un tableau à la vue de tous.

3. Investigation/discussion à partir des cartes (20 min-2 h)
------------------------------------------------------------

Dans cette phase d'investigation à partir des tirages de cartes, chaque joueur réalise les actions suivantes à tour de rôle. Il est à noter que chaque tour dure une dizaine de minutes.

A. **Lancer de dé**. Le joueur dont c'est le tour lance le dé, qui donne le numéro de la pile de la carte à piocher (1=C, 2=O, 3=N, 4=P, 5=A). Si le 6 sort, le joueur pioche une carte dans la pile de son choix.

B. **Lecture de la carte et réflexion individuelle/collective**. Le joueur lit la carte à haute voix, prend quelques secondes pour réfléchir et exprime, toujours à haute voix, quelle pourrait être la relation entre le contenu de cette carte et sa question de recherche. Les autres joueurs peuvent l'aider à préciser des choses, poser des questions pour approfondir les liens, en relisant la Question sur le tableau si nécessaire. La consigne principale, habituelle dans ce type de jeux, *est de ne pas se censurer*, d'exprimer toutes les idées qui viennent, et de se servir des idées des autres pour exprimer la sienne, sans les censurer, toujours librement. La formulation des cartes est intentionnellement vague pour laisser toute latitude d'interprétation.

C. **Pose de la carte et éventuels liens et reformulations**. Ensuite, le joueur pose librement la carte sur son plateau, à l'endroit qui lui paraît le plus approprié par rapport aux autres cartes déjà posées. Il justifie ce tri à haute voix et ajoute, au crayon à papier ou avec les crayons de couleur, tout élément permettant de faire le lien (flèches, arguments). Il peut également modifier/reformuler sa Question de départ (sans effacer les précédentes versions). Au fur et à mesure, chaque joueur se trouve face à une sorte de "carte de concepts" en gestation à propos de sa question initiale, qu'il amende, complète, restructure tout au long du jeu (et donc pas uniquement quand c'est à lui de tirer une carte). Il est ainsi tout à fait possible de profiter d'une idée issue d'une carte tirée par un autre joueur. Au besoin, il peut adjoindre un plateau supplémentaire pour gagner de la place.

D. **Suite et fin du jeu**. Le jeu se poursuit à l'étape 1 tant que la durée impartie n'est pas écoulée. Optionnellement, quand il arrive que des joueurs n'ont jamais tiré une catégorie de cartes, le dernier tour peut se réaliser sans lancer de dé, mais en laissant chaque joueur tirer une carte dans la pile de son choix.

4. Débriefing
=============

Une fois le jeu terminé, chaque participant prend un moment personnel pour faire une synthèse de l'état de sa question de recherche et des réponses qu'il lui apporte, et l'expose aux autres joueurs. Il peut aussi photographier l'état de son plateau, pour mémoire.

Règles optionnelles
===================

Le jeu, dans la règle explicitée ci-dessus, est de rythme assez lent, car chaque participant doit envisager les tenants et aboutissants de la nouvelle idée tirée. Il est possible d'obtenir un jeu plus rythmé (mais peut-être également moins créatif), en modifiant les règles comme suit.

Le dé n'est plus utilisé. Chaque participant, à son tour, pioche une carte de chaque type. Dans une première phase, chacun essaie d'intégrer les différentes cartes à sa Question (il est possible de laisser une seule carte de côté sur les 5), puis expose ses idées à tour de rôle. Deux ou trois tours peuvent ainsi être joués.

Il est aussi possible que tous les participants réfléchissent à la même question, et que les cartes soient fixées par aimant sur un tableau à la vue de tous.
 
Références des icones
=====================

Clapperboard par Iconfactory Team, Leader et Wrench par Gregor Cresnar, Idea par Magicon, provenant toutes de `The Noun Project <https://thenounproject.com>`_.

Références
==========
Anderson, L. W., Krathwohl, D. R., & Bloom, B. S. (2001). *A taxonomy for learning, teaching, and assessing : a revision of Bloom's taxonomy of educational objectives* (Complete ed.). New York: Longman.

