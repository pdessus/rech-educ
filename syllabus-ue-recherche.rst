.. _syll_ue_rech_sd:

**************************************************
Syllabus -- UE 800 – Recherche – TD - M1 MEEF - SD
**************************************************

.. index::
	single: auteurs; Dessus, Philippe


.. admonition:: Informations
   * **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, Univ. Grenoble Alpes.

   * **Date de création** : Janvier 2017.

   * **Date de modification** : |today|.

   * **Statut du document** : Terminé.

   * **Résumé** : Ce document présente le contenu de l'UE Recherche 800, M1 MEEF (2\ :sup:`e` degré) pour l'année universitaire 2016-17, à l'Espé, Univ. Grenoble Alpes, et plus particulièrement leurs TD.

   * **Licence** : Ce document est placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


But de l'UE
===========

Cette UE a pour but de familiariser les étudiants avec la recherche, dans le but d'un réinvestissement lors de la réalisation de leur mémoire et écrit réflexif en M2. Son exergue pourrait être "**La recherche comme éclairage sur la posture professionnelle**". Elle a deux buts principaux :

* Apprendre à exploiter les ressources de la recherche (et donc par exemple, avoir des connaissances sur les démarches de recherche) ;
* Adopter une démarche scientifique, au moins dans sa réflexion (problématiser, formuler des hypothèses, chercher les explications alternatives avec rigueur, etc.). 

Cette UE est constituée de 8 h CM (communs aux masters MEEF premier degré, second degré et encadrement éducatif) et de 16 h de TD. L'objet de ce document est de décrire les TD.


Compétences travaillées
=======================

1. Problématisation (4h)
------------------------

Problématiser une situation est une compétence importante pour comprendre son environnement. Que se passe-t-il dans cette situation ? Quelles hypothèses est-ce que je peux faire ? Quel lien entre la théorie et la problématique ? etc. Ce thème pourra être abordé sous l’angle des pratiques dites « innovantes ». Quelles incidences cela peut avoir sur des questions éthiques ?

Des documents décrivant, d'une part, des innovations issues de pratiques de classe (récupérées dans `l'Expérithèque <http://eduscol.education.fr/experitheque/carte.php>`_) et, d'autre part, de la recherche, seront analysés en séance.

2. Opérationnalisation : quoi mesurer et comment ? (4h)
-------------------------------------------------------

L’opérationnalisation de concepts théoriques consiste à passer du concept à une mesure qui soit le plus objective possible. Par exemple, opérationnaliser le concept de motivation peut se faire de différentes manières : questionnaire, mise en action, persévérance, performance, posture, etc. C’est donc la question de ce qu’il fait mesurer pour tester une hypothèse et de comment le mesurer qui sera abordée.

3. Lecture critique d’article (4h)
----------------------------------

Il s’agira ici de comprendre comment est structuré un article, d’essayer de prendre du recul sur la méthodologie utilisée pour tester l’hypothèse proposée, de comprendre comment la problématique est liée à l’exposé théorique, est-ce que les auteurs ne vont pas trop loin dans leur conclusion, etc. 

Si une véritable recension de l’article ne peut être effectuée à ce niveau, il s’agit de comprendre ce sur quoi il faut être vigilant lorsqu’on aborde lit des rapports de recherche (ou qu’on écoute des conférences). Apprendre à lire un article rapidement en comprenant les différentes parties d’un rapport de recherche peut également être un des buts de ce thème.

Il s’agira ici de comprendre comment est structuré un article, d’essayer de prendre du recul sur la méthodologie utilisée pour tester l’hypothèse proposée, de comprendre comment la problématique est liée à l’exposé théorique, est-ce que les auteurs ne vont pas trop loin dans leur conclusion, etc. Si une véritable *review* (relecture, recension) de l’article ne peut être effectuée à ce niveau, il s’agit de comprendre ce sur quoi il faut être vigilant lorsqu’on aborde lit des rapports de recherche (ou qu’on écoute des conférences). Apprendre à lire un article rapidement en comprenant les différentes parties d’un rapport de recherche peut également être un des buts de ce thème. 


Contenu des séances de travaux dirigés (16 h)
=============================================

* **Séance 1** (3 h, **le 26 janvier 2017 matin**). Problématisation et innovation. Cette séance sera l'occasion de lire, d'analyser, et rapporter au groupe diverses innovations dans le domaine de l'enseignement des sciences.

* **Séance 2** (3 h, **le 2 mars matin**). Mesurer et apprendre. Quelles variables prendre en compte dans les recherches en éducation, notamment pour rendre compte de l'apprentissage ?

* **Séance 3** (3 h, **le 7 mars matin**). Présentations narratives et mythes pédagogiques. Nous verrons que des présentations narratives de phénomènes de la nature peuvent engager la motivation des élèves, mais aussi que, du point de vue des enseignants, certaines représentations à propos de l'apprentissage sont des mythes.

* **Séance 4** (3 h, **le 21 mars matin**). Lecture critique d'articles. Cette séance sera l'occasion de lire et d'analyser des textes de recherche pour en faire une présentation sous la forme d'un poster.

* **Séance 5** (4 h, **le 2 mai après-midi**). Séance de réalisation du poster et des fiches de séminaire.

**Matériel** : De nombreuses recherches sur internet seront nécessaires pendant les séances, il est donc préférable d'amener son ordinateur portable (au moins un pour deux étudiants).

Production attendue et évaluation
=================================

Productions attendues
---------------------

* La réalisation d'activités demandées pendant les TD.
* La rédaction d’un poster numérique (par un logiciel de présentation de type Impress, `LibreOffice <http://fr.libreoffice.org>`_), décrivant un article scientifique et dont les parties seront : introduction théorique, méthode, résultats, discussion. L'article sera donné environ un mois avant la séance 5 (réalisation du poster). Des trames de posters seront proposées.
* La rédaction de 3 fiches attestant le suivi de séminaires de recherche, et montrant leur apport pour le métier d'enseignant. 
  
Ce travail permet d’évaluer certaines des compétences développées dans le thème « lecture critique d’article ». Une attention particulière sera apportée à : 1) l’explicitation de la problématique et des hypothèses et du lien fait avec la théorie présentée (Thème « problématisation » et 2) au choix des mesures (Thème « opérationnalisation »).

Certains posters pourront être affichés à l'Espé. Une session de présentation des posters pourra également être organisée à la journée des mémoires de l'Espé, ou à la journée-recherche.


Évaluation
----------

L’évaluation est fondée sur le poster, la participation active à la formation et la réalisation des activités attendues.


Plagiat
-------

Tout plagiat détecté sera signalé aux instances universitaires compétentes.


Références
==========

* `Ressources SAPP, Espé Grenoble <http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/>`_
* `Infosphère (UQÀM, Canada) <http://www.infosphere.uqam.ca>`_